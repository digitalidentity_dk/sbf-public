﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sofd_fjernprint_integration.Controllers.Dto
{
    public class Attachment
    {
        public string filename { get; set; }
        public string content { get; set; }
    }
}
