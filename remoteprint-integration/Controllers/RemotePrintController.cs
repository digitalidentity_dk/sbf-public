﻿using System;
using Microsoft.AspNetCore.Mvc;
using sofd_fjernprint_integration.Controllers.Dto;
using sofd_fjernprint_integration.Services;

namespace sofd_fjernprint_integration.Controllers
{
    [Route("api/[controller]")]
    public class RemotePrintController : Controller
    {
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private RemotePrintService _remotePrintService;

        public RemotePrintController(RemotePrintService remotePrintService)
        {
            _remotePrintService = remotePrintService;
        }

        [HttpPost("SendLetterToCpr")]
        public IActionResult SendLetterToCpr([FromBody] SenderLetterToCprRequest request, [FromHeader] string allowPhysicalPrint)
        {
            try
            {
                bool physicalPrintAlternativeAllowed = allowPhysicalPrint != null && allowPhysicalPrint.ToLower() == "true";
                string requestGuid = _remotePrintService.SendLetterToCpr(request.cpr, request.cvr, request.senderId, request.contentTypeId, request.subject, Convert.FromBase64String(request.pdfFileBase64), request.attachments, physicalPrintAlternativeAllowed);
                if (requestGuid == null)
                {
                    // not a bad request per say....
                    return Ok("Ikke tilmeldt e-boks!");
                }

                return Ok(requestGuid);
            }
            catch (Exception e)
            {
                log.Error("Error occurred while trying to send digital post.", e);
                return BadRequest();
            }
        }
    }
}
