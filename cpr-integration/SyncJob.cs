using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using SofdCprIntegration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

[DisallowConcurrentExecution]
public class SyncJob : IJob
{
    private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    // Inject the DI provider
    private readonly IServiceProvider _provider;
    private readonly IConfiguration _configuration;
    private PersonContext personContext;

    public SyncJob(IConfiguration configuration, IServiceProvider provider)
    {
        _configuration = configuration;
        _provider = provider;
    }

    public Task Execute(IJobExecutionContext context)
    {
        CleanupUnusedEntries();

        FetchFromSFTP();
        
        return Task.CompletedTask;
    }

    private void CleanupUnusedEntries()
    {
        log.Info("Running SQL Cleanup job");

        // Create a new scope
        using (var scope = _provider.CreateScope())
        {
            personContext = scope.ServiceProvider.GetService<PersonContext>();

            DateTime thirtyDaysAgo = DateTime.Today.AddDays(-30);
            var notUsedPersons = personContext.Person.Where(p => p.LastUsed < thirtyDaysAgo);

            if (notUsedPersons.Count() > 0)
            {
                log.Info("Removing " + notUsedPersons.Count() + " cached persons from DB because the are not used");

                personContext.Person.RemoveRange(notUsedPersons);
            }
        }
    }

    private void FetchFromSFTP()
    {
        log.Info("Running FTP Sync Job");

        // Create a new scope
        using (var scope = _provider.CreateScope())
        {
            personContext = scope.ServiceProvider.GetService<PersonContext>();

            SFTPService sftp = new SFTPService(_configuration);
            DateTime previousDate = DateTime.Now; // overwritten below, so the value does not matter

            LastSync lastSync = personContext.LastSync.OrderBy(x => x.Id).FirstOrDefaultAsync().Result;
            if (lastSync != null)
            {
                previousDate = lastSync.LastSyncDate;
            }
            else
            {
                lastSync = new LastSync();
                previousDate = DateTime.MinValue;
            }

            var fileData = sftp.GetNewestFile(previousDate);
            if (fileData != null)
            {
                // parse the file
                List<string> updatedPersons = ExtractCPR(fileData.Content);

                log.Info("File from CPR office contained " + updatedPersons.Count + " numbers with changes");

                // update timestamp after reading from sftp
                lastSync.LastSyncDate = fileData.LastWriteTime;
                personContext.Update(lastSync);

                // remove updated persons from cache-db
                personContext.Person.RemoveRange(personContext.Person.Where(p => updatedPersons.Contains(p.Cpr)));

                personContext.SaveChanges();
            }
        }
    }

    private List<string> ExtractCPR(string content)
    {
        return content.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None) //split string into lines
                        .Where(l => !l.StartsWith("0000") && !l.StartsWith("9999") && !string.IsNullOrWhiteSpace(l)) //skip empty lines and start & end line
                        .Select(l => l.Substring(3, 10)) // cut only the cpr
                        .Distinct() // remove duplicates
                        .ToList(); // return as list
    }
}
