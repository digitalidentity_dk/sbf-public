package dk.digitalidentity.sbf.service.organization.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeDTO {
    private String uuid;
    private String username;
    private String firstName;
    private String surname;
    private String email;
    private String phone;
    private String position;
    private String department;
    private String orgunitUuid;
    private String orgunitPath;
}
