package dk.digitalidentity.sbf.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sbf.dao.MeetingInternalContactDao;
import dk.digitalidentity.sbf.dao.model.InternalContact;
import dk.digitalidentity.sbf.dao.model.Meeting;
import dk.digitalidentity.sbf.dao.model.MeetingInternalContact;

@Service
public class MeetingInternalContactService {
	
	@Autowired
	private MeetingInternalContactDao meetingInternalContactDao;
	
	public MeetingInternalContact getById(long id) {
		return meetingInternalContactDao.findById(id);
	}
	
	public MeetingInternalContact save(MeetingInternalContact meetingInternalContact) {
		return meetingInternalContactDao.save(meetingInternalContact);
	}
	
	public MeetingInternalContact createAndSave(InternalContact internalContact, Meeting activeMeeting) {
		MeetingInternalContact meetingInternalContact = new MeetingInternalContact();
        meetingInternalContact.setInternalContact(internalContact);
        meetingInternalContact.setMeeting(activeMeeting);
		return meetingInternalContactDao.save(meetingInternalContact);
	}
	
	public void createAndSaveAll(List<InternalContact> internalContacts, Meeting activeMeeting) {
		for (InternalContact internalContact : internalContacts) {
			MeetingInternalContact meetingInternalContact = new MeetingInternalContact();
			meetingInternalContact.setInternalContact(internalContact);
			meetingInternalContact.setMeeting(activeMeeting);
            meetingInternalContactDao.save(meetingInternalContact);
		}
	}
}
