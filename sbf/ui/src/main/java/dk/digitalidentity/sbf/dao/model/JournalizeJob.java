package dk.digitalidentity.sbf.dao.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import dk.digitalidentity.sbf.dao.model.enums.JobStatus;
import dk.digitalidentity.sbf.dao.model.enums.AgendaType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity(name = "journalize_job")
@Getter
@Setter
public class JournalizeJob {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    @NotNull
    private LocalDateTime created;

    @Column
    private LocalDateTime changed;

    @Column
    private String createdBy;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private AgendaType agendaType;

    @JsonBackReference
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "meeting_id")
    private Meeting meeting;

    @JsonBackReference
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "meeting_party_id")
    private MeetingParty meetingParty;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private JobStatus status;

    @Column
    private String esdhReference;

    @Column
    private String error;
}