package dk.digitalidentity.sbf.service.sbsys;

import dk.digitalidentity.sbf.service.sbsys.dto.CreateCaseResponse;
import dk.digitalidentity.sbf.service.sbsys.dto.CreateDocumentResponse;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("SBSYSServiceMock")
public class SBSYSServiceMock implements ISBSYSService {
    @Override
    @SneakyThrows
    public CreateCaseResponse createCase(String title, String partyCpr, String caseWorkerUsername, String partyName, String partyAddress, String partyPostCode, String partyCity ) {
        var result = new CreateCaseResponse();
        result.setId(1);
        result.setSagIdentity("00000000-0000-0000-0000-000000000001");
        result.setSagsTitel(title);
        result.setNummer("27.00.01-G01-9999-21");
        Thread.sleep(1500);
        return result;
    }

    @Override
    @SneakyThrows
    public CreateDocumentResponse createDocument(String esdhCaseReference, String title, String description, String partyCpr, byte[] file) {
        var result = new CreateDocumentResponse();
        result.setDokumentID(1);
        result.setDokumentIdentity("00000000-0000-0000-0000-000000000001");
        Thread.sleep(1500);
        return result;
    }

    @Override
    @SneakyThrows
    public void updateCaseWorker(String caseUuid, String caseWorkerUsername) {
    	 Thread.sleep(1500);
    }

    @Override
    @SneakyThrows
    public void closeCase(String caseReference, String comment) {
    	 Thread.sleep(1500);
    }

    @Override
    @SneakyThrows
    public void openCase(String caseReference) {
    	 Thread.sleep(1500);
    }
}