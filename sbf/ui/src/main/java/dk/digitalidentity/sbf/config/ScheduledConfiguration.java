package dk.digitalidentity.sbf.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "scheduled")
public class ScheduledConfiguration {
	private boolean enabled = false;
}
