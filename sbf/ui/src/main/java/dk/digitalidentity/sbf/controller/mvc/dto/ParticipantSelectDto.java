package dk.digitalidentity.sbf.controller.mvc.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString(onlyExplicitlyIncluded = true)
public class ParticipantSelectDto {
	@ToString.Include
	private Long id;
	@ToString.Include
	private ParticipantTypeDto type;
	private String textString;
}
