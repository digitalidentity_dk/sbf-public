package dk.digitalidentity.sbf.controller.rest.dto;

import javax.validation.constraints.Pattern;

import dk.digitalidentity.sbf.dao.model.enums.ConsentType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class PartyDto {
	@ToString.Include
	private String id;
	private String cpr;
	@ToString.Include
	private String name;
	@ToString.Include
	private String surname;
	private String address;
	private String city;
	private String postCode;
	private String birthday;
	@Pattern(regexp = "^((.+)@(.+))?$")
	private String email;
	@Pattern(regexp = "^([\\d\\+() ]{8,})?$")
	private String phone;
	private ConsentType consentType;
	private String consentDate;
	private boolean mandatoryMeetingHeld16;
	private String mandatoryMeetingDate16;
	private boolean mandatoryMeetingHeld18;
	private String mandatoryMeetingDate18;
	private String esdhCaseReference;
	private String esdhLinkReference;
	private String esdhCaseDisplayName;
}
