package dk.digitalidentity.sbf.controller.rest.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class PartyProgressDto {
	@ToString.Include
	private long courseId;
	@ToString.Include
	private long partyId;
	@ToString.Include
	private long progressId;
	private int selectedRating;
	private String comment;
	private boolean professionalOpinion;
}
