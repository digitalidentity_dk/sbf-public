package dk.digitalidentity.sbf.config.modules;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Links {
	private String agendaLink;
	private String sbsysCaseLinkUrl;
	private String downloadTrialActionForm;
	private String downloadConsentForm;
	private String managementResolveForm;
}
