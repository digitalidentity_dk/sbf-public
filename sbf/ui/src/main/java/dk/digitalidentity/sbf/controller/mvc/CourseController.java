package dk.digitalidentity.sbf.controller.mvc;

import dk.digitalidentity.sbf.config.SBFConfiguration;
import dk.digitalidentity.sbf.controller.mvc.dto.PartyListDto;
import dk.digitalidentity.sbf.dao.model.*;
import dk.digitalidentity.sbf.interceptors.SecurityLogIntercepted;
import dk.digitalidentity.sbf.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class CourseController {

    @Autowired
    private CourseService courseService;

    @Autowired
    private SBFConfiguration sbfConfiguration;

    @SecurityLogIntercepted(operation = "Tilgår forløbssiden", args = {"courseId"})
    @GetMapping("/course")
    public String coursePage(Model model, @RequestParam(required = false) Long courseId) {
        model.addAttribute("courseId", courseId);
        return "course/course";
    }

    @GetMapping("/course/fragments/parties")
    public String loadPartiesFragment(Model model, @RequestParam(required = false) Long courseId) {
        if (courseId != null) {
            Course course = courseService.getById(courseId);
            if (course == null) {
            	model.addAttribute("isCourseDraft", true);
                model.addAttribute("parties", new ArrayList<PartyListDto>());
            } else {
                List<PartyListDto> parties = course.getParties().stream().filter(p -> p.isActive()).map(p -> new PartyListDto(p, sbfConfiguration.getMandatoryMeetings())).collect(Collectors.toList());
                model.addAttribute("parties", parties);
                model.addAttribute("isCourseDraft", course.isDraft());
            }
        } else {
        	model.addAttribute("isCourseDraft", true);
            model.addAttribute("parties", new ArrayList<PartyListDto>());
        }
        return "course/fragments/party :: courseParty";
    }

    @GetMapping("/course/fragments/partyrepresentatives")
    public String loadPartyRepresentativesFragment(Model model, @RequestParam(required = false) Long courseId) {
        if (courseId != null) {
            Course course = courseService.getById(courseId);
            if (course == null) {
                model.addAttribute("partyRepresentatives", new ArrayList<PartyRepresentative>());
            } else {
                List<PartyRepresentative> partyRepresentatives = course.getPartyRepresentatives().stream().filter(p -> p.isActive()).collect(Collectors.toList());
                model.addAttribute("partyRepresentatives", partyRepresentatives);
            }
        } else {
            model.addAttribute("partyRepresentatives", new ArrayList<PartyRepresentative>());
        }
        return "course/fragments/party_representative :: coursePartyRepresentative";
    }

    @GetMapping("/course/fragments/externals")
    public String loadExternalsFragment(Model model, @RequestParam(required = false) Long courseId) {
        if (courseId != null) {
            Course course = courseService.getById(courseId);
            if (course == null) {
                model.addAttribute("externals", new ArrayList<ExternalContact>());
            } else {
                List<ExternalContact> externals = course.getExternalContacts().stream().filter(e -> e.isActive()).collect(Collectors.toList());
                model.addAttribute("externals", externals);
            }
        } else {
            model.addAttribute("externals", new ArrayList<ExternalContact>());
        }
        return "course/fragments/external :: courseExternal";
    }

    @GetMapping("/course/fragments/internals")
    public String loadInternalsFragment(Model model, @RequestParam(required = false) Long courseId) {
        if (courseId != null) {
            Course course = courseService.getById(courseId);
            if (course == null) {
                model.addAttribute("internals", new ArrayList<InternalContact>());
                model.addAttribute("primaryId", null);
            } else {
                List<InternalContact> allInternals = course.getInternalContacts().stream().filter(i -> i.isActive()).collect(Collectors.toList());
                if (course.getPrimaryInternalContact() != null) {
                    long primaryId = course.getPrimaryInternalContact().getId();
                    List<InternalContact> primary = allInternals.stream().filter(i -> i.getId() == primaryId).collect(Collectors.toList());
                    List<InternalContact> nonPrimary = allInternals.stream().filter(i -> i.getId() != primaryId).collect(Collectors.toList());
                    List<InternalContact> internals = new ArrayList<InternalContact>();
                    internals.addAll(primary);
                    internals.addAll(nonPrimary);
                    model.addAttribute("internals", internals);
                    model.addAttribute("primaryId", primaryId);
                } else {
                    model.addAttribute("internals", allInternals);
                    model.addAttribute("primaryId", null);
                }
            }
        } else {
            model.addAttribute("internals", new ArrayList<InternalContact>());
            model.addAttribute("primaryId", null);
        }
        return "course/fragments/internal :: courseInternal";
    }

    @GetMapping("/course/fragments/generalInformation")
    public String loadGeneralInformationFragment(Model model, @RequestParam(required = false) Long courseId) {
        if (courseId != null) {
            Course course = courseService.getById(courseId);
            if (course == null) {
                return "redirect:/error";
            }
            boolean disableCreateBtn = true;
            if (course.getParties() != null && course.getInternalContacts() != null) {
                if (!course.getParties().isEmpty() && !course.getInternalContacts().isEmpty()) {
                    disableCreateBtn = false;
                }
            }
            model.addAttribute("disableCreateBtn", disableCreateBtn);
            model.addAttribute("draft", course.isDraft());

            List<Meeting> meetings = course.getMeetings();
            if (meetings != null && !meetings.isEmpty()) {
            	List<Meeting> notNullMeetings = meetings.stream().filter(m -> m.getMeetingTime() != null).collect(Collectors.toList());
            	notNullMeetings.sort((Meeting m1, Meeting m2) -> m1.getMeetingTime().compareTo(m2.getMeetingTime()));
            	if (!notNullMeetings.isEmpty()) {
            		model.addAttribute("firstMeetingDate", notNullMeetings.get(0).getMeetingTime());
            	}
            } else {
                model.addAttribute("firstMeetingDate", null);
            }
            if( course.getPrimaryInternalContact() != null && course.getPrimaryInternalContact().getEmployee() != null )
            {
                model.addAttribute("primaryContactName",course.getPrimaryInternalContact().getEmployee().getFullName());
            }
            model.addAttribute("active", course.isActive());
        } else {
            model.addAttribute("firstMeetingDate", null);
            model.addAttribute("active", true);
            model.addAttribute("disableCreateBtn", true);
            model.addAttribute("draft", true);
        }
        return "course/fragments/general_information :: generalInformation";
    }

    @GetMapping("/course/fragments/searchresult")
    public String loadSearchResultFragment(Model model, @RequestParam String query, @RequestParam String sort, @RequestParam boolean onlyCoordinator, @RequestParam boolean onlyMine, @RequestParam boolean onlyMultipleParties, @RequestParam boolean onlyOneParty, @RequestParam boolean onlyInactive, @RequestParam boolean onlyActive) {
        model.addAttribute("courses", courseService.search(query, sort, onlyCoordinator, onlyMine, onlyMultipleParties, onlyOneParty, onlyInactive, onlyActive));
        return "course/fragments/search_result :: searchResult";
    }


}
