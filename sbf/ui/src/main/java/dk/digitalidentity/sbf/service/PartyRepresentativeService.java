package dk.digitalidentity.sbf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sbf.dao.PartyRepresentativeDao;
import dk.digitalidentity.sbf.dao.model.PartyRepresentative;

@Service
public class PartyRepresentativeService {
	
	@Autowired
	private PartyRepresentativeDao partyRepresentativeDao;
	
	public PartyRepresentative getById(long id) {
		return partyRepresentativeDao.findById(id);
	}
	
	public PartyRepresentative save(PartyRepresentative partyRepresentative) {
		return partyRepresentativeDao.save(partyRepresentative);
	}
}
