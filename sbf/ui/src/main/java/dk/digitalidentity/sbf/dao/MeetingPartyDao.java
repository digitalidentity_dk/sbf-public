package dk.digitalidentity.sbf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.Meeting;
import dk.digitalidentity.sbf.dao.model.MeetingParty;
import dk.digitalidentity.sbf.dao.model.Party;

public interface MeetingPartyDao extends JpaRepository<MeetingParty, Long>{
	MeetingParty findById(long id);
	MeetingParty findByPartyAndMeeting(Party party, Meeting meeting);
}
