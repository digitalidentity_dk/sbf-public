package dk.digitalidentity.sbf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.Party;


public interface PartyDao extends JpaRepository<Party, Long>{
	Party findById(long id);

    boolean existsByCprAndActiveTrueAndCourse_ActiveTrueAndCourse_DraftFalse(String cpr);
}
