package dk.digitalidentity.sbf.service.post;

import java.util.UUID;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("PostServiceMock")
public class PostServiceMock implements IPostService {

	@Override
	public String sendPost(String cpr, String subject, byte[] file) {
		return UUID.randomUUID().toString();
	}

}
