package dk.digitalidentity.sbf.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import dk.digitalidentity.sbf.dao.SecurityLogDao;
import dk.digitalidentity.sbf.dao.model.SecurityLog;
import dk.digitalidentity.sbf.security.SecurityUtil;

@Service
public class SecurityLogService {
	
	@Autowired
	private SecurityLogDao securityLogDao;
	
	public SecurityLog getById(long id) {
		return securityLogDao.findById(id);
	}
	
	public void log(String operation, String details) {
		SecurityLog securityLog = new SecurityLog();
		securityLog.setUserUuid(SecurityUtil.getUser().getUuid());
		securityLog.setIp(SecurityUtil.getUserIP());
		securityLog.setTimestamp(LocalDateTime.now());
		securityLog.setOperation(operation);
		securityLog.setDetails(details);
		securityLogDao.save(securityLog);
	}
}
