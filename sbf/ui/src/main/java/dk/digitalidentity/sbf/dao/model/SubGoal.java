package dk.digitalidentity.sbf.dao.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


import com.fasterxml.jackson.annotation.JsonBackReference;

import dk.digitalidentity.sbf.dao.model.enums.ClosedReason;
import lombok.Getter;
import lombok.Setter;

@Entity(name="sub_goals")
@Getter
@Setter
public class SubGoal {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column
	@Size(max = 255)
	private String title;
	
	@Column
	@Size(max = 255)
	private String action;
	
	@Column
	private LocalDate startDate;
	
	@Column
	@NotNull
	private boolean closed;
	
	@Column
	@Enumerated(EnumType.STRING)
	private ClosedReason closedReason;
	
	@Column
	@NotNull
	private boolean trialActionInitiated ;
	
	@Column
	@NotNull
	private boolean trialActionApproved;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "goal_id")
	private Goal goal;
	
	@ManyToMany
	@JoinTable(name = "sub_goal_responsible_party", joinColumns = { @JoinColumn(name = "sub_goal_id") }, inverseJoinColumns = { @JoinColumn(name = "party_id") })
	private List<Party> responsibleParties;
	
	@ManyToMany
	@JoinTable(name = "sub_goal_responsible_internal_contact", joinColumns = { @JoinColumn(name = "sub_goal_id") }, inverseJoinColumns = { @JoinColumn(name = "internal_contact_id") })
	private List<InternalContact> responsibleInternalContacts;
	
	@ManyToMany
	@JoinTable(name = "sub_goal_responsible_external_contact", joinColumns = { @JoinColumn(name = "sub_goal_id") }, inverseJoinColumns = { @JoinColumn(name = "external_contact_id") })
	private List<ExternalContact> responsibleExternalContacts;
	
	@ManyToMany
	@JoinTable(name = "sub_goal_responsible_party_representative", joinColumns = { @JoinColumn(name = "sub_goal_id") }, inverseJoinColumns = { @JoinColumn(name = "party_representative_id") })
	private List<PartyRepresentative> responsiblePartyRepresentatives;
}
