package dk.digitalidentity.sbf.service;

import dk.digitalidentity.sbf.dao.model.Party;
import dk.digitalidentity.sbf.service.dto.ESDHCase;
import dk.digitalidentity.sbf.service.dto.ESDHDocument;
import dk.digitalidentity.sbf.service.sbsys.ISBSYSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ESDHService {

    @Autowired
    ISBSYSService sbsysService;

    public ESDHCase createCase(Party party) throws Exception {
        var title = "Sammenhængende borgerforløb - " + party.getFirstName() + " " + party.getSurname();
        var caseWorkerReference = party.getCourse().getPrimaryInternalContact().getEmployee().getUsername();
        var createCaseRespose = sbsysService.createCase(title, party.getCpr(), caseWorkerReference, party.getFirstName() + ' ' + party.getSurname(), party.getAddress(), party.getPostCode(), party.getCity());
        var result = ESDHCase.builder()
                .caseReference(String.valueOf(createCaseRespose.getSagIdentity()))
                .displayName(createCaseRespose.getNummer() + " " + createCaseRespose.getSagsTitel())
                .linkReference( String.valueOf(createCaseRespose.getId()) )
                .build();
        return result;
    }

    public ESDHDocument createDocument(String title, String description, Party party, byte[] file) throws Exception {
        var createDocumentResponse = sbsysService.createDocument(party.getEsdhCaseReference(), title, description, party.getCpr(), file);
        var result = ESDHDocument.builder().documentReference(String.valueOf(createDocumentResponse.getDokumentID())).build();
        return result;
    }

    public void updateCaseWorker(Party party) throws Exception {
        var caseWorkderReference = party.getCourse().getPrimaryInternalContact().getEmployee().getUsername();
        sbsysService.updateCaseWorker(party.getEsdhCaseReference(), caseWorkderReference);
    }

    public void closeCase(String caseReference, String comment) {
        sbsysService.closeCase(caseReference, comment);
    }

    public void openCase(String caseReference) {
        sbsysService.openCase(caseReference);
    }
}