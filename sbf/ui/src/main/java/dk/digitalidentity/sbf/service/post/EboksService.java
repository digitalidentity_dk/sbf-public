package dk.digitalidentity.sbf.service.post;

import dk.digitalidentity.sbf.config.SBFConfiguration;
import dk.digitalidentity.sbf.service.post.dto.EboksMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.xml.security.utils.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
@Profile("!PostServiceMock")
@Slf4j
public class EboksService implements IPostService {

    @Autowired
    private SBFConfiguration sbfConfiguration;

    @Override
    public String sendPost(String cpr, String subject, byte[] file) throws Exception {
        try {
            RestTemplate restTemplate = new RestTemplate();

            log.info("Sending e-boks message: '" + subject + "' to " + maskCpr(cpr));

            String resourceUrl = sbfConfiguration.getEboks().getUrl();
            if (!resourceUrl.endsWith("/")) {
                resourceUrl += "/";
            }
            resourceUrl += "api/remotePrint/SendLetterToCpr";

            String guid = "";

            EboksMessage eBoks = new EboksMessage();
            eBoks.setContentTypeId(sbfConfiguration.getEboks().getMaterialeId());
            eBoks.setCpr(cpr);
            eBoks.setCvr(sbfConfiguration.getEboks().getCvr());
            eBoks.setSenderId(sbfConfiguration.getEboks().getSenderId());
            eBoks.setSubject(subject);
            eBoks.setPdfFileBase64(Base64.encode(file));

            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
            headers.add("AllowPhysicalPrint", "true");
            HttpEntity<EboksMessage> request = new HttpEntity<>(eBoks, headers);

            ResponseEntity<String> response = restTemplate.postForEntity(resourceUrl, request, String.class);
            guid = response.getBody();

            if (response.getStatusCodeValue() != 200) {
                throw new Exception("Unexpected http status code: " + response.getStatusCodeValue());
            }
			return guid;
        } catch (Exception e) {
			throw new Exception( "Failed to send e-boks message to: " + maskCpr(cpr), e);
        }
    }

    private static String maskCpr(String cpr) {
        if (cpr != null && cpr.length() > 6) {
            return cpr.substring(0, 6) + "-XXXX";
        }
        return "";
    }

}
