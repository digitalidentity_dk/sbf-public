package dk.digitalidentity.sbf.service.organization;

import dk.digitalidentity.sbf.config.SBFConfiguration;
import dk.digitalidentity.sbf.service.organization.dto.EmployeeDTO;
import dk.digitalidentity.sbf.service.organization.dto.FKHierarchyWrapper;
import dk.digitalidentity.sbf.service.organization.dto.FKOU;
import dk.digitalidentity.sbf.service.organization.dto.FKPosition;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Service
@Profile("!OrganizationServiceMock")
public class FKOrganizationService implements IOrganizationService {

    @Autowired
    protected SBFConfiguration sbfConfiguration;

    private RestTemplate restTemplate = new RestTemplate();

    private final Pattern namePattern = Pattern.compile("^(?<firstname>.*?)(\\s(?<surname>\\S*?))?$");

    @Override
    public List<EmployeeDTO> getEmployees() throws Exception {
        log.info("Attempting to fetch organisation data from FK Organisation");
        var result = new ArrayList<EmployeeDTO>();


        var headers = new HttpHeaders();
        headers.add("cvr",sbfConfiguration.getOrganization().getOs2SyncCvr());
        HttpEntity<String> request = new HttpEntity<>(headers);

        ResponseEntity<String> keyResponse = restTemplate.exchange(sbfConfiguration.getOrganization().getOs2SyncUrl(), HttpMethod.GET, request, String.class);

        if (keyResponse.getStatusCodeValue() != 200) {
            throw new Exception("Synchronization (getKey) failed: " + keyResponse.getStatusCodeValue());
        }

        String key = keyResponse.getBody();

        ResponseEntity<FKHierarchyWrapper> response = null;
        var maxAttempts = 15;
        for (int i = 1; i <= maxAttempts; i++) {
            Thread.sleep(60 * 1000); // sleep 1 minute before attempting to read again
            try {
                response = restTemplate.getForEntity(sbfConfiguration.getOrganization().getOs2SyncUrl() + "/" + key, FKHierarchyWrapper.class);
                if (response.getStatusCodeValue() != 404) {
                    break;
                }
            } catch (RestClientException e) {
                log.warn("Failed to get hierarchy for key " + key +". Attempt " + i + " of " + maxAttempts + ".");
            }
        }

        if (response.getStatusCodeValue() != 200) {
            throw new Exception("Synchronization (getResponse) failed: " + response.getStatusCodeValue());
        }

        FKHierarchyWrapper hierarchyWrapper = response.getBody();
        if (hierarchyWrapper.getStatus() != 0) {
            throw new Exception("Synchronization (HierarchyStatus) failed: " + hierarchyWrapper.getStatus());
        }

        var hierarchy = hierarchyWrapper.getResult();
        log.info("Successfully fetched orgunit Hierarcy. OU count: " + hierarchy.getOus().size() + ". User count: " + hierarchy.getUsers().size() + "." );

        // create a map of orguuid and uuid-hierachy-path to support whitelisting and blacklisting
        var pathMap = buildPathMap(hierarchy.getOus(), null, "");

        var validOrgUuids = pathMap.entrySet().stream()
                // include orgs in the whitelist (path contains a whitelisted uuid) or all orgs if white-list is empty
                .filter(o -> sbfConfiguration.getOrganization().getOrgunitWhitelist().size() == 0 || sbfConfiguration.getOrganization().getOrgunitWhitelist().stream().anyMatch(wl -> o.getValue().contains(wl)))
                // exclude orgs in the blacklist (path contains a blacklisted uuid)
                .filter(o -> sbfConfiguration.getOrganization().getOrgunitBlacklist().stream().noneMatch(bl -> o.getValue().contains(bl)))
                // select only the key (orgunit uuid) as "output"
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        var validOrgs = hierarchy.getOus().stream().filter(o -> validOrgUuids.stream().anyMatch(vo -> vo.equalsIgnoreCase(o.getUuid()))).collect(Collectors.toList());

        for (var fkUser : hierarchy.getUsers()) {
            var employee = new EmployeeDTO();
            employee.setUuid(fkUser.getUuid());
            employee.setUsername(fkUser.getUserId());
            employee.setEmail(fkUser.getEmail());
            employee.setPhone(fkUser.getTelephone());

            // extract firstname and surname from fullname
            var fullname = fkUser.getName() == null ? "" : fkUser.getName().trim();
            var firstname = fullname;
            var surname = "";
            var matcher = namePattern.matcher(fullname);
            if (matcher.find()) {
                firstname = matcher.group("firstname");
                surname = matcher.group("surname") == null ? "" : matcher.group("surname");
            }
            employee.setFirstName(firstname);
            employee.setSurname(surname);

            for (var position : fkUser.getPositions().stream().sorted(Comparator.comparing(FKPosition::getUuid)).collect(Collectors.toList())) {
                var validOrg = validOrgs.stream().filter(vo -> vo.getUuid().equalsIgnoreCase(position.getUuid())).findFirst();
                if (validOrg.isPresent()) {
                    employee.setPosition(position.getName());
                    employee.setDepartment(validOrg.get().getName());
                    employee.setOrgunitUuid(position.getUuid());
                    employee.setOrgunitPath(pathMap.get(position.getUuid()));
                    // the employee is only added to the result list of employees if employed in a valid org (whitelisted, not blacklsited)
                    result.add(employee);
                    break;
                }
            }
        }
        return result;
    }

    private HashMap<String, String> buildPathMap(List<FKOU> ous, FKOU parentOrg, String parentPath) throws Exception {
        if (parentOrg == null) {
            var rootOrg = ous.stream().filter(o -> o.getParentOU() == null).findFirst();
            if (rootOrg.isEmpty()) {
                throw new Exception("Could not find root OU");
            }
            parentOrg = rootOrg.get();
        }
        var parentUuid = parentOrg.getUuid();
        var path = parentPath + "/" + parentUuid;
        var result = new HashMap<String, String>();
        result.put(parentUuid, path);
        var children = ous.stream().filter(o -> o.getParentOU() != null && o.getParentOU().equalsIgnoreCase(parentUuid)).collect(Collectors.toList());
        for (var child : children) {
            var childMap = buildPathMap(ous, child, path);
            result.putAll(childMap);
        }
        return result;
    }
}

