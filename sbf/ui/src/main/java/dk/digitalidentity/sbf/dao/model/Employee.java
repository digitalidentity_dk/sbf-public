package dk.digitalidentity.sbf.dao.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Entity(name="employees")
@Getter
@Setter
public class Employee {
	@Id
	@Column(name = "employee_uuid")
	private String uuid;
	
	@Column
	@NotNull
	private boolean active;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String username;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String firstName;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String surname;
	
	@Column
	@Size(max = 255)
	private String email;
	
	@Column
	@Size(max = 255)
	private String phone;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String position;

	@Column
	@NotNull
	@Size(max = 255)
	private String department;
	
	@Column
	@Size(max = 255)
	private String orgunitUuid;
	
	@Column
	@Size(max = 255)
	private String orgunitPath;

	public String getFullName() {
		var firstname = getFirstName() != null ? getFirstName() : "";
		var surname = getSurname() != null ? getSurname() : "";
		return firstname + " " + surname;
	}
}
