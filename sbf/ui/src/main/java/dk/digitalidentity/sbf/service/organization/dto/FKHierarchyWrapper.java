package dk.digitalidentity.sbf.service.organization.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FKHierarchyWrapper {
	private FKHierarchy result;
	private long status;
}
