package dk.digitalidentity.sbf.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.Employee;
import dk.digitalidentity.sbf.dao.model.InternalContact;

public interface InternalContactDao extends JpaRepository<InternalContact, Long>{
	InternalContact findById(long id);
	
	List<InternalContact> findByEmployee(Employee employee);
}
