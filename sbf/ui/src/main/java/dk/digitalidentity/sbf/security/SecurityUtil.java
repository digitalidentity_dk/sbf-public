package dk.digitalidentity.sbf.security;

import dk.digitalidentity.saml.model.TokenUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


public class SecurityUtil {

    private static boolean isUserLoggedIn() {
        if (SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().getDetails() != null && SecurityContextHolder.getContext().getAuthentication().getDetails() instanceof TokenUser) {
            return true;
        }

        return false;
    }

    public static boolean hasRole(String role) {
        boolean hasRole = false;
        if (isUserLoggedIn()) {
            for (GrantedAuthority grantedAuthority : (SecurityContextHolder.getContext().getAuthentication()).getAuthorities()) {
                if (grantedAuthority.getAuthority().equals(role)) {
                    hasRole = true;
                }
            }
        }
        return hasRole;
    }

    public static AuthenticatedUser getUser() {
        AuthenticatedUser user = null;

        if (isUserLoggedIn()) {
            user = (AuthenticatedUser) ((TokenUser) SecurityContextHolder.getContext().getAuthentication().getDetails()).getAttributes().get("user");
        }

        return user;
    }
    
    public static String getUserIP() {
        if (RequestContextHolder.getRequestAttributes() == null) {
            return "0.0.0.0";
        }
        var request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return request.getRemoteAddr();
    }

}