package dk.digitalidentity.sbf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sbf.dao.ExternalContactDao;
import dk.digitalidentity.sbf.dao.model.ExternalContact;

@Service
public class ExternalContactService {
	
	@Autowired
	private ExternalContactDao externalContactDao;
	
	public ExternalContact getById(long id) {
		return externalContactDao.findById(id);
	}
	
	public ExternalContact save(ExternalContact externalContact) {
		return externalContactDao.save(externalContact);
	}
}
