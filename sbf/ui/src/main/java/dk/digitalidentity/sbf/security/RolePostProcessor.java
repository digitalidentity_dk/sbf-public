package dk.digitalidentity.sbf.security;

import dk.digitalidentity.saml.extension.SamlLoginPostProcessor;
import dk.digitalidentity.saml.model.TokenUser;
import dk.digitalidentity.sbf.config.RoleConstants;
import dk.digitalidentity.sbf.config.SBFConfiguration;
import dk.digitalidentity.sbf.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
@Slf4j
public class RolePostProcessor implements SamlLoginPostProcessor {

    @Autowired
    SBFConfiguration sbfConfiguration;

    @Autowired
    EmployeeService employeeService;

    @Override
    public void process(TokenUser tokenUser) {
        // get uuid of the user from KOMBIT Adgangsstyring
        var uuid = (String) tokenUser.getAttributes().get(TokenUser.ATTRIBUTE_UUID);

        // lookup employee with given uuid
        var employee = employeeService.getByUuid(uuid);
        if (employee == null) {
            var message = "Kunne ikke finde bruger med uuid: " + uuid;
            log.warn(message);
            throw new UsernameNotFoundException(message);
        }
        var authenticatedUser = new AuthenticatedUser(employee);
        if (!employee.isActive()) {
            var message = "Bruger er ikke aktiv. " + authenticatedUser.getLogInfo();
            log.warn(message);
            throw new DisabledException(message);
        }

        tokenUser.getAttributes().put("user", authenticatedUser);

        List<GrantedAuthority> newAuthorities = new ArrayList<>();
        for (Iterator<? extends GrantedAuthority> iterator = tokenUser.getAuthorities().iterator(); iterator.hasNext(); ) {
            GrantedAuthority grantedAuthority = iterator.next();
            if (sbfConfiguration.getRoles().getCoordinator().equals(grantedAuthority.getAuthority())) {
                newAuthorities.add(new SimpleGrantedAuthority(RoleConstants.COORDINATOR));
            }
        }
        tokenUser.setAuthorities(newAuthorities);

        // currently, all users that log on need to have the coordinator role
        // replace this with more fine-grained role management if more roles are implemented in the future
        if (tokenUser.getAuthorities().stream().noneMatch(a -> a.getAuthority().equals(RoleConstants.COORDINATOR))) {
            var message = "Bruger mangler tovholder rolle. " + authenticatedUser.getLogInfo();
            log.warn(message);
            throw new BadCredentialsException(message);
        }
    }
}
