package dk.digitalidentity.sbf.controller.rest;

import dk.digitalidentity.sbf.dao.model.*;
import dk.digitalidentity.sbf.interceptors.SecurityLogIntercepted;
import dk.digitalidentity.sbf.service.*;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
public class MeetingInviteRestController {

    @Autowired
    private MeetingService meetingService;

    @Autowired
    private MeetingPartyService meetingPartyService;

    @Autowired
    private MeetingPartyRepresentativeService meetingPartyRepresentativeService;

    @Autowired
    private MeetingExternalContactService meetingExternalContactService;

    @Autowired
    private MeetingInternalContactService meetingInternalContactService;

    @PostMapping("/rest/invite/savemeetingtime/{id}")
    public ResponseEntity<?> saveAddress(Model model, @PathVariable long id, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime date) {
        Meeting meeting = meetingService.getById(id);
        if (meeting == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        meeting.setMeetingTime(date);
        meetingService.save(meeting);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ResponseBody
    @PostMapping("/rest/invite/saveaddress/{id}")
    public ResponseEntity<?> saveAddress(Model model, @PathVariable long id, @RequestBody(required = false) String address) {
        Meeting meeting = meetingService.getById(id);
        if (meeting == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        meeting.setAddress(address);
        meetingService.save(meeting);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ResponseBody
    @PostMapping("/rest/invite/saveagenda/{id}")
    public ResponseEntity<?> saveAgenda(Model model, @PathVariable long id, @RequestBody(required = false) String agenda) {
        Meeting meeting = meetingService.getById(id);
        if (meeting == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        meeting.setAgenda(agenda);
        meetingService.save(meeting);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ResponseBody
    @PostMapping("/rest/invite/saveinvitation/{id}")
    public ResponseEntity<?> saveInvitation(Model model, @PathVariable long id, @RequestBody(required = false) String invitation) {
        Meeting meeting = meetingService.getById(id);
        if (meeting == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        meeting.setInvitation(invitation);
        meetingService.save(meeting);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Inviter/af-inviter (møde)part", args = {"id", "invited"})
    @PostMapping("/rest/invite/meetingparty/{id}")
    public ResponseEntity<?> meetingPartyInvite(Model model, @PathVariable long id, @RequestParam boolean invited) {
        MeetingParty meetingParty = meetingPartyService.getById(id);
        if (meetingParty == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        meetingParty.setInvited(invited);
        meetingPartyService.save(meetingParty);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Inviter/af-inviter (møde)partsrepræsentant", args = {"id", "invited"})
    @PostMapping("/rest/invite/meetingpartyrepresentative/{id}")
    public ResponseEntity<?> meetingPartyRepresentativeInvite(Model model, @PathVariable long id, @RequestParam boolean invited) {
        MeetingPartyRepresentative meetingPartyRepresentative = meetingPartyRepresentativeService.getById(id);
        if (meetingPartyRepresentative == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        meetingPartyRepresentative.setInvited(invited);
        meetingPartyRepresentativeService.save(meetingPartyRepresentative);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Inviter/af-inviter (møde)intern kontakt", args = {"id", "invited"})
    @PostMapping("/rest/invite/meetinginternal/{id}")
    public ResponseEntity<?> meetingInternalInvite(Model model, @PathVariable long id, @RequestParam boolean invited) {
        MeetingInternalContact meetingInternalContact = meetingInternalContactService.getById(id);
        if (meetingInternalContact == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        meetingInternalContact.setInvited(invited);
        meetingInternalContactService.save(meetingInternalContact);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Inviter/af-inviter (møde)ekstern kontakt", args = {"id", "invited"})
    @PostMapping("/rest/invite/meetingexternal/{id}")
    public ResponseEntity<?> meetingEternalInvite(Model model, @PathVariable long id, @RequestParam boolean invited) {
        MeetingExternalContact meetingExternalContact = meetingExternalContactService.getById(id);
        if (meetingExternalContact == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        meetingExternalContact.setInvited(invited);
        meetingExternalContactService.save(meetingExternalContact);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Send invitation", args = {"id"})
    @PostMapping("/rest/invite/sendinvite/{id}")
    public ResponseEntity<?> sendInvite(Model model, @PathVariable long id) throws Exception {
        Meeting meeting = meetingService.getById(id);
        if (meeting == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        
        long partiesWithoutCase = meeting.getMeetingParties().stream().map(m -> m.getParty()).filter(p -> p.isActive() && StringUtils.isEmpty(p.getEsdhCaseReference())).count();
        if (partiesWithoutCase > 0) {
        	return new ResponseEntity<>("Der er en eller flere parter, der ikke har en sag tilknyttet, og invitationen kan derfor ikke sendes", HttpStatus.BAD_REQUEST);
        }
        
        meetingService.postAndJournalizeAgenda(meeting);
        meetingService.executePendingJobs();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

