package dk.digitalidentity.sbf.config.modules;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MandatoryMeetings {
	private int sixteenMeetingIntervalStartInMonths = 188;
	private int sixteenMeetingIntervalStopInMonths = 228;
	private int eighteenMeetingIntervalStartInMonths = 205;
	private int eighteenMeetingIntervalStopInMonths = 228;
}
