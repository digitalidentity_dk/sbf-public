package dk.digitalidentity.sbf.dao.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import dk.digitalidentity.sbf.dao.model.enums.ConsentType;
import lombok.Getter;
import lombok.Setter;

@Entity(name="parties")
@Getter
@Setter
public class Party {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column
	@Size(max = 255)
	private String esdhCaseReference;

	@Column
	@Size(max = 255)
	private String esdhLinkReference;

	@Column
	@Size(max = 255)
	private String esdhCaseDisplayName;
	
	@Column
	@NotNull
	private boolean active;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String cpr;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String firstName;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String surname;
	
	@Column
	@NotNull
	private LocalDateTime birthDate;
	
	@Column
	@Size(max = 255)
	private String phone;
	
	@Column
	@Size(max = 255)
	private String email;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String address;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String postCode;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String city;
	
	@Column
	@Enumerated(EnumType.STRING)
	private ConsentType consentType;
	
	@Column
	private LocalDateTime consentDate;
	
	@Column
	@NotNull
	private boolean sixteenYearMeetingHeld;
	
	@Column
	private LocalDateTime sixteenYearMeetingDate;
	
	@Column
	@NotNull
	private boolean eighteenYearMeetingHeld;
	
	@Column
	private LocalDateTime eighteenYearMeetingDate;
	
	@Column
	@NotNull
	private LocalDateTime createdDate;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "course_id")
	private Course course;
	
	@OneToMany(mappedBy = "party", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<Progress> progresses;
}
