package dk.digitalidentity.sbf.service.sbsys.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class CreateDocumentRequest {
    private int sagID;
    private String dokumentNavn;
    private String beskrivelse;
    private CreateDocumentRequestPart sagsPart;
    private CreateDocumentRequestArt dokumentArt;
}