package dk.digitalidentity.sbf.config;

import dk.digitalidentity.sbf.config.modules.*;
import dk.digitalidentity.sbf.service.post.EboksService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "sbf")
public class SBFConfiguration {
	private MandatoryMeetings mandatoryMeetings = new MandatoryMeetings();
	private SBSYS sbsys = new SBSYS();
	private Cpr cpr = new Cpr();
	private Links links = new Links();
	private Organization organization = new Organization();
	private Roles roles = new Roles();
	private Eboks eboks = new Eboks();
	private boolean scheduledJobsEnabled = true;
}
