package dk.digitalidentity.sbf.dao.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Entity(name="security_logs")
@Getter
@Setter
public class SecurityLog {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column
	@NotNull
	@Size(max = 36)
	private String userUuid;
	
	@Column(name = "tts")
	private LocalDateTime timestamp;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String ip;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String operation;
	
	@Column
	@Size(max = 255)
	private String details;
	
}
