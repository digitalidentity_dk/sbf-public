package dk.digitalidentity.sbf.controller.mvc.dto;

public enum ParticipantTypeDto {
	PARTY,
	INTERNAL_CONTACT,
	EXTERNAL_CONTACT,
	PARTY_REPRESENTATIVE
}
