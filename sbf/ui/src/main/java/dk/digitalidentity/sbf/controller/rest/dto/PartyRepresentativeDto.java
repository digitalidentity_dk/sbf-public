package dk.digitalidentity.sbf.controller.rest.dto;

import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class PartyRepresentativeDto {
	@ToString.Include
	private String id;
	private String cpr;
	@ToString.Include
	private String name;
	@ToString.Include
	private String surname;
	private String address;
	private String city;
	private String postCode;
	private String birthday;
	@Pattern(regexp = "^((.+)@(.+))?$")
	private String email;
	@Pattern(regexp = "^([\\d\\+() ]{8,})?$")
	private String phone;
	private String relationship;
}
