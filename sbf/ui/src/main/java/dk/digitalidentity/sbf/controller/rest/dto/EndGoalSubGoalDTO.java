package dk.digitalidentity.sbf.controller.rest.dto;

import dk.digitalidentity.sbf.dao.model.enums.ClosedReason;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class EndGoalSubGoalDTO {
	@ToString.Include
	private ClosedReason closedReason;
}
