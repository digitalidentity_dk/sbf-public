package dk.digitalidentity.sbf.dao.model.enums;

public enum AgendaType {
    AGENDA,
    RECORD
}
