package dk.digitalidentity.sbf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.Goal;

public interface GoalDao extends JpaRepository<Goal, Long>{
	Goal findById(long id);
}
