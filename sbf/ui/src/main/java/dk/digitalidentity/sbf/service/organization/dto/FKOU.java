package dk.digitalidentity.sbf.service.organization.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FKOU {
	private String uuid;
	private String name;
	private String parentOU;
}
