package dk.digitalidentity.sbf.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sbf.dao.InternalContactDao;
import dk.digitalidentity.sbf.dao.model.Employee;
import dk.digitalidentity.sbf.dao.model.InternalContact;

@Service
public class InternalContactService {
	
	@Autowired
	private InternalContactDao internalContactDao;
	
	public InternalContact getById(long id) {
		return internalContactDao.findById(id);
	}
	
	public InternalContact save(InternalContact internalContact) {
		return internalContactDao.save(internalContact);
	}
	
	public List<InternalContact> getByEmployee(Employee employee) {
		return internalContactDao.findByEmployee(employee);
	}
}
