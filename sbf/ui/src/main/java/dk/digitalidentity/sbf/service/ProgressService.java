package dk.digitalidentity.sbf.service;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sbf.dao.ProgressDao;
import dk.digitalidentity.sbf.dao.model.Party;
import dk.digitalidentity.sbf.dao.model.Progress;

@Service
public class ProgressService {
	
	@Autowired
	private ProgressDao progressDao;
	
	public Progress getById(long id) {
		return progressDao.findById(id);
	}
	
	public Progress getByPartyAndProgressDate(Party party, LocalDate date) {
		return progressDao.findByPartyAndProgressDate(party, date);
	}
	
	public Progress save(Progress progress) {
		Progress progressDB = progressDao.findByPartyAndProgressDate(progress.getParty(), progress.getProgressDate());
		if (progressDB == null) {
			return progressDao.save(progress);
		} else {
			progressDB.setProgressComment(progress.getProgressComment());
			progressDB.setScore(progress.getScore());
			progressDB.setScoredByProfessional(progress.isScoredByProfessional());
			return progressDao.save(progressDB);
		}
	}
}
