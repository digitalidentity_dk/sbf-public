package dk.digitalidentity.sbf.service.organization.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FKPosition {
	private String uuid;
	private String name;
}
