package dk.digitalidentity.sbf.service.dto;

import java.util.List;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourseProgressChartDto {
	private List<MeetingDateDto> meetingDates;
	private Set<String> allProgressDates;
	private List<PartyProgressChartDto> parties;

}
