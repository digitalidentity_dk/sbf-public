package dk.digitalidentity.sbf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.Meeting;

public interface MeetingDao extends JpaRepository<Meeting, Long>{
	Meeting findById(long id);
}
