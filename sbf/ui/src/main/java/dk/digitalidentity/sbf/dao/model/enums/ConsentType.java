package dk.digitalidentity.sbf.dao.model.enums;

public enum ConsentType {
	NONE,
	IN_WRITING,
	ORALLY
}
