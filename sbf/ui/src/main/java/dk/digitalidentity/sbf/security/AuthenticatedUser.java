package dk.digitalidentity.sbf.security;

import dk.digitalidentity.sbf.dao.model.Employee;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AuthenticatedUser implements Serializable {

    private String name;
    private String uuid;
    private String username;

    public AuthenticatedUser() {
        ;
    }

    public AuthenticatedUser(Employee employee) {
        this.setName((employee.getFirstName() + " " + employee.getSurname()).trim());
        this.setUuid(employee.getUuid());
        this.setUsername(employee.getUsername());
    }

    public String getLogInfo() {
        var result = "Navn: " + name;
        result += "; Uuid: " + uuid;
        result += "; Brugernavn: " + username;
        return result;
    }
}
