package dk.digitalidentity.sbf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.MeetingInternalContact;

public interface MeetingInternalContactDao extends JpaRepository<MeetingInternalContact, Long>{
	MeetingInternalContact findById(long id);
}
