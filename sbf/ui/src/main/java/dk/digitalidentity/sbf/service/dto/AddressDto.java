package dk.digitalidentity.sbf.service.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressDto {
	private String name;
	private String address;
	private String postCode;
	private String city;

}
