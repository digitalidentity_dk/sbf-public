package dk.digitalidentity.sbf.service.sbsys.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class CreateDocumentRequestPart {
    private int partId;
    private String partType = "Person";
}