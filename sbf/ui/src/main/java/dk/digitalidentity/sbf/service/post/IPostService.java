package dk.digitalidentity.sbf.service.post;

public interface IPostService {
	String sendPost(String cpr, String subject, byte[] file) throws Exception;
}
