package dk.digitalidentity.sbf.service.organization;

import dk.digitalidentity.sbf.service.organization.dto.EmployeeDTO;

import java.util.List;

public interface IOrganizationService {
    List<EmployeeDTO> getEmployees() throws Exception;
}
