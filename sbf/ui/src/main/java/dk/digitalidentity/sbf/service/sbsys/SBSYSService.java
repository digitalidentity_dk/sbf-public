package dk.digitalidentity.sbf.service.sbsys;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.primitives.Ints;
import dk.digitalidentity.sbf.config.SBFConfiguration;
import dk.digitalidentity.sbf.service.sbsys.dto.*;
import dk.digitalidentity.sbf.utility.LogHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Optional;
import java.util.UUID;
import java.util.regex.Pattern;

@Service
@Profile("!SBSYSServiceMock")
@EnableCaching
@Slf4j
public class SBSYSService implements ISBSYSService {

    @Autowired
    private WebClient sbsysWebClient;

    @Autowired
    private SBFConfiguration config;

    @Autowired
    SBSYSService self;

    private ObjectMapper mapper = new ObjectMapper();

    private Pattern cprPattern = Pattern.compile("^(\\d{6})-?(\\d{4})$");

    public SBSYSService() {
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
    }

    private String getFormattedCpr(String cpr) throws Exception {
        var matcher = cprPattern.matcher(cpr);
        if (!matcher.find()) {
            throw new Exception("Unexpected cpr format: " + LogHelper.SafeCPR(cpr));
        }
        return matcher.group(1) + "-" + matcher.group(2);
    }

    @Cacheable(value = "sbsysPersons", unless = "#result == null")
    public Person getPerson(String cpr) throws Exception {
        var formattedCpr = getFormattedCpr(cpr);

        var request = "{'CprNummer':'" + formattedCpr + "'}";

        var url = config.getSbsys().getBaseUrl() + "/api/v12/person/search";
        var response = sbsysWebClient.post()
                .uri(url)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(request)
                .retrieve()
                .bodyToMono(String.class)
                .block();
        var dto = mapper.readValue(response, Person[].class);
        if (dto != null && dto.length >= 1 && dto[0].getCprNummer().equalsIgnoreCase(formattedCpr)) {
            return dto[0];
        }
        log.warn("Could not get person from SBSYS. Cpr: " + LogHelper.SafeCPR(cpr));
        return null;
    }

    @Cacheable(value = "sbsysCaseIds", unless = "#result == null")
    public Integer getCaseIdFromUuid(String uuid) throws JsonProcessingException {

        var url = config.getSbsys().getBaseUrl() + "/api/v12/convert/GetId?type=Sag&uuid=" + uuid;
        var response = sbsysWebClient.get()
                .uri(url)
                .retrieve()
                .bodyToMono(String.class)
                .block();
        var dto = mapper.readValue(response, GetIdResponse.class);
        if( dto != null)
        {
            return dto.getId();
        }
        log.warn("Could not get get case id from SBSYS. uuid: " + uuid);
        return null;
    }

    @Cacheable(value = "sbsysOrgunitIds", unless = "#result == null")
    public Integer getOrgunitIdFromUuid(String uuid) throws JsonProcessingException {

        var url = config.getSbsys().getBaseUrl() + "/api/v12/convert/GetId?type=Ansaettelsessted&uuid=" + uuid;
        var response = sbsysWebClient.get()
                .uri(url)
                .retrieve()
                .bodyToMono(String.class)
                .block();
        var dto = mapper.readValue(response, GetIdResponse.class);
        if( dto != null)
        {
            return dto.getId();
        }
        log.warn("Could not get get Orgunit id from SBSYS. uuid: " + uuid);
        return null;
    }


    @Cacheable(value = "sbsysUsers", unless = "#result == null")
    public Bruger getUser(String username) throws JsonProcessingException {
        var request = "{'LogonId':'" + username.replace("'", "") + "'}";

        var url = config.getSbsys().getBaseUrl() + "/api/v12/bruger/search";
        var response = sbsysWebClient.post()
                .uri(url)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(request)
                .retrieve()
                .bodyToMono(String.class)
                .block();
        var dto = mapper.readValue(response, Bruger[].class);
        for( var dtoUser : dto )
        {
            if( dtoUser.getLogonId().equalsIgnoreCase(username) )
            {
                return dtoUser;
            }
        }
        log.warn("Could not get user from SBSYS. Username: " + username);
        return null;
    }

    public String getOrgunitUuid(String userUuid) throws JsonProcessingException {
        var url = config.getSbsys().getBaseUrl() + "/api/v12/bruger/" + userUuid;
        var response = sbsysWebClient.get()
                .uri(url)
                .retrieve()
                .bodyToMono(String.class)
                .block();
        var dto = mapper.readValue(response, GetOrgunitUuidResponse.class);
        if( dto != null)
        {
            return dto.getAnsaettelsesstedIdentity();
        }
        log.warn("Could not get get orgunit uuid from SBSYS. userUuid: " + userUuid);
        return null;
    }

    private Person createPerson(String cpr, String name, String address, int postCode, String city) throws Exception {
        log.info("Creating person in SBSYS with cpr: " + LogHelper.SafeCPR(cpr));
        var requestObj = new CreatePersonRequest();
        requestObj.setCprNummer( getFormattedCpr(cpr));
        requestObj.setNavn(name);
        requestObj.getAdresse().setAdresse1(address);
        requestObj.getAdresse().setPostNummer(postCode);
        requestObj.getAdresse().setBynavn(city);

        var request = mapper.writeValueAsString(requestObj);

        var url = config.getSbsys().getBaseUrl() + "/api/v12/person";
        var response = sbsysWebClient.post()
                .uri(url)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(request)
                .retrieve()
                .bodyToMono(String.class)
                .block();
        var dto = mapper.readValue(response, Person.class);
        if (dto != null ) {
            return dto;
        } else {
            throw new Exception("Could not create person in SBSYS. Cpr: " + LogHelper.SafeCPR(cpr) + ". Response: " + response);
        }
    }

    @Override
    public CreateCaseResponse createCase(String title, String partyCpr, String caseWorkerUsername, String partyName, String partyAddress, String partyPostCode, String partyCity ) throws Exception {

        var person = self.getPerson(partyCpr);
        if (person == null) {
            // create the party in SBSYS
            var postCode = Optional.ofNullable(partyPostCode).map(Ints::tryParse).orElse(0);
            person = createPerson(partyCpr,partyName,partyAddress,postCode.intValue(),partyCity);
        }
        var caseWorker = self.getUser(caseWorkerUsername);
        if (caseWorker == null) {
            throw new Exception("Could not create case because caseworker could not be found in SBSYS. Username: " + caseWorkerUsername);
        }
        var orgunitUuid = self.getOrgunitUuid(caseWorker.getUuid());

        var requestObj = new CreateCaseRequest();
        requestObj.setSagsTitel(title);
        requestObj.setSkabelonId(config.getSbsys().getCaseTemplateId());
        requestObj.setSagsBehandlerID(caseWorker.getId());
        var party = new CreateCaseRequestPart();
        party.setPartId(person.getId());
        requestObj.setPrimaryPart(party);
        requestObj.getParts().add(party);

        if( orgunitUuid != null ) {
            var orgUnit = new CreateCaseRequestOrgUnit();
            orgUnit.setUuid(orgunitUuid);
            requestObj.setAnsaettelsessted(orgUnit);
        }

        var request = mapper.writeValueAsString(requestObj);

        var url = config.getSbsys().getBaseUrl() + "/api/v12/sag/template";
        var response = sbsysWebClient.post()
                .uri(url)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(request)
                .retrieve()
                .bodyToMono(String.class)
                .block();
        var dto = mapper.readValue(response, CreateCaseResponse.class);
        if (dto != null && dto.getSagIdentity() != null) {
            return dto;
        } else {
            throw new Exception("Could not extract ID after creating new case. Response: " + response);
        }
    }

    @Override
    public CreateDocumentResponse createDocument(String caseReference, String title, String description, String partyCpr, byte[] file) throws Exception {

        var person = self.getPerson(partyCpr);
        if (person == null) {
            throw new Exception("Could not create document because person could not be found in SBSYS. Cpr: " + LogHelper.SafeCPR(partyCpr));
        }

        var requestObj = new CreateDocumentRequest();
        // convert case uuid to id
        var integerId = getCaseIdFromUuid(caseReference);
        requestObj.setSagID(integerId.intValue());
        requestObj.setDokumentNavn(title);
        requestObj.setBeskrivelse(description);
        var party = new CreateDocumentRequestPart();
        party.setPartId(person.getId());
        requestObj.setSagsPart(party);
        var documentArt = new CreateDocumentRequestArt();
        documentArt.setId(config.getSbsys().getDocumentArtId());
        requestObj.setDokumentArt(documentArt);

        var json = mapper.writeValueAsString(requestObj);

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("file", file).filename(UUID.randomUUID().toString() + ".pdf").contentType(MediaType.APPLICATION_PDF);
        builder.part("json", json).contentType(MediaType.APPLICATION_JSON);

        var url = config.getSbsys().getBaseUrl() + "/api/v12/dokument/journaliser";
        var response = sbsysWebClient.post()
                .uri(url)
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .bodyValue(builder.build())
                .retrieve()
                .onStatus(HttpStatus::isError, r -> {
                    logTraceResponse(log, r);
                    return Mono.error(new IllegalStateException("createDocument failed"));
                })
                .bodyToMono(String.class)
                .block();
        var dto = mapper.readValue(response, CreateDocumentResponse.class);
        if (dto != null && dto.getDokumentIdentity() != null) {
            return dto;
        } else {
            throw new Exception("Could not extract ID after creating new document. Response: " + response);
        }
    }

    @Override
    public void updateCaseWorker(String caseReference, String caseWorkerUsername) throws Exception {
        var caseWorker = self.getUser(caseWorkerUsername);
        if (caseWorker == null) {
            throw new Exception("Could not update case worker because case worker could not be found in SBSYS. Username: " + caseWorkerUsername);
        }
        var requestObj = new UpdateCaseWorkerRequest();
        var caseworkerDto = new UpdateCaseWorkerRequestCaseWorker();
        caseworkerDto.setId(caseWorker.getId());
        requestObj.setBehandler(caseworkerDto);

        var orgunitUuid = self.getOrgunitUuid(caseWorker.getUuid());
        if( orgunitUuid != null )
        {
            var orgunitId = self.getOrgunitIdFromUuid(orgunitUuid);
            if( orgunitId != null ) {
                var orgunitDto = new UpdateCaseWorkerRequestOrgUnit();
                orgunitDto.setId(orgunitId);
                requestObj.setAnsaettelsessted(orgunitDto);
            }
        }

        var json = mapper.writeValueAsString(requestObj);
        var url = config.getSbsys().getBaseUrl() + "/api/v12/sag/" + caseReference;
        sbsysWebClient.put()
                .uri(url)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(json)
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    @Override
    public void closeCase(String caseReference, String comment) {
        var request = "{'SagsStatusID': " + config.getSbsys().getCaseClosedStatusId() + ",'Kommentar':'" + comment + "'}";
        var url = config.getSbsys().getBaseUrl() + "/api/v12/sag/" + caseReference + "/status";
        sbsysWebClient.put()
                .uri(url)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(request)
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    @Override
    public void openCase(String caseReference) {
        var comment = "Sag genåbnet fra SBF";
        var request = "{'SagsStatusID': " + config.getSbsys().getCaseActiveStatusId() + ",'Kommentar':'" + comment + "'}";
        var url = config.getSbsys().getBaseUrl() + "/api/v12/sag/" + caseReference + "/status";
        sbsysWebClient.put()
                .uri(url)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(request)
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    public static void logTraceResponse(org.slf4j.Logger log, ClientResponse response) {
        if (log.isTraceEnabled()) {
            log.trace("Response status: {}", response.statusCode());
            log.trace("Response headers: {}", response.headers().asHttpHeaders());
            response.bodyToMono(String.class)
                    .publishOn(Schedulers.boundedElastic())
                    .subscribe(body -> log.trace("Response body: {}", body));
        }
    }

}