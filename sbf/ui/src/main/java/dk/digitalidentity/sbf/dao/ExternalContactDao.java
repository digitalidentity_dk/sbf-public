package dk.digitalidentity.sbf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.ExternalContact;

public interface ExternalContactDao extends JpaRepository<ExternalContact, Long>{
	ExternalContact findById(long id);
}
