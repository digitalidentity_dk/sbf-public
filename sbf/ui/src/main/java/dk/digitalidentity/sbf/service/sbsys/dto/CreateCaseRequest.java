package dk.digitalidentity.sbf.service.sbsys.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class CreateCaseRequest {
    private int skabelonId;
    private String sagsTitel;
    private int sagsBehandlerID;
    private List<CreateCaseRequestPart> parts = new ArrayList<>();
    private CreateCaseRequestPart primaryPart;
    private CreateCaseRequestOrgUnit ansaettelsessted;
}