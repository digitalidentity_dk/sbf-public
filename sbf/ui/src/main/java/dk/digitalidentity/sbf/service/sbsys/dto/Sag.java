package dk.digitalidentity.sbf.service.sbsys.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Sag {
    private String id;
    private String sagsnummer;
    private String titel;
    private String sagstilstand;
    private Part primaerPart;
}