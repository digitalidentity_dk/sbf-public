package dk.digitalidentity.sbf.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sbf.dao.MeetingPartyRepresentativeDao;
import dk.digitalidentity.sbf.dao.model.Meeting;
import dk.digitalidentity.sbf.dao.model.MeetingPartyRepresentative;
import dk.digitalidentity.sbf.dao.model.PartyRepresentative;

@Service
public class MeetingPartyRepresentativeService {
	
	@Autowired
	private MeetingPartyRepresentativeDao meetingPartyRepresentativeDao;
	
	public MeetingPartyRepresentative getById(long id) {
		return meetingPartyRepresentativeDao.findById(id);
	}
	
	public MeetingPartyRepresentative save(MeetingPartyRepresentative meetingPartyRepresentative) {
		return meetingPartyRepresentativeDao.save(meetingPartyRepresentative);
	}
	
	public MeetingPartyRepresentative createAndSave(PartyRepresentative partyRepresentative, Meeting activeMeeting) {
		MeetingPartyRepresentative meetingPartyRepresentative = new MeetingPartyRepresentative();
        meetingPartyRepresentative.setPartyRepresentative(partyRepresentative);
        meetingPartyRepresentative.setMeeting(activeMeeting);
		return meetingPartyRepresentativeDao.save(meetingPartyRepresentative);
	}
	
	public void createAndSaveAll(List<PartyRepresentative> partyRepresentatives, Meeting activeMeeting) {
		for (PartyRepresentative partyRepresentative : partyRepresentatives) {
			MeetingPartyRepresentative meetingPartyRepresentative = new MeetingPartyRepresentative();
			meetingPartyRepresentative.setPartyRepresentative(partyRepresentative);
			meetingPartyRepresentative.setMeeting(activeMeeting);
			meetingPartyRepresentativeDao.save(meetingPartyRepresentative);
		}
	}
}
