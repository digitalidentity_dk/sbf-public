package dk.digitalidentity.sbf.config.modules;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Organization {
    private String os2SyncUrl;
    private String os2SyncCvr;
    private List<String> orgunitWhitelist = new ArrayList<>();
    private List<String> orgunitBlacklist = new ArrayList<>();
}