package dk.digitalidentity.sbf.config.modules;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cpr {
	private String url;
	private String cvr;
}
