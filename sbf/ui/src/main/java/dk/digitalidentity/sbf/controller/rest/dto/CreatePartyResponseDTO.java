package dk.digitalidentity.sbf.controller.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreatePartyResponseDTO {
	private Long courseId;
	private boolean caseCreationFailed;
}
