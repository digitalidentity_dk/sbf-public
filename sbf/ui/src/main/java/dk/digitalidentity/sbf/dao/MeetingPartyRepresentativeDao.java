package dk.digitalidentity.sbf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.MeetingPartyRepresentative;

public interface MeetingPartyRepresentativeDao extends JpaRepository<MeetingPartyRepresentative, Long>{
	MeetingPartyRepresentative findById(long id);
}
