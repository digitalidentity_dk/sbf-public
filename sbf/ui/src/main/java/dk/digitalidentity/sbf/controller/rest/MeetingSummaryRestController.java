package dk.digitalidentity.sbf.controller.rest;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dk.digitalidentity.sbf.controller.mvc.dto.ParticipantSelectDto;
import dk.digitalidentity.sbf.controller.rest.dto.EndGoalSubGoalDTO;
import dk.digitalidentity.sbf.dao.model.Course;
import dk.digitalidentity.sbf.dao.model.ExternalContact;
import dk.digitalidentity.sbf.dao.model.Goal;
import dk.digitalidentity.sbf.dao.model.InternalContact;
import dk.digitalidentity.sbf.dao.model.Meeting;
import dk.digitalidentity.sbf.dao.model.MeetingExternalContact;
import dk.digitalidentity.sbf.dao.model.MeetingInternalContact;
import dk.digitalidentity.sbf.dao.model.MeetingParty;
import dk.digitalidentity.sbf.dao.model.MeetingPartyRepresentative;
import dk.digitalidentity.sbf.dao.model.Party;
import dk.digitalidentity.sbf.dao.model.PartyRepresentative;
import dk.digitalidentity.sbf.dao.model.SubGoal;
import dk.digitalidentity.sbf.interceptors.SecurityLogIntercepted;
import dk.digitalidentity.sbf.service.CourseService;
import dk.digitalidentity.sbf.service.ExternalContactService;
import dk.digitalidentity.sbf.service.GoalService;
import dk.digitalidentity.sbf.service.InternalContactService;
import dk.digitalidentity.sbf.service.MeetingExternalContactService;
import dk.digitalidentity.sbf.service.MeetingInternalContactService;
import dk.digitalidentity.sbf.service.MeetingPartyRepresentativeService;
import dk.digitalidentity.sbf.service.MeetingPartyService;
import dk.digitalidentity.sbf.service.MeetingService;
import dk.digitalidentity.sbf.service.PartyRepresentativeService;
import dk.digitalidentity.sbf.service.PartyService;
import dk.digitalidentity.sbf.service.SubGoalService;

@RestController
public class MeetingSummaryRestController {
	
	@Autowired
	private MeetingService meetingService;
	
	@Autowired
	private CourseService courseService;
	
	@Autowired
    private MeetingPartyService meetingPartyService;
    
    @Autowired
    private MeetingPartyRepresentativeService meetingPartyRepresentativeService;
    
    @Autowired
    private MeetingExternalContactService meetingExternalContactService;
    
    @Autowired
    private MeetingInternalContactService meetingInternalContactService;
    
    @Autowired
    private GoalService goalService;
    
    @Autowired
    private SubGoalService subGoalService;
    
    @Autowired
    private PartyService partyService;
    
    @Autowired
    private InternalContactService internalContactService;
    
    @Autowired
    private ExternalContactService externalContactService;
    
    @Autowired
    private PartyRepresentativeService partyRepresentativeService;
	
	@ResponseBody
	@PostMapping("/rest/summary/savenote/{id}")
	public ResponseEntity<?> saveNote(Model model, @PathVariable long id, @RequestBody String note) {
		Meeting meeting = meetingService.getById(id);
		if (meeting == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Course course = meeting.getCourse();
		course.setNotes(note);
		courseService.save(course);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@SecurityLogIntercepted(operation = "(Møde)Part deltog/deltog ikke", args = {"id", "attended"})
	@PostMapping("/rest/summary/meetingparty/{id}")
	public ResponseEntity<?> meetingPartyAttend(Model model, @PathVariable long id, @RequestParam boolean attended) {
		MeetingParty meetingParty = meetingPartyService.getById(id);
		if (meetingParty == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		meetingParty.setAttended(attended);
		meetingPartyService.save(meetingParty);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@SecurityLogIntercepted(operation = "(Møde)Partsrepræsentant deltog/deltog ikke", args = {"id", "attended"})
	@PostMapping("/rest/summary/meetingpartyrepresentative/{id}")
	public ResponseEntity<?> meetingPartyRepresentativeAttend(Model model, @PathVariable long id, @RequestParam boolean attended) {
		MeetingPartyRepresentative meetingPartyRepresentative = meetingPartyRepresentativeService.getById(id);
		if (meetingPartyRepresentative == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		meetingPartyRepresentative.setAttended(attended);
		meetingPartyRepresentativeService.save(meetingPartyRepresentative);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@SecurityLogIntercepted(operation = "(Møde)Intern kontakt deltog/deltog ikke", args = {"id", "attended"})
	@PostMapping("/rest/summary/meetinginternal/{id}")
	public ResponseEntity<?> meetingInternalAttend(Model model, @PathVariable long id, @RequestParam boolean attended) {
		MeetingInternalContact meetingInternalContact = meetingInternalContactService.getById(id);
		if (meetingInternalContact == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		meetingInternalContact.setAttended(attended);
		meetingInternalContactService.save(meetingInternalContact);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@SecurityLogIntercepted(operation = "(Møde)Ekstern kontakt deltog/deltog ikke", args = {"id", "attended"})
	@PostMapping("/rest/summary/meetingexternal/{id}")
	public ResponseEntity<?> meetingEternalAttend(Model model, @PathVariable long id, @RequestParam boolean attended) {
		MeetingExternalContact meetingExternalContact = meetingExternalContactService.getById(id);
		if (meetingExternalContact == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		meetingExternalContact.setAttended(attended);
		meetingExternalContactService.save(meetingExternalContact);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@SecurityLogIntercepted(operation = "(Møde)Part registrer drøm", args = {"id", "dream"})
	@ResponseBody
	@PostMapping("/rest/summary/savedream/{id}")
	public ResponseEntity<?> saveDream(Model model, @PathVariable long id, @RequestBody(required = false) String dream) {
		MeetingParty meetingParty = meetingPartyService.getById(id);
		if (meetingParty == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		meetingParty.setDream(dream);
		meetingPartyService.save(meetingParty);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@SecurityLogIntercepted(operation = "Afslut mål", args = {"id", "dto"})
	@ResponseBody
	@PostMapping("/rest/summary/goal/{id}/close")
	public ResponseEntity<?> closeGoal(Model model, @PathVariable long id, @RequestBody EndGoalSubGoalDTO dto) {
		Goal goal = goalService.getById(id);
		if (goal == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		List<SubGoal> activeSubgoals = goal.getSubGoals().stream().filter(s -> !s.isClosed()).collect(Collectors.toList());
		if (!activeSubgoals.isEmpty()) {
			return new ResponseEntity<>("Alle målets tiltag skal afsluttes før målet kan afsluttes.", HttpStatus.BAD_REQUEST);
		}
		
		goal.setActive(false);
		goal.setClosedReason(dto.getClosedReason());
		goalService.save(goal);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@SecurityLogIntercepted(operation = "Opret mål", args = {"id", "title", "objective"})
	@PostMapping("/rest/summary/addgoal/{id}")
	public ResponseEntity<?> addGoal(Model model, @PathVariable long id, @RequestParam String title, @RequestParam String objective) {
		MeetingParty meetingParty = meetingPartyService.getById(id);
		if (meetingParty == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Goal goal = new Goal();
		goal.setActive(true);
		goal.setMeetingParty(meetingParty);
		goal.setObjective(objective);
		goal.setTitle(title);
		goalService.save(goal);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ResponseBody
	@PostMapping("/rest/summary/subgoals/savetitle/{id}")
	public ResponseEntity<?> saveTitle(Model model, @PathVariable long id, @RequestBody(required = false) String title) {
		SubGoal subGoal = subGoalService.getById(id);
		if (subGoal == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		subGoal.setTitle(title);
		subGoalService.save(subGoal);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ResponseBody
	@PostMapping("/rest/summary/subgoals/saveaction/{id}")
	public ResponseEntity<?> saveAction(Model model, @PathVariable long id, @RequestBody(required = false) String action) {
		SubGoal subGoal = subGoalService.getById(id);
		if (subGoal == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		subGoal.setAction(action);
		subGoalService.save(subGoal);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping("/rest/summary/subgoals/savestartdate/{id}")
	public ResponseEntity<?> saveStartDate(Model model, @PathVariable long id, @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") Date date) {
		SubGoal subGoal = subGoalService.getById(id);
		if (subGoal == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		LocalDate startDate = convertToLocalDate(date);
		
		subGoal.setStartDate(startDate);
		subGoalService.save(subGoal);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@SecurityLogIntercepted(operation = "Afslut tiltag", args = {"id", "dto"})
	@ResponseBody
	@PostMapping("/rest/summary/subgoal/{id}/close")
	public ResponseEntity<?> closeSubGoal(Model model, @PathVariable long id, @RequestBody EndGoalSubGoalDTO dto) {
		SubGoal subGoal = subGoalService.getById(id);
		if (subGoal == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		subGoal.setClosed(true);
		subGoal.setClosedReason(dto.getClosedReason());
		subGoalService.save(subGoal);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping("/rest/summary/subgoals/savetrialactioninitiated/{id}")
	public ResponseEntity<?> saveTrialActionInitiated(Model model, @PathVariable long id, @RequestParam boolean checked) {
		SubGoal subGoal = subGoalService.getById(id);
		if (subGoal == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		subGoal.setTrialActionInitiated(checked);
		subGoalService.save(subGoal);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping("/rest/summary/subgoals/savetrialactionapproved/{id}")
	public ResponseEntity<?> saveTrialActionApproved(Model model, @PathVariable long id, @RequestParam boolean checked) {
		SubGoal subGoal = subGoalService.getById(id);
		if (subGoal == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		subGoal.setTrialActionApproved(checked);
		subGoalService.save(subGoal);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@SecurityLogIntercepted(operation = "Fjern ansvarlig fra tiltag", args = {"id", "dto"})
	@ResponseBody
	@PostMapping("/rest/summary/subgoal/{id}/removeresponsible")
	public ResponseEntity<?> removeResponsible(Model model, @PathVariable long id, @RequestBody ParticipantSelectDto dto) {
		SubGoal subGoal = subGoalService.getById(id);
		if (subGoal == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		switch (dto.getType()) {
	    	case PARTY:
	    		Party party = partyService.getById(dto.getId());
	    		if (subGoal.getResponsibleParties().contains(party)) {
	    			subGoal.getResponsibleParties().remove(party);
	    		}
	    		break;
	    	case INTERNAL_CONTACT:
	    		InternalContact internalContact = internalContactService.getById(dto.getId());
	    		if (subGoal.getResponsibleInternalContacts().contains(internalContact)) {
	    			subGoal.getResponsibleInternalContacts().remove(internalContact);
	    		}
	    		break;
	    	case EXTERNAL_CONTACT:
	    		ExternalContact externalContact = externalContactService.getById(dto.getId());
	    		if (subGoal.getResponsibleExternalContacts().contains(externalContact)) {
	    			subGoal.getResponsibleExternalContacts().remove(externalContact);
	    		}
	    		break;
	    	case PARTY_REPRESENTATIVE:
        		PartyRepresentative partyRepresentative = partyRepresentativeService.getById(dto.getId());
        		if (subGoal.getResponsiblePartyRepresentatives().contains(partyRepresentative)) {
	    			subGoal.getResponsiblePartyRepresentatives().remove(partyRepresentative);
	    		}
        		break;
		}
		
		subGoalService.save(subGoal);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@SecurityLogIntercepted(operation = "Tilføj ansvarlig til tiltag", args = {"id", "dto"})
	@ResponseBody
	@PostMapping("/rest/summary/subgoal/{id}/addresponsible")
	public ResponseEntity<?> addResponsible(Model model, @PathVariable long id, @RequestBody ParticipantSelectDto dto) {
		SubGoal subGoal = subGoalService.getById(id);
		if (subGoal == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		switch (dto.getType()) {
        	case PARTY:
        		Party party = partyService.getById(dto.getId());
        		subGoal.getResponsibleParties().add(party);
        		break;
        	case INTERNAL_CONTACT:
        		InternalContact internalContact = internalContactService.getById(dto.getId());
        		subGoal.getResponsibleInternalContacts().add(internalContact);
        		break;
        	case EXTERNAL_CONTACT:
        		ExternalContact externalContact = externalContactService.getById(dto.getId());
        		subGoal.getResponsibleExternalContacts().add(externalContact);
        		break;
        	case PARTY_REPRESENTATIVE:
        		PartyRepresentative partyRepresentative = partyRepresentativeService.getById(dto.getId());
        		subGoal.getResponsiblePartyRepresentatives().add(partyRepresentative);
        		break;
		}
		
		subGoalService.save(subGoal);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@SecurityLogIntercepted(operation = "Opret tiltag på kategori", args = {"id"})
	@PostMapping("/rest/summary/goal/{id}/addsubgoal")
	public ResponseEntity<?> closeGoal(Model model, @PathVariable long id) {
		Goal goal = goalService.getById(id);
		if (goal == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		SubGoal subGoal = new SubGoal();
		subGoal.setClosed(false);
		subGoal.setGoal(goal);

		subGoalService.save(subGoal);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ResponseBody
	@PostMapping("/rest/summary/goal/{id}/saveobjective")
	public ResponseEntity<?> closeSubGoal(Model model, @PathVariable long id, @RequestBody String objective) {
		Goal goal = goalService.getById(id);
		if (goal == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		goal.setObjective(objective);
		goalService.save(goal);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
	private LocalDate convertToLocalDate(Date dateToConvert) {
	    return Instant.ofEpochMilli(dateToConvert.getTime())
	      .atZone(ZoneId.systemDefault())
	      .toLocalDate();
	}
}

