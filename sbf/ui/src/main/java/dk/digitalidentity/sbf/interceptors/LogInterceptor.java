package dk.digitalidentity.sbf.interceptors;

import java.lang.reflect.Method;
import java.util.Objects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dk.digitalidentity.sbf.service.SecurityLogService;
import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@Slf4j
public class LogInterceptor {
	@Autowired
	private SecurityLogService securityLogService;

	@Before(value = "execution(* dk.digitalidentity.sbf.controller..*.*(..)) && @annotation(SecurityLogIntercepted)")
	public void interceptBefore(JoinPoint jp) {
		MethodSignature signature = (MethodSignature) jp.getSignature();
		Method method = signature.getMethod();

		SecurityLogIntercepted annotation = method.getAnnotation(SecurityLogIntercepted.class);
		String operation = annotation.operation();
		String[] args = annotation.args();

		Object[] actualArgs = jp.getArgs();
		String[] methodParams = signature.getParameterNames();
		String details = "";

		for (String arg : args) {
			boolean found = false;
			Object obj = null;

			for (int i = 0; i < methodParams.length; i++) {
				if (Objects.equals(arg, methodParams[i])) {
					obj = actualArgs[i];
					found = true;
					break;
				}
			}

			if (found) {
				if (obj != null) {
					details += arg + ": " + obj.toString() + " ";
				}
			} else {
				log.error("No argument with the given name " + arg + " supplied for operation " + operation);
			}
		}

		securityLogService.log(operation, details);
	}
}
