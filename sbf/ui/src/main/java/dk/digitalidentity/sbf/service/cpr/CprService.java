package dk.digitalidentity.sbf.service.cpr;

import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import dk.digitalidentity.sbf.service.cpr.dto.CPRServiceLookupDTO;
import dk.digitalidentity.sbf.service.cpr.dto.CprLookupDto;
import dk.digitalidentity.sbf.utility.LogHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Profile("!CprServiceMock")
public class CprService extends AbstractCprService implements ICprService {

	@Override
	public CprLookupDto cprLookup(String cpr) {
		if (!validCpr(cpr)) {
			return null;
		}
		
		var restTemplate = new RestTemplate();

		String cprResourceUrl = sbfConfiguration.getCpr().getUrl();
		if (!cprResourceUrl.endsWith("/")) {
			cprResourceUrl += "/";
		}
		cprResourceUrl += "api/person?cpr=" + cpr + "&cvr=" + sbfConfiguration.getCpr().getCvr();

		try {
			ResponseEntity<CPRServiceLookupDTO> response = restTemplate.getForEntity(cprResourceUrl, CPRServiceLookupDTO.class);
			CPRServiceLookupDTO dto = response.getBody();
			CprLookupDto result = null;
			if(dto != null) {
				result = new CprLookupDto();
				//Address
				result.setAddress(dto.getStreet());
				result.setCity(dto.getCity());
				result.setPostCode(dto.getPostalCode());
				//Name
				result.setName(dto.getFirstname());
				result.setSurname(dto.getLastname());
				result.setCpr(cpr);
				//Rest
				result.setBirthday(getBirthDateFromCpr(cpr));
				result.setShow16(shouldShow16(result.getBirthday()));
				result.setShow18(shouldShow18(result.getBirthday()));
			}

			return result;
		}
		catch (RestClientException ex) {
			log.warn("Failed to lookup: " + LogHelper.SafeCPR(cpr), ex);

			return null;
		}
	}
}