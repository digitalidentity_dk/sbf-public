package dk.digitalidentity.sbf.service.cpr;

import dk.digitalidentity.sbf.service.cpr.dto.CprLookupDto;

public interface ICprService {
    CprLookupDto cprLookup(String cpr);
    boolean validCpr(String cpr);
}
