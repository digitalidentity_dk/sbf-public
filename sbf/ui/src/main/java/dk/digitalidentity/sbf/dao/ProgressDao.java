package dk.digitalidentity.sbf.dao;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.Party;
import dk.digitalidentity.sbf.dao.model.Progress;


public interface ProgressDao extends JpaRepository<Progress, Long>{
	Progress findById(long id);
	Progress findByPartyAndProgressDate(Party party, LocalDate progressDate);
}
