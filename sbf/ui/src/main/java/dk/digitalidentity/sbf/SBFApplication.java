package dk.digitalidentity.sbf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "dk.digitalidentity" })
public class SBFApplication {

	public static void main(String[] args) {
		SpringApplication.run(SBFApplication.class, args);
	}

}
