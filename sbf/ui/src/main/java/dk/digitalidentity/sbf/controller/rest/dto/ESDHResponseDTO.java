package dk.digitalidentity.sbf.controller.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ESDHResponseDTO {
	private String caseRef;
	private String linkRef;
	private String esdhCaseDisplayName;
}
