package dk.digitalidentity.sbf.controller.rest.dto;

import dk.digitalidentity.sbf.service.cpr.dto.CprLookupDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CprESDHDto {
	private CprLookupDto cprLookupDto;
	private boolean belongsToActiveCourse;
	private boolean previouslyAssociated;
}
