package dk.digitalidentity.sbf.service.cpr.dto;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CprLookupDto {
	private String name;
	private String surname;
	private LocalDate birthday;
	private String address;
	private String city;
	private String postCode;
	private String cpr;
	private boolean show16 = false;
	private boolean show18 = false;

}
