package dk.digitalidentity.sbf.service.organization.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class FKUser {
	private String uuid;
	private String name;
	private String userId;
	private String email;
	private String telephone;
	private List<FKPosition> positions;
}
