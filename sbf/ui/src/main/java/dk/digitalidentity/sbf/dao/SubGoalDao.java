package dk.digitalidentity.sbf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.SubGoal;

public interface SubGoalDao extends JpaRepository<SubGoal, Long>{
	SubGoal findById(long id);
}
