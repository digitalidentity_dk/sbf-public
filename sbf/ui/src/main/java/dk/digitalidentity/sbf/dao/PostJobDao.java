package dk.digitalidentity.sbf.dao;

import dk.digitalidentity.sbf.dao.model.PostJob;
import dk.digitalidentity.sbf.dao.model.enums.JobStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface PostJobDao extends JpaRepository<PostJob, Long> {
    List<PostJob> findByStatus(JobStatus status);
}
