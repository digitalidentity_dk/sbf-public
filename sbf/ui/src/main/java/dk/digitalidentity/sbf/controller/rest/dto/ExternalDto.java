package dk.digitalidentity.sbf.controller.rest.dto;

import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class ExternalDto {
	@ToString.Include
	private Long id;
	@ToString.Include
	private String name;
	@ToString.Include
	private String surname;
	@Pattern(regexp = "^((.+)@(.+))?$")
	private String email;
	@Pattern(regexp = "^([\\d\\+() ]{8,})?$")
	private String phone;
	private String relationship;
}
