package dk.digitalidentity.sbf.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sbf.dao.MeetingExternalContactDao;
import dk.digitalidentity.sbf.dao.model.ExternalContact;
import dk.digitalidentity.sbf.dao.model.Meeting;
import dk.digitalidentity.sbf.dao.model.MeetingExternalContact;

@Service
public class MeetingExternalContactService {
	
	@Autowired
	private MeetingExternalContactDao meetingExternalContactDao;
	
	public MeetingExternalContact getById(long id) {
		return meetingExternalContactDao.findById(id);
	}
	
	public MeetingExternalContact save(MeetingExternalContact meetingExternalContact) {
		return meetingExternalContactDao.save(meetingExternalContact);
	}
	
	public MeetingExternalContact createAndSave(ExternalContact externalContact, Meeting activeMeeting) {
		MeetingExternalContact meetingExternalContact = new MeetingExternalContact();
        meetingExternalContact.setExternalContact(externalContact);
        meetingExternalContact.setMeeting(activeMeeting);
		return meetingExternalContactDao.save(meetingExternalContact);
	}
	
	public void createAndSaveAll(List<ExternalContact> externalContacts, Meeting activeMeeting) {
		for (ExternalContact externalContact : externalContacts) {
			MeetingExternalContact meetingExternalContact = new MeetingExternalContact();
            meetingExternalContact.setExternalContact(externalContact);
            meetingExternalContact.setMeeting(activeMeeting);
            meetingExternalContactDao.save(meetingExternalContact);
		}
	}
}
