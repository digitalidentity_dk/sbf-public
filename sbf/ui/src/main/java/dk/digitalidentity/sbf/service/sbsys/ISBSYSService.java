package dk.digitalidentity.sbf.service.sbsys;

import dk.digitalidentity.sbf.service.sbsys.dto.CreateCaseResponse;
import dk.digitalidentity.sbf.service.sbsys.dto.CreateDocumentResponse;

public interface ISBSYSService {

    CreateCaseResponse createCase(String title, String partyCpr, String caseWorkerUsername, String partyName, String partyAddress, String partyPostCode, String partyCity ) throws Exception;

    CreateDocumentResponse createDocument(String caseReference, String title, String description, String partyCpr, byte[] file) throws Exception;

    void updateCaseWorker(String caseUuid, String caseWorkerUsername) throws Exception;

    void closeCase(String caseReference, String comment);

    void openCase(String caseReference);
}