package dk.digitalidentity.sbf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.PartyRepresentative;


public interface PartyRepresentativeDao extends JpaRepository<PartyRepresentative, Long>{
	PartyRepresentative findById(long id);
}
