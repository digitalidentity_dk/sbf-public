package dk.digitalidentity.sbf.service.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ESDHDocument {
    private String documentReference;
}
