package dk.digitalidentity.sbf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import dk.digitalidentity.sbf.dao.SubGoalDao;
import dk.digitalidentity.sbf.dao.model.SubGoal;

@Service
public class SubGoalService {
	
	@Autowired
	private SubGoalDao subGoalDao;
	
	public SubGoal getById(long id) {
		return subGoalDao.findById(id);
	}
	
	public SubGoal save(SubGoal subGoal) {
		return subGoalDao.save(subGoal);
	}
}
