package dk.digitalidentity.sbf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.MeetingExternalContact;

public interface MeetingExternalContactDao extends JpaRepository<MeetingExternalContact, Long>{
	MeetingExternalContact findById(long id);
}
