package dk.digitalidentity.sbf.controller.mvc;

import java.io.IOException;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lowagie.text.DocumentException;

import dk.digitalidentity.sbf.dao.model.Course;
import dk.digitalidentity.sbf.dao.model.Meeting;
import dk.digitalidentity.sbf.interceptors.SecurityLogIntercepted;
import dk.digitalidentity.sbf.service.CourseService;
import dk.digitalidentity.sbf.service.MeetingService;
import dk.digitalidentity.sbf.service.dto.AddressDto;

@Controller
public class MeetingJournalizeController {
	
	@Autowired
	private CourseService courseService;
	
	@Autowired
	private MeetingService meetingService;
	
	@SecurityLogIntercepted(operation = "Tilgår send og journaliser siden", args = {"courseId"})
	@GetMapping("/journalize")
	public String coursePage(Model model, @RequestParam Long courseId) {
		Course course = courseService.getById(courseId);
		
		if (course == null) {
			return "redirect:/error";
		}
		
		model.addAttribute("meeting", course.getActiveMeeting());
		var parties = course.getActiveMeeting().getMeetingParties().stream().filter(p -> p.getParty().isActive()).collect(Collectors.toList());
		model.addAttribute("parties", parties);

		var meetingPartyRepresentatives = course.getActiveMeeting().getMeetingPartyRepresentatives().stream().filter(p -> p.getPartyRepresentative().isActive()).collect(Collectors.toList());
		model.addAttribute("meetingPartyRepresentatives", meetingPartyRepresentatives);


		return "course/meeting_journalize";
	}
	
	@GetMapping("/journalize/pdf")
    public ResponseEntity<ByteArrayResource> getMeetingInvitePdf(@RequestParam Long id, HttpServletResponse response) throws IOException, DocumentException {
        Meeting meeting = meetingService.getById(id);
        if (meeting == null) {
        	return ResponseEntity.badRequest().build();
        }

		AddressDto addressDto = new AddressDto();
		addressDto.setName("<<Navn>>");
		addressDto.setAddress("<<Gade>>");
		addressDto.setPostCode("<<PostNr>>");
		addressDto.setCity("<<By>>");

		byte[] byteData = meetingService.getRecordPdf(meeting, addressDto);
        ByteArrayResource resource = new ByteArrayResource(byteData);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=følgebrev_" + id + ".pdf");
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(byteData.length)
                .contentType(MediaType.parseMediaType("application/pdf"))
                .body(resource);
    }
}
