package dk.digitalidentity.sbf.service.organization;

import dk.digitalidentity.sbf.service.organization.dto.EmployeeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@Profile("OrganizationServiceMock")
public class OrganizationServiceMock implements IOrganizationService {
    @Override
    public List<EmployeeDTO> getEmployees() throws Exception {

        var result = new ArrayList<EmployeeDTO>();

        var peter = new EmployeeDTO();
        peter.setUuid("3b1bcf21-ea68-47f2-9656-c76828a64e1d");
        peter.setUsername("pso");
        peter.setFirstName("Peter");
        peter.setSurname("Søgaard");
        peter.setEmail("pso@digital-identity.dk");
        peter.setPhone("12345678");
        peter.setPosition("Udvikler");
        peter.setDepartment("Udvikling");
        peter.setOrgunitUuid("1234567h-ej45-98k1-ooo1-123456789efh");
        peter.setOrgunitPath("path");
        result.add(peter);

        var amalie = new EmployeeDTO();
        amalie.setUuid("999a35e8-ac8e-4d8a-8cfc-c73aa0575eee");
        amalie.setUsername("abo");
        amalie.setFirstName("Amalie");
        amalie.setSurname("Bojsen");
        amalie.setEmail("abo@digital-identity.dk");
        amalie.setPhone("12345678");
        amalie.setPosition("Udvikler");
        amalie.setDepartment("Udvikling");
        amalie.setOrgunitUuid("1234567h-ej45-98k1-ooo1-123456789efg");
        amalie.setOrgunitPath("path");
        result.add(amalie);

        var sbf = new EmployeeDTO();
        sbf.setUuid("b881d0c2-5b9a-479e-ad8d-9a605baf6c54");
        sbf.setUsername("sbf");
        sbf.setFirstName("SBF");
        sbf.setSurname("Test");
        sbf.setEmail("sbf@digital-identity.dk");
        sbf.setPhone("12345678");
        sbf.setPosition("Tester");
        sbf.setDepartment("Testafdelingen");
        sbf.setOrgunitUuid("1234567h-ej45-98k1-ooo1-123456789efi");
        sbf.setOrgunitPath("path");
        result.add(sbf);

        return result;
    }
}