package dk.digitalidentity.sbf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.SecurityLog;


public interface SecurityLogDao extends JpaRepository<SecurityLog, Long>{
	SecurityLog findById(long id);
}
