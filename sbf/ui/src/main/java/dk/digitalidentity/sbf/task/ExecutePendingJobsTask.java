package dk.digitalidentity.sbf.task;

import dk.digitalidentity.sbf.config.SBFConfiguration;
import dk.digitalidentity.sbf.service.CourseService;
import dk.digitalidentity.sbf.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class ExecutePendingJobsTask {
	@Autowired
	private MeetingService meetingService;

	@Autowired
	SBFConfiguration sbfConfiguration;

	@Scheduled(cron = "${cron.ExecutePendingJobsTask:0 0/5 * * * ?}")
	public void executePendingJobs() {
		if( sbfConfiguration.isScheduledJobsEnabled() ) {
			meetingService.executePendingJobs();
		}
	}
}
