package dk.digitalidentity.sbf.service.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MeetingDateDto {
	private String value;
	private String text;
	
	public MeetingDateDto(LocalDateTime meetingTime) {
		String date = meetingTime.toString().split("T")[0];
		String[] dateFragments = date.split("-");
		this.value = date;
		this.text = "Møde: " + dateFragments[2] + "-" + dateFragments[1] + "-" + dateFragments[0]; 
	}
}
