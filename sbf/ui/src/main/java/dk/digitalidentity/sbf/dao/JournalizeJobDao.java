package dk.digitalidentity.sbf.dao;

import dk.digitalidentity.sbf.dao.model.JournalizeJob;
import dk.digitalidentity.sbf.dao.model.enums.JobStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface JournalizeJobDao extends JpaRepository<JournalizeJob, Long> {
    List<JournalizeJob> findByStatus(JobStatus status);
}
