package dk.digitalidentity.sbf.service;

import com.lowagie.text.DocumentException;
import dk.digitalidentity.sbf.dao.JournalizeJobDao;
import dk.digitalidentity.sbf.dao.MeetingDao;
import dk.digitalidentity.sbf.dao.PostJobDao;
import dk.digitalidentity.sbf.dao.model.*;
import dk.digitalidentity.sbf.dao.model.enums.AgendaType;
import dk.digitalidentity.sbf.dao.model.enums.JobStatus;
import dk.digitalidentity.sbf.security.SecurityUtil;
import dk.digitalidentity.sbf.service.dto.AddressDto;
import dk.digitalidentity.sbf.service.post.IPostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Service
@Slf4j
@EnableAsync
public class MeetingService {

    @Autowired
    private MeetingDao meetingDao;

    @Autowired
    TemplateEngine templateEngine;

    @Autowired
    private JournalizeJobDao journalizeJobDao;

    @Autowired
    private PostJobDao postJobDao;

    @Autowired
    private IPostService eboksService;

    @Autowired
    private ESDHService esdhService;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public Meeting getById(long id) {
        return meetingDao.findById(id);
    }

    public Meeting save(Meeting meeting) {
        return meetingDao.save(meeting);
    }

    public Meeting copyOldMeetingToNewMeeting(Meeting oldMeeting) {
        Meeting newMeeting = new Meeting();

        newMeeting.setAddress(oldMeeting.getAddress());
        newMeeting.setAgenda(oldMeeting.getAgenda());
        newMeeting.setCourse(oldMeeting.getCourse());
        newMeeting.setInvitation(oldMeeting.getInvitation());
        newMeeting.setLetter(oldMeeting.getLetter());

        newMeeting.setMeetingParties(new ArrayList<>());
        for (MeetingParty meetingParty : oldMeeting.getMeetingParties().stream().filter(p -> p.getParty().isActive()).collect(Collectors.toList())) {
            MeetingParty newMeetingParty = new MeetingParty();
            newMeetingParty.setDream(meetingParty.getDream());
            newMeetingParty.setInvited(meetingParty.isInvited());
            newMeetingParty.setReceiveMeetingRecord(meetingParty.isReceiveMeetingRecord());
            newMeetingParty.setParty(meetingParty.getParty());
            newMeetingParty.setMeeting(newMeeting);
            newMeeting.getMeetingParties().add(newMeetingParty);

            newMeetingParty.setGoals(new ArrayList<>());
            for (Goal goal : meetingParty.getGoals()) {
                if (goal.isActive()) {
                    Goal newGoal = new Goal();
                    newGoal.setActive(true);
                    newGoal.setObjective(goal.getObjective());
                    newGoal.setTitle(goal.getTitle());
                    newGoal.setMeetingParty(newMeetingParty);
                    newMeetingParty.getGoals().add(newGoal);

                    newGoal.setSubGoals(new ArrayList<>());
                    for (SubGoal subGoal : goal.getSubGoals()) {
                        if (!subGoal.isClosed()) {
                            SubGoal newSubGoal = new SubGoal();
                            newSubGoal.setAction(subGoal.getAction());

                            newSubGoal.setResponsibleParties(new ArrayList<>());
                            for(var party : subGoal.getResponsibleParties())
                            {
                                newSubGoal.getResponsibleParties().add(party);
                            }

                            newSubGoal.setResponsibleInternalContacts(new ArrayList<>());
                            for(var internalContact : subGoal.getResponsibleInternalContacts())
                            {
                                newSubGoal.getResponsibleInternalContacts().add(internalContact);
                            }

                            newSubGoal.setResponsibleExternalContacts(new ArrayList<>());
                            for(var externalContact : subGoal.getResponsibleExternalContacts())
                            {
                                newSubGoal.getResponsibleExternalContacts().add(externalContact);
                            }
                            
                            newSubGoal.setResponsiblePartyRepresentatives(new ArrayList<>());
                            for(var partyRepresentative : subGoal.getResponsiblePartyRepresentatives())
                            {
                                newSubGoal.getResponsiblePartyRepresentatives().add(partyRepresentative);
                            }

                            newSubGoal.setStartDate(subGoal.getStartDate());
                            newSubGoal.setTitle(subGoal.getTitle());
                            newSubGoal.setTrialActionApproved(subGoal.isTrialActionApproved());
                            newSubGoal.setTrialActionInitiated(subGoal.isTrialActionInitiated());
                            newSubGoal.setGoal(newGoal);
                            newGoal.getSubGoals().add(newSubGoal);
                        }
                    }
                }
            }

        }

        newMeeting.setMeetingPartyRepresentatives(new ArrayList<>());
        for (MeetingPartyRepresentative meetingPartyRepresentative : oldMeeting.getMeetingPartyRepresentatives().stream().filter(p -> p.getPartyRepresentative().isActive()).collect(Collectors.toList())) {
            MeetingPartyRepresentative newMeetingPartyRepresentative = new MeetingPartyRepresentative();
            newMeetingPartyRepresentative.setInvited(meetingPartyRepresentative.isInvited());
            newMeetingPartyRepresentative.setReceiveMeetingRecord(meetingPartyRepresentative.isReceiveMeetingRecord());
            newMeetingPartyRepresentative.setPartyRepresentative(meetingPartyRepresentative.getPartyRepresentative());
            newMeetingPartyRepresentative.setMeeting(newMeeting);
            newMeeting.getMeetingPartyRepresentatives().add(newMeetingPartyRepresentative);
        }

        newMeeting.setMeetingInternalContacts(new ArrayList<>());
        for (MeetingInternalContact meetingInternalContact : oldMeeting.getMeetingInternalContacts().stream().filter(p -> p.getInternalContact().isActive()).collect(Collectors.toList())) {
            MeetingInternalContact newMeetingInternalContact = new MeetingInternalContact();
            newMeetingInternalContact.setInvited(meetingInternalContact.isInvited());
            newMeetingInternalContact.setInternalContact(meetingInternalContact.getInternalContact());
            newMeetingInternalContact.setMeeting(newMeeting);
            newMeeting.getMeetingInternalContacts().add(newMeetingInternalContact);
        }

        newMeeting.setMeetingExternalContacts(new ArrayList<>());
        for (MeetingExternalContact meetingExternalContact : oldMeeting.getMeetingExternalContacts().stream().filter(p -> p.getExternalContact().isActive()).collect(Collectors.toList())) {
            MeetingExternalContact newMeetingExternalContact = new MeetingExternalContact();
            newMeetingExternalContact.setInvited(meetingExternalContact.isInvited());
            newMeetingExternalContact.setExternalContact(meetingExternalContact.getExternalContact());
            newMeetingExternalContact.setMeeting(newMeeting);
            newMeeting.getMeetingExternalContacts().add(newMeetingExternalContact);
        }
        return newMeeting;
    }

    public byte[] getInvitePdf(Meeting meeting, AddressDto addressDto) throws IOException, DocumentException {
        var html = getInviteHtml(meeting, addressDto);
        var binary = convertHtmlToPdf(html);
        return binary;
    }

    public byte[] getRecordPdf(Meeting meeting, AddressDto addressDto) throws IOException, DocumentException {
        var html = getRecordHtml(meeting, addressDto);
        var binary = convertHtmlToPdf(html);
        return binary;
    }

    private String getInviteHtml(Meeting meeting, AddressDto addressDto) {
        String typeHeader = "Enkeltpartsforløb deltager: ";
        if (meeting.getCourse().getParties().stream().filter(p -> p.isActive()).count() > 1) {
            typeHeader = "Flerpartsforløb deltagere: ";
        }
        var context = new Context();
        context.setVariable("meeting", meeting);
        context.setVariable("addressDto", addressDto);
        context.setVariable("primary", meeting.getCourse().getPrimaryInternalContact().getEmployee());
        context.setVariable("typeHeader", typeHeader);
        context.setVariable("parties", meeting.getMeetingParties().stream().filter(p -> p.isInvited() && p.getParty().isActive()).collect(Collectors.toList()));
        context.setVariable("partyRepresentatives", meeting.getMeetingPartyRepresentatives().stream().filter(p -> p.isInvited() && p.getPartyRepresentative().isActive()).collect(Collectors.toList()));
        context.setVariable("internals", meeting.getMeetingInternalContacts().stream().filter(i -> i.isInvited() && i.getInternalContact().isActive()).collect(Collectors.toList()));
        context.setVariable("externals", meeting.getMeetingExternalContacts().stream().filter(e -> e.isInvited() && e.getExternalContact().isActive()).collect(Collectors.toList()));
        var html = templateEngine.process("course/meeting_invite_pdf", context);
        return html;
    }

    private String getRecordHtml(Meeting meeting, AddressDto addressDto) {
        String typeHeader = "Enkeltpartsforløb deltager: ";
        if (meeting.getCourse().getParties().stream().filter(p -> p.isActive()).count() > 1) {
            typeHeader = "Flerpartsforløb deltagere: ";
        }
        var context = new Context();
        context.setVariable("meeting", meeting);
        context.setVariable("addressDto", addressDto);
        context.setVariable("primary", meeting.getCourse().getPrimaryInternalContact().getEmployee());
        context.setVariable("typeHeader", typeHeader);
        context.setVariable("parties", meeting.getMeetingParties().stream().filter(p -> p.isAttended() && p.getParty().isActive()).collect(Collectors.toList()));
        context.setVariable("partyRepresentatives", meeting.getMeetingPartyRepresentatives().stream().filter(p -> p.isAttended() && p.getPartyRepresentative().isActive()).collect(Collectors.toList()));
        context.setVariable("internals", meeting.getMeetingInternalContacts().stream().filter(i -> i.isAttended() && i.getInternalContact().isActive()).collect(Collectors.toList()));
        context.setVariable("externals", meeting.getMeetingExternalContacts().stream().filter(e -> e.isAttended() && e.getExternalContact().isActive()).collect(Collectors.toList()));
        context.setVariable("allParties", meeting.getMeetingParties().stream().filter(p -> p.getParty().isActive()).collect(Collectors.toList()));
        var html = templateEngine.process("course/meeting_journalize_pdf", context);
        return html;
    }

    private byte[] convertHtmlToPdf(String html) throws DocumentException, IOException {
        var outputStream = new ByteArrayOutputStream();
        var renderer = new ITextRenderer();
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(outputStream);
        var result = outputStream.toByteArray();
        outputStream.close();
        return result;
    }

    @Transactional
    public void postAndJournalizeAgenda(Meeting meeting) throws Exception {
        for (MeetingParty meetingParty : meeting.getMeetingParties().stream().filter(p -> p.getParty().isActive()).collect(Collectors.toList())) {
            // only send to parties if invited
            if (meetingParty.isInvited()) {
                createPostJob(AgendaType.AGENDA, meetingParty, null);
            }
            // always journalize on all parties
            createJournalizeJob(AgendaType.AGENDA, meetingParty);
        }

        // also send to party representatives that are invited
        for (MeetingPartyRepresentative meetingPartyRepresentative : meeting.getMeetingPartyRepresentatives().stream().filter(p -> p.isInvited() && p.getPartyRepresentative().isActive()).collect(Collectors.toList())) {
            createPostJob(AgendaType.AGENDA, null, meetingPartyRepresentative);
        }

        meeting.setInviteSent(true);
        meeting.setInviteTime(LocalDateTime.now());
        save(meeting);
    }

    @Transactional
    public void postAndJournalizeRecord(Meeting meeting) throws Exception {
        if (meeting.isRecordSent()) {
            throw new Exception("Cannot post and journalize record, since it is already sent");
        }

        for (MeetingParty meetingParty : meeting.getMeetingParties().stream().filter(p -> p.getParty().isActive()).collect(Collectors.toList())) {
            // only send to parties if they should receive record
            if (meetingParty.isReceiveMeetingRecord()) {
                createPostJob(AgendaType.RECORD, meetingParty, null);
            }
            // always journalize on all parties
            createJournalizeJob(AgendaType.RECORD, meetingParty);
        }

        // also send to party representatives that should receive record
        for (MeetingPartyRepresentative meetingPartyRepresentative : meeting.getMeetingPartyRepresentatives().stream().filter(p -> p.isReceiveMeetingRecord() && p.getPartyRepresentative().isActive()).collect(Collectors.toList())) {
            createPostJob(AgendaType.RECORD, null, meetingPartyRepresentative);
        }

        // copy the meeting to a new meeting and set the new meeting as active
        var newMeeting = copyOldMeetingToNewMeeting(meeting);
        save(newMeeting);
        meeting.getCourse().setActiveMeeting(newMeeting);
        meeting.setRecordSent(true);

        save(meeting);
    }


    private void createJournalizeJob(AgendaType agendaType, MeetingParty meetingParty) {
        var journalizeJob = new JournalizeJob();
        journalizeJob.setStatus(JobStatus.PENDING);
        journalizeJob.setCreated(LocalDateTime.now());
        journalizeJob.setAgendaType(agendaType);
        journalizeJob.setMeetingParty(meetingParty);
        journalizeJob.setCreatedBy(SecurityUtil.getUser().getUsername());
        journalizeJob.setMeeting(meetingParty.getMeeting());
        journalizeJobDao.save(journalizeJob);
    }

    private void createPostJob(AgendaType agendaType, MeetingParty meetingParty, MeetingPartyRepresentative meetingPartyRepresentative) throws Exception {
        if (meetingParty == null && meetingPartyRepresentative == null) {
            throw new Exception("meetingParty and meetingPartyRepresentative cannot both be null");
        }
        if (meetingParty != null && meetingPartyRepresentative != null) {
            throw new Exception("either meetingParty or meetingPartyRepresentative must be supplied");
        }
        var postJob = new PostJob();
        postJob.setStatus(JobStatus.PENDING);
        postJob.setCreated(LocalDateTime.now());
        postJob.setAgendaType(agendaType);
        postJob.setMeetingParty(meetingParty);
        postJob.setMeetingPartyRepresentative(meetingPartyRepresentative);
        postJob.setCreatedBy(SecurityUtil.getUser().getUsername());
        Meeting meeting;
        if (meetingParty != null) {
            meeting = meetingParty.getMeeting();
        } else {
            meeting = meetingPartyRepresentative.getMeeting();
        }
        postJob.setMeeting(meeting);
        postJobDao.save(postJob);
    }

    // Async ensures that frontend can invoke the method, but avoid waiting for it to complete (ie. after triggering the jobs)
    // synchronized ensures that threads are not executing jobs concurrently (ie. from frontend and from scheduled task at the same time)
    @Async
    @Transactional
    public synchronized void executePendingJobs() {
        try {
            executePendingPostJobs();
            executePendingJournalizeJobs();
        } catch (Exception e) {
            log.error("Failed to execute pending jobs", e);
        }
    }

    private void executePendingJournalizeJobs() {
        var pendingJournalizeJobs = journalizeJobDao.findByStatus(JobStatus.PENDING);
        for (var journalizeJob : pendingJournalizeJobs) {
            try {
                // clear previous error, if any
                journalizeJob.setError("");

                var addressDto = new AddressDto();
                var party = journalizeJob.getMeetingParty().getParty();
                addressDto.setName(party.getFirstName() + " " + journalizeJob.getMeetingParty().getParty().getSurname());
                addressDto.setCity(party.getCity());
                addressDto.setPostCode(party.getPostCode());
                addressDto.setAddress(party.getAddress());

                byte[] bytes;
                String title;
                String description;
                String meetingTime = "";
                if( journalizeJob.getMeeting().getMeetingTime() != null )
                {
                    meetingTime = journalizeJob.getMeeting().getMeetingTime().format(formatter);
                }
                if (journalizeJob.getAgendaType() == AgendaType.AGENDA) {
                    bytes = getInvitePdf(journalizeJob.getMeeting(), addressDto);
                    title = "Invitation til møde i tværfagligt team d. " + meetingTime;
                    description = journalizeJob.getMeetingParty().isInvited() ? "Digital post sendt via SBF" : "";
                } else if (journalizeJob.getAgendaType() == AgendaType.RECORD) {
                    bytes = getRecordPdf(journalizeJob.getMeeting(), addressDto);
                    title = "Referat fra møde i tværfagligt team d. " + meetingTime;
                    description = journalizeJob.getMeetingParty().isReceiveMeetingRecord() ? "Digital post sendt via SBF" : "";
                } else {
                    throw new Exception("Unexpected type (" + journalizeJob.getAgendaType() + ") for JournalizeJob with id: " + journalizeJob.getId());
                }

                var esdhDocument = esdhService.createDocument(title, description, party, bytes);
                journalizeJob.setEsdhReference(esdhDocument.getDocumentReference());
                journalizeJob.setStatus(JobStatus.COMPLETED);
            } catch (Exception e) {
                journalizeJob.setStatus(JobStatus.FAILED);
                journalizeJob.setError(e.getMessage());
                log.error("Failed to jounalize document " ,e);
            } finally {
                journalizeJob.setChanged(LocalDateTime.now());
                journalizeJobDao.save(journalizeJob);
            }
        }
    }

    private void executePendingPostJobs() {
        var pendingPostJobs = postJobDao.findByStatus(JobStatus.PENDING);
        for (var postJob : pendingPostJobs) {
            try {
                // clear previous error, if any
                postJob.setError("");
                var addressDto = new AddressDto();
                String cpr;
                Meeting meeting;
                // either meetingParty og meetingPartyRepresentative is set
                if (postJob.getMeetingParty() != null) {
                    addressDto.setName(postJob.getMeetingParty().getParty().getFirstName() + " " + postJob.getMeetingParty().getParty().getSurname());
                    addressDto.setCity(postJob.getMeetingParty().getParty().getCity());
                    addressDto.setPostCode(postJob.getMeetingParty().getParty().getPostCode());
                    addressDto.setAddress(postJob.getMeetingParty().getParty().getAddress());
                    cpr = postJob.getMeetingParty().getParty().getCpr();
                    meeting = postJob.getMeetingParty().getMeeting();
                } else if (postJob.getMeetingPartyRepresentative() != null) {
                    addressDto.setName(postJob.getMeetingPartyRepresentative().getPartyRepresentative().getFirstName() + " " + postJob.getMeetingPartyRepresentative().getPartyRepresentative().getSurname());
                    addressDto.setCity(postJob.getMeetingPartyRepresentative().getPartyRepresentative().getCity());
                    addressDto.setPostCode(postJob.getMeetingPartyRepresentative().getPartyRepresentative().getPostCode());
                    addressDto.setAddress(postJob.getMeetingPartyRepresentative().getPartyRepresentative().getAddress());
                    cpr = postJob.getMeetingPartyRepresentative().getPartyRepresentative().getCpr();
                    meeting = postJob.getMeetingPartyRepresentative().getMeeting();
                } else {
                    throw new Exception("No meetingParty or meetingPartyRepresentative was found for PostJob with id: " + postJob.getId());
                }
                byte[] bytes;
                String subject;
                String meetingTime = "";
                if( meeting.getMeetingTime() != null )
                {
                    meetingTime = meeting.getMeetingTime().format(formatter);
                }
                if (postJob.getAgendaType() == AgendaType.AGENDA) {
                    bytes = getInvitePdf(meeting, addressDto);
                    subject = "Invitation til møde i tværfagligt team " + meetingTime;
                } else if (postJob.getAgendaType() == AgendaType.RECORD) {
                    bytes = getRecordPdf(meeting, addressDto);
                    subject = "Referat fra møde i tværfagligt team " + meetingTime;
                } else {
                    throw new Exception("Unexpected type (" + postJob.getAgendaType() + ") for PostJob with id: " + postJob.getId());
                }
                var reference = eboksService.sendPost(cpr, subject, bytes);
                postJob.setPostReference(reference);
                postJob.setStatus(JobStatus.COMPLETED);

            } catch (Exception e) {
                log.error("Post job failed",e);
                postJob.setStatus(JobStatus.FAILED);
                postJob.setError(e.getMessage());
            } finally {
                postJob.setChanged(LocalDateTime.now());
                postJobDao.save(postJob);
            }
        }
    }
}
