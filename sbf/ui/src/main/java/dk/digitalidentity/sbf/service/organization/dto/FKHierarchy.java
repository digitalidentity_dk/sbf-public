package dk.digitalidentity.sbf.service.organization.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FKHierarchy {
	
	private List<FKUser> users;
	
	@JsonProperty(value = "oUs")
	private List<FKOU> ous;
}
