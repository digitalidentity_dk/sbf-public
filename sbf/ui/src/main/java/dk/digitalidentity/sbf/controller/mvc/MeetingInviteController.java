package dk.digitalidentity.sbf.controller.mvc;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.context.Context;

import com.lowagie.text.DocumentException;

import dk.digitalidentity.sbf.config.SBFConfiguration;
import dk.digitalidentity.sbf.dao.model.Course;
import dk.digitalidentity.sbf.dao.model.Meeting;
import dk.digitalidentity.sbf.interceptors.SecurityLogIntercepted;
import dk.digitalidentity.sbf.service.CourseService;
import dk.digitalidentity.sbf.service.MeetingService;
import dk.digitalidentity.sbf.service.dto.AddressDto;

@Controller
public class MeetingInviteController {
	
	@Autowired
	private CourseService courseService;
	
	@Autowired
	private MeetingService meetingService;
	
	@Autowired
	private SBFConfiguration sbfConfiguration;
	
	@SecurityLogIntercepted(operation = "Tilgår invitationssiden", args = {"courseId"})
	@GetMapping("/invite")
	public String coursePage(Model model, @RequestParam Long courseId) {
		Course course = courseService.getById(courseId);
		
		if (course == null) {
			return "redirect:/error";
		}
		Meeting meeting = course.getActiveMeeting();
		
		if (StringUtils.isEmpty(sbfConfiguration.getLinks().getAgendaLink())) {
			model.addAttribute("agendaLink", "");
		} else {
			model.addAttribute("agendaLink", sbfConfiguration.getLinks().getAgendaLink());
		}
		
		model.addAttribute("meeting", meeting);
		model.addAttribute("course", course);
		model.addAttribute("parties", meeting.getMeetingParties().stream().filter(p -> p.getParty().isActive()).collect(Collectors.toList()));
		model.addAttribute("partyRepresentatives", meeting.getMeetingPartyRepresentatives().stream().filter(p -> p.getPartyRepresentative().isActive()).collect(Collectors.toList()));
		model.addAttribute("internals", meeting.getMeetingInternalContacts().stream().filter(i -> i.getInternalContact().isActive()).collect(Collectors.toList()));
		model.addAttribute("externals", meeting.getMeetingExternalContacts().stream().filter(e -> e.getExternalContact().isActive()).collect(Collectors.toList()));
		
		return "course/meeting_invite";
	}
	
	@GetMapping("/invite/pdf")
    public ResponseEntity<ByteArrayResource> getMeetingInvitePdf(@RequestParam Long id, HttpServletResponse response) throws IOException, DocumentException {
        Meeting meeting = meetingService.getById(id);
        if (meeting == null) {
        	return ResponseEntity.badRequest().build();
        }
        AddressDto addressDto = new AddressDto();
		addressDto.setName("<<Navn>>");
		addressDto.setAddress("<<Gade>>");
		addressDto.setPostCode("<<PostNr>>");
		addressDto.setCity("<<By>>");

		byte[] byteData = meetingService.getInvitePdf(meeting, addressDto);
        ByteArrayResource resource = new ByteArrayResource(byteData);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=mødeinvitation_" + id + ".pdf");
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(byteData.length)
                .contentType(MediaType.parseMediaType("application/pdf"))
                .body(resource);
    }
}
