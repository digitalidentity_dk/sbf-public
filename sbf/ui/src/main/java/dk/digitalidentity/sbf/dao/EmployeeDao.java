package dk.digitalidentity.sbf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.Employee;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeDao extends JpaRepository<Employee, Long>{
	Employee findByUuid(String uuid);

	@Query(nativeQuery = true, value = "select * from employees where active=1 and (first_name like ?1% or surname like ?1% or username like ?1%) limit 10")
	List<Employee> search(String query);
}
