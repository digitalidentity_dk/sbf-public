package dk.digitalidentity.sbf.controller.rest;

import dk.digitalidentity.sbf.dao.model.Meeting;
import dk.digitalidentity.sbf.dao.model.MeetingParty;
import dk.digitalidentity.sbf.dao.model.MeetingPartyRepresentative;
import dk.digitalidentity.sbf.interceptors.SecurityLogIntercepted;
import dk.digitalidentity.sbf.service.MeetingPartyRepresentativeService;
import dk.digitalidentity.sbf.service.MeetingPartyService;
import dk.digitalidentity.sbf.service.MeetingService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
public class MeetingJournalizeRestController {

    @Autowired
    private MeetingPartyService meetingPartyService;

    @Autowired
    private MeetingPartyRepresentativeService meetingPartyRepresentativeService;

    @Autowired
    private MeetingService meetingService;

    @SecurityLogIntercepted(operation = "Send/send ikke til (møde)part", args = {"id", "send"})
    @PostMapping("/rest/journalize/meetingparty/{id}")
    public ResponseEntity<?> meetingPartySend(Model model, @PathVariable long id, @RequestParam boolean send) {
        MeetingParty meetingParty = meetingPartyService.getById(id);
        if (meetingParty == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        meetingParty.setReceiveMeetingRecord(send);
        meetingPartyService.save(meetingParty);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Send/send ikke til (møde)partsrepræsentant", args = {"id", "send"})
    @PostMapping("/rest/journalize/meetingpartyrepresentative/{id}")
    public ResponseEntity<?> meetingPartyRepresentativeSend(Model model, @PathVariable long id, @RequestParam boolean send) {
        MeetingPartyRepresentative meetingPartyRepresentative = meetingPartyRepresentativeService.getById(id);
        if (meetingPartyRepresentative == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        meetingPartyRepresentative.setReceiveMeetingRecord(send);
        meetingPartyRepresentativeService.save(meetingPartyRepresentative);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ResponseBody
    @PostMapping("/rest/journalize/saveletter/{id}")
    public ResponseEntity<?> saveLetter(Model model, @PathVariable long id, @RequestBody(required = false) String letter) {
        Meeting meeting = meetingService.getById(id);
        if (meeting == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        meeting.setLetter(letter);
        meetingService.save(meeting);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Send følgebrev", args = {"id"})
    @PostMapping("/rest/journalize/send/{id}")
    public ResponseEntity<?> sendLetter(Model model, @PathVariable long id) throws Exception {
        Meeting meeting = meetingService.getById(id);
        if (meeting == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        
        long partiesWithoutCase = meeting.getMeetingParties().stream().map(m -> m.getParty()).filter(p -> p.isActive() && StringUtils.isEmpty(p.getEsdhCaseReference())).count();
        if (partiesWithoutCase > 0) {
        	return new ResponseEntity<>("Der er en eller flere parter, der ikke har en sag tilknyttet, og følgebrevet kan derfor ikke sendes", HttpStatus.BAD_REQUEST);
        }
        
        meetingService.postAndJournalizeRecord(meeting);
        meetingService.executePendingJobs();

        return new ResponseEntity<>(HttpStatus.OK);
    }
}

