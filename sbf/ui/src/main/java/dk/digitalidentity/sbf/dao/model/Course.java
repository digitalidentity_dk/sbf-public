package dk.digitalidentity.sbf.dao.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Entity(name="courses")
@Getter
@Setter
public class Course {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column
	private LocalDateTime startDate;
	
	@Column
	@NotNull
	private boolean active;
	
	@Column
	private String inactiveReason;
	
	@Column
	private LocalDateTime created;
	
	@Column
	@NotNull
	private boolean draft;
	
	@Column
	@Size(max = 255)
	private String notes;
	
	@OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<Meeting> meetings;
	
	@OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<Party> parties;
	
	@OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<PartyRepresentative> partyRepresentatives;
	
	@OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<InternalContact> internalContacts;
	
	@OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<ExternalContact> externalContacts;
	
	@OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<Progress> progresses;
	
	@OneToOne
	@JoinColumn(name = "primary_internal_contact_id", referencedColumnName = "id")
	private InternalContact primaryInternalContact;
	
	@OneToOne
	@JoinColumn(name = "active_meeting_id", referencedColumnName = "id")
	private Meeting activeMeeting;
}
