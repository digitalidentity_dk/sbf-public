package dk.digitalidentity.sbf.dao.model.enums;

public enum JobStatus {
    PENDING,
    COMPLETED,
    FAILED
}
