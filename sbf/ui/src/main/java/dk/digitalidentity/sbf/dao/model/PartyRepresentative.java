package dk.digitalidentity.sbf.dao.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;

@Entity(name="party_representatives")
@Getter
@Setter
public class PartyRepresentative {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column
	@NotNull
	private boolean active;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String cpr;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String firstName;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String surname;
	
	@Column
	@Size(max = 255)
	private String phone;
	
	@Column
	@Size(max = 255)
	private String email;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String address;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String postCode;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String city;
	
	@Column
	@NotNull
	private LocalDateTime createdDate;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String relationship;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "course_id")
	private Course course;
}
