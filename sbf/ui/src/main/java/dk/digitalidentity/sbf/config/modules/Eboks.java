package dk.digitalidentity.sbf.config.modules;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Eboks {
	private String url;
	private String materialeId;
	private String cvr;
	private String senderId;
}
