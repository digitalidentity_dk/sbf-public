package dk.digitalidentity.sbf.service.dto;

import dk.digitalidentity.sbf.dao.model.Progress;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProgressDto {
	private String dateString;
	private int score;
	private String comment;
	private boolean scoredByProfessional;

	public ProgressDto(Progress progress) {
		this.score = progress.getScore();
		this.comment = progress.getProgressComment();
		this.scoredByProfessional = progress.isScoredByProfessional();
		this.dateString = progress.getProgressDate().toString().split("T")[0];
	}
}
