package dk.digitalidentity.sbf.config.modules;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SBSYS {
    private String clientId;
    private String clientSecret;
    private String tokenUri;
    private String baseUrl;
    private int caseTemplateId;
    private int caseClosedStatusId;
    private int caseActiveStatusId;
    private int documentArtId;
}