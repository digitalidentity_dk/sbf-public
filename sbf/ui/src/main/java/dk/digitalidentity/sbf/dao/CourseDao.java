package dk.digitalidentity.sbf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.digitalidentity.sbf.dao.model.Course;


public interface CourseDao extends JpaRepository<Course, Long>{
	Course findById(long id);
}
