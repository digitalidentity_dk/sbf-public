package dk.digitalidentity.sbf.dao.model.enums;

public enum ClosedReason {
	ACCOMPLISHED,
	NOT_ACCOMPLISHED
}
