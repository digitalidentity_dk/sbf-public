package dk.digitalidentity.sbf.dao.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import dk.digitalidentity.sbf.dao.model.enums.ClosedReason;
import lombok.Getter;
import lombok.Setter;

@Entity(name="goals")
@Getter
@Setter
public class Goal {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String title;
	
	@Column
	@NotNull
	@Size(max = 255)
	private String objective;
	
	@Column
	@NotNull
	private boolean active;
	
	@Column
	@Enumerated(EnumType.STRING)
	private ClosedReason closedReason;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "meeting_party_id")
	private MeetingParty meetingParty;
	
	@OneToMany(mappedBy = "goal", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<SubGoal> subGoals;
}
