package dk.digitalidentity.sbf.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "email")
public class EmailConfiguration {
	private boolean enabled = false;
	private String username;
	private String password;
	private String host;
	private String from = "no-reply@aabendagsorden.dk";
	private String fromName = "sbf";
	private long subscriptionDateLimiterDays = 14;
}
