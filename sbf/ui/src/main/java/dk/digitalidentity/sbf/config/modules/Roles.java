package dk.digitalidentity.sbf.config.modules;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Roles {
    private String coordinator = "ROLE_http://sbf.digital-identity.dk/roles/usersystemrole/coordinator/1";
}