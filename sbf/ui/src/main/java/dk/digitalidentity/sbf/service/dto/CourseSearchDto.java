package dk.digitalidentity.sbf.service.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import dk.digitalidentity.sbf.config.modules.MandatoryMeetings;
import dk.digitalidentity.sbf.controller.mvc.dto.PartyListDto;
import dk.digitalidentity.sbf.dao.model.Course;
import dk.digitalidentity.sbf.dao.model.Meeting;
import dk.digitalidentity.sbf.dao.model.Party;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourseSearchDto {
	private long id;
	private List<PartyListDto> parties;
	private LocalDateTime nextMeeting;
	private LocalDateTime startDate;
	private boolean active;
	private String type;
	private String partyHeader;
	
	public CourseSearchDto(MandatoryMeetings mandatoryMeetings, Course course) {
		this.id = course.getId();
		this.active = course.isActive();
		
		List<Meeting> meetings = course.getMeetings();
		if (meetings != null && !meetings.isEmpty()) {
			List<Meeting> notNullMeetings = meetings.stream().filter(m -> m.getMeetingTime() != null).collect(Collectors.toList());
        	notNullMeetings.sort((Meeting m1, Meeting m2) -> m1.getMeetingTime().compareTo(m2.getMeetingTime()));
        	if (!notNullMeetings.isEmpty()) {
        		this.startDate = notNullMeetings.get(0).getMeetingTime();
        	} else {
        		this.startDate = null;
        	}
		} else {
			this.startDate = null;
		}
		
		this.parties = course.getParties().stream().filter(p -> p.isActive()).map(p -> new PartyListDto(p, mandatoryMeetings)).collect(Collectors.toList());
		
		if (course.getParties().stream().filter(p -> p.isActive()).count() > 1) {
			this.type = "Flerpartsforløb";
		} else {
			this.type = "Enkeltpartsforløb";
		}
		
		List<Party> activeParties = course.getParties().stream().filter(p -> p.isActive()).collect(Collectors.toList());
		String header = "<span style='padding-top: 3px;padding-bottom: 3px;'>" + activeParties.get(0).getFirstName() + " " + activeParties.get(0).getSurname() + "</span>";
		for (int i = 1; i < activeParties.size(); i++) {
			header += "<hr style='border-top: 0.01 em; margin: 2px;'/><span style='padding-top: 3px;padding-bottom: 3px;'>" + activeParties.get(i).getFirstName() + " " + activeParties.get(i).getSurname() + "</span>";
		}
		this.partyHeader = header;
		
		//LocalDateTime now = LocalDateTime.now();
		//List<LocalDateTime> afterToday = course.getMeetings().stream().filter(m -> m.getMeetingTime() != null).filter(m -> m.getMeetingTime().isAfter(now)).map(m -> m.getMeetingTime()).sorted().collect(Collectors.toList());
		//this.nextMeeting = afterToday.isEmpty() ? null : afterToday.get(0);
		this.nextMeeting = course.getActiveMeeting() != null ? course.getActiveMeeting().getMeetingTime() : null;
	}
}
