package dk.digitalidentity.sbf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sbf.dao.PartyDao;
import dk.digitalidentity.sbf.dao.model.Course;
import dk.digitalidentity.sbf.dao.model.Party;

@Service
public class PartyService {
	
	@Autowired
	private PartyDao partyDao;
	
	@Autowired
	private CourseService courseService;
	
	public Party getById(long id) {
		return partyDao.findById(id);
	}
	
	public Party save(Party party) {
		return partyDao.save(party);
	}

    public boolean belongsToActiveCourse(String cpr) {
		return partyDao.existsByCprAndActiveTrueAndCourse_ActiveTrueAndCourse_DraftFalse(cpr);
    }
    
    public Party getPreviouslyAssociated(String cpr, Long courseId) {
        if (courseId != null) {
        	Course course = courseService.getById(courseId);
        	if (course != null) {
        		Party party = course.getParties().stream().filter(p -> !p.isActive() && p.getCpr().equals(cpr)).findAny().orElse(null);
        		return party;
        	}
        }
		return null;
    }
}
