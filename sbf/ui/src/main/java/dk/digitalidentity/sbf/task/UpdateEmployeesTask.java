package dk.digitalidentity.sbf.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import dk.digitalidentity.sbf.config.SBFConfiguration;
import dk.digitalidentity.sbf.dao.EmployeeDao;
import dk.digitalidentity.sbf.dao.model.Employee;
import dk.digitalidentity.sbf.service.organization.IOrganizationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

@Component
@EnableScheduling
@Slf4j
public class UpdateEmployeesTask {

    @Autowired
    private UpdateEmployeesTask self;

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private IOrganizationService organizationService;

    @Autowired
    SBFConfiguration sbfConfiguration;

    private final ObjectMapper mapper = new ObjectMapper();

    @Scheduled(cron = "${cron.UpdateEmployeesTask:0 10 2 * * ?}")
    public void execute() throws Exception {
        if( sbfConfiguration.isScheduledJobsEnabled() ) {
            self.updateEmloyees();
        }
    }

    @Transactional
    public void updateEmloyees() throws Exception {
        log.info("Updating employees");
        var sourceEmployees = organizationService.getEmployees();
        var dbEmployees = employeeDao.findAll();

        // handle inserts and updates
        for (var sourceEmployee : sourceEmployees) {
            var existingEmployee = dbEmployees.stream().filter(dbe -> dbe.getUuid().equalsIgnoreCase(sourceEmployee.getUuid())).findFirst();
            var dbEmployee = existingEmployee.isPresent() ? existingEmployee.get() : new Employee();
            var originalState = mapper.writeValueAsString(dbEmployee);

            dbEmployee.setActive(true);
            dbEmployee.setUuid(sourceEmployee.getUuid());
            dbEmployee.setUsername(sourceEmployee.getUsername());
            dbEmployee.setFirstName(sourceEmployee.getFirstName());
            dbEmployee.setSurname(sourceEmployee.getSurname());
            dbEmployee.setEmail(sourceEmployee.getEmail());
            dbEmployee.setPhone(sourceEmployee.getPhone());
            dbEmployee.setPosition(sourceEmployee.getPosition());
            dbEmployee.setDepartment(sourceEmployee.getDepartment());
            dbEmployee.setOrgunitUuid(sourceEmployee.getOrgunitUuid());
            dbEmployee.setOrgunitPath(sourceEmployee.getOrgunitPath());

            var updatedState = mapper.writeValueAsString(dbEmployee);
            if (!originalState.equalsIgnoreCase(updatedState)) {
                employeeDao.save(dbEmployee);
            }
        }

        // handle inactivations (employee no longer present in source)
        for (var toBeInactivated : dbEmployees.stream().filter(dbe -> dbe.isActive() == true && sourceEmployees.stream().noneMatch(se -> se.getUuid().equalsIgnoreCase(dbe.getUuid()))).collect(Collectors.toList())) {
            toBeInactivated.setActive(false);
            employeeDao.save(toBeInactivated);
        }

        log.info("Finished updating employees");
    }

}
