package dk.digitalidentity.sbf.controller.rest;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dk.digitalidentity.sbf.controller.rest.dto.PartyProgressDto;
import dk.digitalidentity.sbf.dao.model.Course;
import dk.digitalidentity.sbf.dao.model.Party;
import dk.digitalidentity.sbf.dao.model.Progress;
import dk.digitalidentity.sbf.interceptors.SecurityLogIntercepted;
import dk.digitalidentity.sbf.service.CourseService;
import dk.digitalidentity.sbf.service.PartyService;
import dk.digitalidentity.sbf.service.ProgressService;

@RestController
public class ProgressRestController {
	@Autowired
	private CourseService courseService;
	
	@Autowired
	private PartyService partyService;
	
	@Autowired
	private ProgressService progressService;
	
	@SecurityLogIntercepted(operation = "Gem nyt fremskridt", args = {"partyProgressDto"})
	@ResponseBody
	@PostMapping("/rest/progress/save")
	public ResponseEntity<?> addProgress(Model model, @RequestBody PartyProgressDto partyProgressDto) {
		Course course  = courseService.getById(partyProgressDto.getCourseId());
		if (course == null) {
			return new ResponseEntity<>("Forløbet findes ikke", HttpStatus.BAD_REQUEST);
		}
		
		Party party = partyService.getById(partyProgressDto.getPartyId());
		if (party == null) {
			return new ResponseEntity<>("Parten findes ikke", HttpStatus.BAD_REQUEST);
		}
		
		Progress progress = new Progress();
		progress.setCourse(course);
		progress.setProgressDate(LocalDate.now());
		progress.setScore(partyProgressDto.getSelectedRating());
		progress.setScoredByProfessional(partyProgressDto.isProfessionalOpinion());
		progress.setParty(party);
		progress.setProgressComment(partyProgressDto.getComment());
		
		progressService.save(progress);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@SecurityLogIntercepted(operation = "Rediger fremskridt", args = {"partyProgressDto"})
	@ResponseBody
	@PostMapping("/rest/progress/edit")
	public ResponseEntity<?> editProgress(Model model, @RequestBody PartyProgressDto partyProgressDto) {
		Course course  = courseService.getById(partyProgressDto.getCourseId());
		if (course == null) {
			return new ResponseEntity<>("Forløbet findes ikke", HttpStatus.BAD_REQUEST);
		}
		
		Party party = partyService.getById(partyProgressDto.getPartyId());
		if (party == null) {
			return new ResponseEntity<>("Parten findes ikke", HttpStatus.BAD_REQUEST);
		}
		
		Progress progress = progressService.getById(partyProgressDto.getProgressId());
		if (progress == null) {
			return new ResponseEntity<>("Fremskridtet findes ikke", HttpStatus.BAD_REQUEST);
		}
		
		progress.setProgressDate(LocalDate.now());
		progress.setScore(partyProgressDto.getSelectedRating());
		progress.setScoredByProfessional(partyProgressDto.isProfessionalOpinion());
		progress.setProgressComment(partyProgressDto.getComment());
		
		progressService.save(progress);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@SecurityLogIntercepted(operation = "Hent grafdata fra forløb", args = {"id", "to", "from"})
	@GetMapping("/rest/progress/fetchchartdata/{id}")
	public ResponseEntity<?> editProgress(Model model, @PathVariable long id, @RequestParam(required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date to, @RequestParam(required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date from) {
		Course course  = courseService.getById(id);
		if (course == null) {
			return new ResponseEntity<>("Forløbet findes ikke", HttpStatus.BAD_REQUEST);
		}
		//subtracting 1 day, to include the whole day in the search
		LocalDate fromDate = from == null ? null : convertToLocalDate(from).minusDays(1);
		//adding 1 day, to include the whole day in the search
		LocalDate toDate = to == null ? null : convertToLocalDate(to).plusDays(1);
		
		return new ResponseEntity<>(courseService.getChartDto(course, fromDate, toDate), HttpStatus.OK);
	}
	
	private LocalDate convertToLocalDate(Date dateToConvert) {
		return Instant.ofEpochMilli(dateToConvert.getTime())
		      .atZone(ZoneId.systemDefault())
		      .toLocalDate();
	}
}

