package dk.digitalidentity.sbf.controller.mvc.dto;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import dk.digitalidentity.sbf.dao.model.SubGoal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubGoalDto {
	private Long id;
	private String title;
	private String action;
	private String dateString;
	private boolean trialActionInitiated;
	private boolean trialActionApproved;
	private List<ParticipantSelectDto> selected;
	private List<ParticipantSelectDto> possible;
	
	
	public SubGoalDto(SubGoal subGoal) {
		this.id = subGoal.getId();
		this.title = subGoal.getTitle();
		this.action = subGoal.getAction();
		
		if (subGoal.getStartDate() == null) {
			this.dateString = "";
		} else {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	        String formattedDate = subGoal.getStartDate().format(formatter);
			this.dateString = formattedDate;
		}
		
		this.trialActionInitiated = subGoal.isTrialActionInitiated();
		this.trialActionApproved = subGoal.isTrialActionApproved();
		
		List<ParticipantSelectDto> tempSelected = new ArrayList<>();
		tempSelected.addAll(subGoal.getResponsibleParties().stream().filter(p -> p.isActive()).map(p -> ParticipantSelectDto.builder().id(p.getId()).type(ParticipantTypeDto.PARTY).textString(p.getFirstName() + " (" + p.getCpr() + ")").build()).collect(Collectors.toList()));
		tempSelected.addAll(subGoal.getResponsibleInternalContacts().stream().filter(i -> i.isActive()).map(i -> ParticipantSelectDto.builder().id(i.getId()).type(ParticipantTypeDto.INTERNAL_CONTACT).textString(i.getEmployee().getFirstName() + " (" + i.getRelationship() + ")").build()).collect(Collectors.toList()));
		tempSelected.addAll(subGoal.getResponsibleExternalContacts().stream().filter(e -> e.isActive()).map(e -> ParticipantSelectDto.builder().id(e.getId()).type(ParticipantTypeDto.EXTERNAL_CONTACT).textString(e.getFirstName() + " (" + e.getRelationship() + ")").build()).collect(Collectors.toList()));
		tempSelected.addAll(subGoal.getResponsiblePartyRepresentatives().stream().filter(p -> p.isActive()).map(p -> ParticipantSelectDto.builder().id(p.getId()).type(ParticipantTypeDto.PARTY_REPRESENTATIVE).textString(p.getFirstName() + " (" + p.getRelationship() + ")").build()).collect(Collectors.toList()));
		this.selected = tempSelected;
		
		List<Long> selectedPartyIds = subGoal.getResponsibleParties().stream().filter(p -> p.isActive()).map(p -> p.getId()).collect(Collectors.toList());
		List<Long> selectedInternalContactIds = subGoal.getResponsibleInternalContacts().stream().filter(i -> i.isActive()).map(i -> i.getId()).collect(Collectors.toList());
		List<Long> selectedExternalContactIds = subGoal.getResponsibleExternalContacts().stream().filter(e -> e.isActive()).map(e -> e.getId()).collect(Collectors.toList());
		List<Long> selectedPartyRepresentativeIds = subGoal.getResponsiblePartyRepresentatives().stream().filter(p -> p.isActive()).map(p -> p.getId()).collect(Collectors.toList());
		
		List<ParticipantSelectDto> tempPossible = new ArrayList<>();
		tempPossible.addAll(subGoal.getGoal().getMeetingParty().getMeeting().getCourse().getParties().stream().filter(p -> p.isActive() && !selectedPartyIds.contains(p.getId())).map(p -> ParticipantSelectDto.builder().id(p.getId()).type(ParticipantTypeDto.PARTY).textString(p.getFirstName() + " (" + p.getCpr() + ")").build()).collect(Collectors.toList()));
		tempPossible.addAll(subGoal.getGoal().getMeetingParty().getMeeting().getCourse().getInternalContacts().stream().filter(i -> i.isActive() && !selectedInternalContactIds.contains(i.getId())).map(i -> ParticipantSelectDto.builder().id(i.getId()).type(ParticipantTypeDto.INTERNAL_CONTACT).textString(i.getEmployee().getFirstName() + " (" + i.getRelationship() + ")").build()).collect(Collectors.toList()));
		tempPossible.addAll(subGoal.getGoal().getMeetingParty().getMeeting().getCourse().getExternalContacts().stream().filter(e -> e.isActive() && !selectedExternalContactIds.contains(e.getId())).map(e -> ParticipantSelectDto.builder().id(e.getId()).type(ParticipantTypeDto.EXTERNAL_CONTACT).textString(e.getFirstName() + " (" + e.getRelationship() + ")").build()).collect(Collectors.toList()));
		tempPossible.addAll(subGoal.getGoal().getMeetingParty().getMeeting().getCourse().getPartyRepresentatives().stream().filter(p -> p.isActive() && !selectedPartyRepresentativeIds.contains(p.getId())).map(p -> ParticipantSelectDto.builder().id(p.getId()).type(ParticipantTypeDto.PARTY_REPRESENTATIVE).textString(p.getFirstName() + " (" + p.getRelationship() + ")").build()).collect(Collectors.toList()));
		this.possible = tempPossible;
		
	}
}
