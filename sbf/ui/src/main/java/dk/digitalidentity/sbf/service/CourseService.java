package dk.digitalidentity.sbf.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dk.digitalidentity.sbf.config.SBFConfiguration;
import dk.digitalidentity.sbf.dao.CourseDao;
import dk.digitalidentity.sbf.dao.model.Course;
import dk.digitalidentity.sbf.dao.model.Employee;
import dk.digitalidentity.sbf.dao.model.Party;
import dk.digitalidentity.sbf.security.SecurityUtil;
import dk.digitalidentity.sbf.service.dto.CourseProgressChartDto;
import dk.digitalidentity.sbf.service.dto.CourseSearchDto;
import dk.digitalidentity.sbf.service.dto.MeetingDateDto;
import dk.digitalidentity.sbf.service.dto.PartyProgressChartDto;
import dk.digitalidentity.sbf.service.dto.ProgressDto;

@Service
public class CourseService {
	
	@Autowired
	private CourseDao courseDao;
	
	@Autowired 
	private SBFConfiguration sbfConfiguration;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private InternalContactService internalContactService;
	
	public Course getById(long id) {
		return courseDao.findById(id);
	}
	
	public Course save(Course course) {
		return courseDao.save(course);
	}
	
	public List<CourseSearchDto> search(String query, String sort, boolean onlyCoordinator, boolean onlyMine, boolean onlyMultipleParties, boolean onlyOneParty, boolean onlyInactive, boolean onlyActive ) {
		List<CourseSearchDto> result = new ArrayList<>();
		List<Course> all = courseDao.findAll().stream().filter(c -> !c.isDraft()).collect(Collectors.toList());
		
		if (onlyInactive) {
			all = all.stream().filter(c -> !c.isActive()).collect(Collectors.toList());
		} else if (onlyActive) {
			all = all.stream().filter(c -> c.isActive()).collect(Collectors.toList());
		}
		
		if (onlyMultipleParties) {
			all = all.stream().filter(c -> c.getParties().size() > 1).collect(Collectors.toList());
		} else if (onlyOneParty) {
			all = all.stream().filter(c -> c.getParties().size() == 1).collect(Collectors.toList());
		}
		
		if (onlyCoordinator) {
			Employee employee = employeeService.getByUuid(SecurityUtil.getUser().getUuid());
			if (employee != null) {
				all = all.stream().filter(c -> c.getPrimaryInternalContact().getEmployee().getUuid().equals(employee.getUuid())).collect(Collectors.toList());
			}
			
		} else if (onlyMine) {
			Employee employee = employeeService.getByUuid(SecurityUtil.getUser().getUuid());
			if (employee != null) {
				List<Long> internalContactsCourseIds = internalContactService.getByEmployee(employee).stream().filter(i -> i.isActive()).map(i -> i.getCourse().getId()).collect(Collectors.toList());
				all = all.stream().filter(c -> internalContactsCourseIds.contains(c.getId())).collect(Collectors.toList());
			}
		}
		
		for (Course course : all) {
			boolean match = false;
			for (Party party : course.getParties().stream().filter(p -> p.isActive()).collect(Collectors.toList())) {
				if (validCpr(query)) {
					if (party.getCpr().equals(query)) {
						match=true;
						break;
					}
				} else {
					if ((party.getFirstName().toLowerCase() + " " + party.getSurname().toLowerCase()).contains(query.toLowerCase())) {
						match=true;
						break;
					}
				}
			}
			if (match) {
				result.add(new CourseSearchDto(sbfConfiguration.getMandatoryMeetings(), course));
			}
		}

		result.sort((lhs, rhs) -> {
			// Order by active then by chosen date (start or next meeting)
			if( lhs.isActive() && !rhs.isActive())
			{
				return -1;
			}
			else if (!lhs.isActive() && rhs.isActive())
			{
				return 1;
			}
			else
			{
				var lhsCompareDate = sort.equals("start") ? lhs.getStartDate()  : lhs.getNextMeeting();
				if( lhsCompareDate == null) { lhsCompareDate = LocalDateTime.MIN; }
				var rhsCompareDate = sort.equals("start") ? rhs.getStartDate() : rhs.getNextMeeting();
				if (rhsCompareDate == null) { rhsCompareDate = LocalDateTime.MIN; }
				return lhsCompareDate.compareTo(rhsCompareDate);
			}
		});
		return result;
	}
	
	public CourseProgressChartDto getChartDto(Course course, LocalDate from, LocalDate to) {
		CourseProgressChartDto dto = new CourseProgressChartDto();
		
		//Meeting dates
		List<MeetingDateDto> meetingDateDtos = null;
		if (from != null) {
			meetingDateDtos = course.getMeetings().stream().filter(m -> m.getMeetingTime() != null).filter(m -> m.getMeetingTime().isAfter(from.atStartOfDay()) && m.getMeetingTime().isBefore(to.atStartOfDay())).map(m -> new MeetingDateDto(m.getMeetingTime())).collect(Collectors.toList());
		} else {
			meetingDateDtos = course.getMeetings().stream().filter(m -> m.getMeetingTime() != null).filter(m -> m.getMeetingTime().isBefore(to.atStartOfDay())).map(m -> new MeetingDateDto(m.getMeetingTime())).collect(Collectors.toList());
		}
		dto.setMeetingDates(meetingDateDtos);
		
		
		//all progress dates
		Set<String> progressDateStrings = new HashSet<>();
		if (from != null) {
			List<String> tempProgressDateStrings = course.getProgresses().stream().filter(p -> p.getProgressDate().isAfter(from) && p.getProgressDate().isBefore(to)).map(p -> p.getProgressDate().toString().split("T")[0]).collect(Collectors.toList());
			progressDateStrings.addAll(tempProgressDateStrings);
		} else {
			List<String> tempProgressDateStrings = course.getProgresses().stream().filter(p -> p.getProgressDate().isBefore(to)).map(p -> p.getProgressDate().toString().split("T")[0]).collect(Collectors.toList());
			progressDateStrings.addAll(tempProgressDateStrings);
		}
		dto.setAllProgressDates(progressDateStrings);
		
		//handeling party progress stuff
		List<PartyProgressChartDto> partyProgressChartDtos = new ArrayList<>();
		int counter = 1;
		for (Party party : course.getParties()) {
			PartyProgressChartDto partyProgressChartDto = new PartyProgressChartDto();
			partyProgressChartDto.setName(party.getFirstName() + " " + party.getSurname());
			partyProgressChartDto.setXIdentifier("x" + counter);
			
			List<ProgressDto> progresses = new ArrayList<>();
			if (from != null) {
				List<ProgressDto> tempProgresses = party.getProgresses().stream().filter(p -> p.getProgressDate().isAfter(from) && p.getProgressDate().isBefore(to)).map(p -> new ProgressDto(p)).collect(Collectors.toList());
				progresses.addAll(tempProgresses);
			} else {
				List<ProgressDto> tempProgresses = party.getProgresses().stream().filter(p -> p.getProgressDate().isBefore(to)).map(p -> new ProgressDto(p)).collect(Collectors.toList());
				progresses.addAll(tempProgresses);
			}
			partyProgressChartDto.setProgresses(progresses);
			
			partyProgressChartDtos.add(partyProgressChartDto);
			
			counter++;
			
		}
		dto.setParties(partyProgressChartDtos);
		
		return dto;
	}
	
	@Transactional
	public void deleteDrafts() {
		LocalDateTime aDayAgo = LocalDateTime.now().minusDays(1);
		List<Course> drafts = courseDao.findAll().stream().filter(c -> c.isDraft() && c.getCreated().isBefore(aDayAgo)).collect(Collectors.toList());
		courseDao.deleteAll(drafts);
	}
	
	private boolean validCpr(String cpr) {
		if (cpr == null || cpr.length() != 10) {
			return false;
		}
		
		for (char c : cpr.toCharArray()) {
			if (!Character.isDigit(c)) {
				return false;
			}
		}
		
		int days = Integer.parseInt(cpr.substring(0, 2));
		int month = Integer.parseInt(cpr.substring(2, 4));

		if (days < 1 || days > 31) {
			return false;
		}
		
		if (month < 1 || month > 12) {
			return false;
		}

		return true;
	}
}
