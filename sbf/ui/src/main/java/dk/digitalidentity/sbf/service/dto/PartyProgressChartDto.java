package dk.digitalidentity.sbf.service.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PartyProgressChartDto {
	private String name;
	private String xIdentifier;
	private List<ProgressDto> progresses;
}
