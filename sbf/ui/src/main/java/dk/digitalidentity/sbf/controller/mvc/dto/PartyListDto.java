package dk.digitalidentity.sbf.controller.mvc.dto;

import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;

import dk.digitalidentity.sbf.config.modules.MandatoryMeetings;
import dk.digitalidentity.sbf.dao.model.Party;
import dk.digitalidentity.sbf.dao.model.enums.ConsentType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PartyListDto {
	private Long id;
	private String cpr;
	private String name;
	private String surname;
	private String address;
	private String city;
	private String postCode;
	private LocalDateTime birthday;
	private String email;
	private String phone;
	private ConsentType consentType;
	private LocalDateTime consentDate;
	private boolean sixteenYearMeetingHeld;
	private LocalDateTime sixteenYearMeetingDate;
	private boolean eighteenYearMeetingHeld;
	private LocalDateTime eighteenYearMeetingDate;
	private boolean show16 = false;
	private boolean show18 = false;
	private String esdhCaseReference;
	private String esdhLinkReference;
	private String esdhCaseDisplayName;
	
	public PartyListDto(Party party, MandatoryMeetings mandatory) {
		this.id = party.getId();
		this.cpr = party.getCpr();
		this.name = party.getFirstName();
		this.surname = party.getSurname();
		this.address = party.getAddress();
		this.city = party.getCity();
		this.postCode = party.getPostCode();
		this.birthday = party.getBirthDate();
		this.email = party.getEmail();
		this.phone = party.getPhone();
		this.consentType = party.getConsentType();
		this.consentDate = party.getConsentDate();
		this.sixteenYearMeetingHeld = party.isSixteenYearMeetingHeld();
		this.sixteenYearMeetingDate = party.getSixteenYearMeetingDate();
		this.eighteenYearMeetingHeld = party.isEighteenYearMeetingHeld();
		this.eighteenYearMeetingDate = party.getEighteenYearMeetingDate();
		this.esdhCaseReference = party.getEsdhCaseReference();
		this.esdhCaseDisplayName = party.getEsdhCaseDisplayName();
		this.esdhLinkReference = party.getEsdhLinkReference();
		
		long monthsBetween = ChronoUnit.MONTHS.between(
				YearMonth.from(this.birthday), 
			    YearMonth.from(LocalDateTime.now())
		);
		
		if (monthsBetween >= mandatory.getSixteenMeetingIntervalStartInMonths() && monthsBetween <= mandatory.getSixteenMeetingIntervalStopInMonths()) {
			this.show16 = true;
		}
		if (monthsBetween >= mandatory.getEighteenMeetingIntervalStartInMonths() && monthsBetween <= mandatory.getEighteenMeetingIntervalStopInMonths()) {
			this.show18 = true;
		}
		
		
	}
}
