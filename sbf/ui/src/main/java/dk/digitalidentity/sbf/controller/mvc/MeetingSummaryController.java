package dk.digitalidentity.sbf.controller.mvc;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dk.digitalidentity.sbf.controller.mvc.dto.SubGoalDto;
import dk.digitalidentity.sbf.dao.model.Course;
import dk.digitalidentity.sbf.dao.model.Goal;
import dk.digitalidentity.sbf.dao.model.MeetingParty;
import dk.digitalidentity.sbf.interceptors.SecurityLogIntercepted;
import dk.digitalidentity.sbf.service.CourseService;
import dk.digitalidentity.sbf.service.GoalService;
import dk.digitalidentity.sbf.service.MeetingPartyService;

@Controller
public class MeetingSummaryController {
	
	@Autowired
	private CourseService courseService;
	
	@Autowired
	private MeetingPartyService meetingPartyService;
	
	@Autowired
	private GoalService goalService;
	
	@SecurityLogIntercepted(operation = "Tilgår mødereferatssiden", args = {"courseId"})
	@GetMapping("/summary")
	public String coursePage(Model model, @RequestParam Long courseId) {
		Course course = courseService.getById(courseId);
		
		if (course == null) {
			return "redirect:/error";
		}
		
		model.addAttribute("meeting", course.getActiveMeeting());
		model.addAttribute("course", course);
		var parties = course.getActiveMeeting().getMeetingParties().stream().filter(p -> p.getParty().isActive()).collect(Collectors.toList());
		model.addAttribute("parties", parties);
		model.addAttribute("firstPartyId", parties.get(0).getId());
		var partyRepresentatives = course.getActiveMeeting().getMeetingPartyRepresentatives().stream().filter(p -> p.getPartyRepresentative().isActive()).collect(Collectors.toList());
		model.addAttribute("partyRepresentatives", partyRepresentatives);
		var meetingInternalContacts = course.getActiveMeeting().getMeetingInternalContacts().stream().filter(p -> p.getInternalContact().isActive()).collect(Collectors.toList());
		model.addAttribute("meetingInternalContacts", meetingInternalContacts);
		var meetingExternalContacts = course.getActiveMeeting().getMeetingExternalContacts().stream().filter(p -> p.getExternalContact().isActive()).collect(Collectors.toList());
		model.addAttribute("meetingExternalContacts", meetingExternalContacts);

		model.addAttribute("parties", parties);

		return "course/meeting_summary";
	}
	
	@GetMapping("/summary/fragments/dreamtab")
	public String dreamTabFragment(Model model, @RequestParam Long meetingPartyId) {
		MeetingParty meetingParty = meetingPartyService.getById(meetingPartyId);
		
		if (meetingParty == null) {
			return "redirect:/error";
		}
		
		model.addAttribute("meetingParty", meetingParty);
		List<Goal> activeGoals = meetingParty.getGoals().stream().filter(g -> g.isActive()).collect(Collectors.toList());
		model.addAttribute("goals", activeGoals);
		model.addAttribute("firstGoalId", activeGoals.isEmpty() ? "" : activeGoals.get(0).getId());
		
		return "course/fragments/dream_tab_fragment :: dreamTab";
	}
	
	@GetMapping("/summary/fragments/goaltab")
	public String summaryTabFragment(Model model, @RequestParam Long goalId) {
		Goal goal = goalService.getById(goalId);
		
		if (goal == null) {
			return "redirect:/error";
		}
		
		model.addAttribute("goal", goal);
		
		return "course/fragments/goal_tab_fragment :: goalTab";
	}
	
	@GetMapping("/summary/fragments/subgoals")
	public String subGoalsTabFragment(Model model, @RequestParam Long goalId) {
		Goal goal = goalService.getById(goalId);
		
		if (goal == null) {
			return "redirect:/error";
		}
		
		model.addAttribute("subGoals", goal.getSubGoals().stream().filter(s -> !s.isClosed()).map(s -> new SubGoalDto(s)).collect(Collectors.toList()));
		
		return "course/fragments/sub_goals :: searchResult";
	}
}
