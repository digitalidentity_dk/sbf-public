package dk.digitalidentity.sbf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import dk.digitalidentity.sbf.dao.GoalDao;
import dk.digitalidentity.sbf.dao.model.Goal;

@Service
public class GoalService {
	
	@Autowired
	private GoalDao goalDao;
	
	public Goal getById(long id) {
		return goalDao.findById(id);
	}
	
	public Goal save(Goal goal) {
		return goalDao.save(goal);
	}
}
