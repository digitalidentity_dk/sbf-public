package dk.digitalidentity.sbf.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sbf.dao.MeetingPartyDao;
import dk.digitalidentity.sbf.dao.model.Meeting;
import dk.digitalidentity.sbf.dao.model.MeetingParty;
import dk.digitalidentity.sbf.dao.model.Party;

@Service
public class MeetingPartyService {
	
	@Autowired
	private MeetingPartyDao meetingPartyDao;
	
	public MeetingParty getById(long id) {
		return meetingPartyDao.findById(id);
	}
	
	public MeetingParty save(MeetingParty meetingParty) {
		return meetingPartyDao.save(meetingParty);
	}
	
	public MeetingParty createAndSave(Party party, Meeting activeMeeting) {
		MeetingParty meetingParty = new MeetingParty();
        meetingParty.setParty(party);
        meetingParty.setMeeting(activeMeeting);
		return meetingPartyDao.save(meetingParty);
	}
	
	public void createAndSaveAll(List<Party> parties, Meeting activeMeeting) {
		for (Party party : parties) {
			MeetingParty meetingParty = new MeetingParty();
			meetingParty.setParty(party);
			meetingParty.setMeeting(activeMeeting);
			meetingPartyDao.save(meetingParty);
		}
	}
	
	public MeetingParty getByPartyAndMeeting(Party party, Meeting meeting) {
		return meetingPartyDao.findByPartyAndMeeting(party, meeting);
	}
}
