package dk.digitalidentity.sbf.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.sbf.dao.EmployeeDao;
import dk.digitalidentity.sbf.dao.model.Employee;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeDao employeeDao;
	
	public Employee getByUuid(String string) {
		return employeeDao.findByUuid(string);
	}
	
	public Employee save(Employee employee) {
		return employeeDao.save(employee);
	}
	
	public List<Employee> searchEmployees(String query) {
		var result = employeeDao.search(query);
		return result;
	}
} 
