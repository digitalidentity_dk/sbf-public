package dk.digitalidentity.sbf.controller.rest;

import dk.digitalidentity.sbf.controller.rest.dto.CprESDHDto;
import dk.digitalidentity.sbf.controller.rest.dto.CreatePartyResponseDTO;
import dk.digitalidentity.sbf.controller.rest.dto.ESDHResponseDTO;
import dk.digitalidentity.sbf.controller.rest.dto.ExternalDto;
import dk.digitalidentity.sbf.controller.rest.dto.InternalDto;
import dk.digitalidentity.sbf.controller.rest.dto.PartyDto;
import dk.digitalidentity.sbf.controller.rest.dto.PartyRepresentativeDto;
import dk.digitalidentity.sbf.dao.model.*;
import dk.digitalidentity.sbf.interceptors.SecurityLogIntercepted;
import dk.digitalidentity.sbf.service.*;
import dk.digitalidentity.sbf.service.cpr.ICprService;
import dk.digitalidentity.sbf.service.cpr.dto.CprLookupDto;
import dk.digitalidentity.sbf.service.dto.ESDHCase;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class CourseRestController {
    @Autowired
    private ICprService cprService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private PartyService partyService;

    @Autowired
    private PartyRepresentativeService partyRepresentativeService;

    @Autowired
    private ExternalContactService externalContactService;

    @Autowired
    private InternalContactService internalContactService;

    @Autowired
    private EmployeeService employeeService;
    
    @Autowired
    private MeetingService meetingService;
    
    @Autowired
    private MeetingPartyService meetingPartyService;
    
    @Autowired
    private MeetingPartyRepresentativeService meetingPartyRepresentativeService;
    
    @Autowired
    private MeetingExternalContactService meetingExternalContactService;
    
    @Autowired
    private MeetingInternalContactService meetingInternalContactService;
    
    @Autowired
    private ESDHService esdhService;
    
    @SecurityLogIntercepted(operation = "CPR-opslag", args = {"cpr"})
    @GetMapping("/rest/course/cprlookup/{cpr}")
    public ResponseEntity<?> cprLookup(Model model, @PathVariable String cpr, @RequestParam(required = false) Long courseId) throws Exception {
        CprLookupDto cprLookupDto = cprService.cprLookup(cpr);
        if( cprLookupDto == null )
        {
            return new ResponseEntity<>("Kunne ikke gennemføre cpr-opslag", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        CprESDHDto response = new CprESDHDto();
        response.setCprLookupDto(cprLookupDto);
        response.setBelongsToActiveCourse(partyService.belongsToActiveCourse(cpr));
        response.setPreviouslyAssociated(partyService.getPreviouslyAssociated(cpr, courseId) != null);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Tilføj part", args = {"courseId", "partyDto"})
    @ResponseBody
    @PostMapping("/rest/course/addparty")
    public ResponseEntity<?> addParty(Model model, @RequestParam(required = false) Long courseId, @Valid @RequestBody PartyDto partyDto) {
        if (StringUtils.isEmpty(partyDto.getCpr()) || StringUtils.isEmpty(partyDto.getName()) || StringUtils.isEmpty(partyDto.getSurname()) || StringUtils.isEmpty(partyDto.getBirthday()) || StringUtils.isEmpty(partyDto.getAddress()) || StringUtils.isEmpty(partyDto.getCity()) || StringUtils.isEmpty(partyDto.getPostCode())) {
            return new ResponseEntity<>("Et eller flere krævede felter mangler en værdi", HttpStatus.BAD_REQUEST);
        }
        Course course = null;
        if (courseId == null) {
            Course tempCourse = new Course();
            tempCourse.setCreated(LocalDateTime.now());
            tempCourse.setDraft(true);
            tempCourse.setActive(true);
            course = courseService.save(tempCourse);
        } else {
            course = courseService.getById(courseId);
        }

        if (course == null) {
            return new ResponseEntity<>("Der mangler et forløb", HttpStatus.BAD_REQUEST);
        }

        Party tempParty = null;
        if (course.getParties() != null) {
            List<String> cprs = course.getParties().stream().filter(p -> p.isActive()).map(p -> p.getCpr()).collect(Collectors.toList());
            if (cprs.contains(partyDto.getCpr())) {
                return new ResponseEntity<>("Der findes allerede en part med dette cpr på dette forløb", HttpStatus.BAD_REQUEST);
            }
            
            tempParty = partyService.getPreviouslyAssociated(partyDto.getCpr(), courseId);
        }
        var reactivating = false;
        if (tempParty != null) {
    		tempParty.setActive(true);
            reactivating = true;
        }
        else {
        	tempParty = new Party();
        	tempParty.setFirstName(partyDto.getName());
            tempParty.setSurname(partyDto.getSurname());
            tempParty.setAddress(partyDto.getAddress());
            tempParty.setCity(partyDto.getCity());
            tempParty.setPostCode(partyDto.getPostCode());
            tempParty.setPhone(partyDto.getPhone());
            tempParty.setEmail(partyDto.getEmail());
            tempParty.setCpr(partyDto.getCpr());

            tempParty.setConsentType(partyDto.getConsentType());
            if (!StringUtils.isEmpty(partyDto.getConsentDate())) {
                tempParty.setConsentDate(LocalDateTime.of(Integer.parseInt(partyDto.getConsentDate().split("/")[2]), Integer.parseInt(partyDto.getConsentDate().split("/")[1]), Integer.parseInt(partyDto.getConsentDate().split("/")[0]), 0, 0));
            } else {
                tempParty.setConsentDate(null);
            }
            tempParty.setBirthDate(LocalDateTime.of(Integer.parseInt(partyDto.getBirthday().split("/")[2]), Integer.parseInt(partyDto.getBirthday().split("/")[1]), Integer.parseInt(partyDto.getBirthday().split("/")[0]), 0, 0));

            tempParty.setSixteenYearMeetingHeld(partyDto.isMandatoryMeetingHeld16());
            tempParty.setEighteenYearMeetingHeld(partyDto.isMandatoryMeetingHeld18());

            if (!StringUtils.isEmpty(partyDto.getMandatoryMeetingDate16())) {
                String[] split = partyDto.getMandatoryMeetingDate16().split("/");
                tempParty.setSixteenYearMeetingDate(LocalDateTime.of(Integer.parseInt(split[2]), Integer.parseInt(split[1]), Integer.parseInt(split[0]), 0, 0));
            } else {
                tempParty.setSixteenYearMeetingDate(null);
            }

            if (!StringUtils.isEmpty(partyDto.getMandatoryMeetingDate18())) {
                String[] split = partyDto.getMandatoryMeetingDate18().split("/");
                tempParty.setEighteenYearMeetingDate(LocalDateTime.of(Integer.parseInt(split[2]), Integer.parseInt(split[1]), Integer.parseInt(split[0]), 0, 0));
            } else {
                tempParty.setEighteenYearMeetingDate(null);
            }

            tempParty.setCreatedDate(LocalDateTime.now());
            tempParty.setCourse(course);
            tempParty.setActive(true);
        }
        
        
        Party party = partyService.save(tempParty);

        // reopen case in ESDH if it exists
        if( reactivating && !StringUtils.isEmpty(tempParty.getEsdhCaseReference() ) )
        {
            esdhService.openCase(tempParty.getEsdhCaseReference());
        }

        boolean caseCreationFailed = false;
        //a new case should be created
        if (!course.isDraft() && StringUtils.isEmpty(party.getEsdhCaseReference())) {
            ESDHCase esdhCase = null;
            try {
                esdhCase = esdhService.createCase(party);
                party.setEsdhCaseReference(esdhCase.getCaseReference());
                party.setEsdhCaseDisplayName(esdhCase.getDisplayName());
                party.setEsdhLinkReference(esdhCase.getLinkReference());
            } catch (Exception e) {
                log.error("Failed to create case for party with id: " + party.getId(), e);
            }
            
            if (StringUtils.isEmpty(party.getEsdhCaseReference())) {
            	caseCreationFailed = true;
            }
        }
        
        party = partyService.save(tempParty);
        
        //Creating a MeetingParty for the active meeting if the course is not a draft and there isn't an existing meetingParty
        if (!course.isDraft() && meetingPartyService.getByPartyAndMeeting(party, course.getActiveMeeting()) == null) {
    		meetingPartyService.createAndSave(party, course.getActiveMeeting());
        }
        
        CreatePartyResponseDTO response = new CreatePartyResponseDTO();
        response.setCaseCreationFailed(caseCreationFailed);
        response.setCourseId(course.getId());
        
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Tilføj partsrepræsentant", args = {"courseId", "partyRepresentativeDto"})
    @ResponseBody
    @PostMapping("/rest/course/addpartyrepresentative")
    public ResponseEntity<?> addPartyRepresentative(Model model, @RequestParam(required = false) Long courseId, @Valid @RequestBody PartyRepresentativeDto partyRepresentativeDto) {
        if (StringUtils.isEmpty(partyRepresentativeDto.getCpr()) || StringUtils.isEmpty(partyRepresentativeDto.getName()) || StringUtils.isEmpty(partyRepresentativeDto.getSurname()) || StringUtils.isEmpty(partyRepresentativeDto.getRelationship()) || StringUtils.isEmpty(partyRepresentativeDto.getAddress()) || StringUtils.isEmpty(partyRepresentativeDto.getCity()) || StringUtils.isEmpty(partyRepresentativeDto.getPostCode())) {
            return new ResponseEntity<>("Et eller flere krævede felter mangler en værdi", HttpStatus.BAD_REQUEST);
        }
        Course course = null;
        if (courseId == null) {
            Course tempCourse = new Course();
            tempCourse.setCreated(LocalDateTime.now());
            tempCourse.setDraft(true);
            tempCourse.setActive(true);
            course = courseService.save(tempCourse);
        } else {
            course = courseService.getById(courseId);
        }

        if (course == null) {
            return new ResponseEntity<>("Der mangles et forløb", HttpStatus.BAD_REQUEST);
        }

        if (course.getPartyRepresentatives() != null) {
            List<String> cprs = course.getPartyRepresentatives().stream().map(p -> p.getCpr()).collect(Collectors.toList());
            if (cprs.contains(partyRepresentativeDto.getCpr())) {
                return new ResponseEntity<>("Der findes allerede en partsrepræsentant med dette cpr på dette forløb", HttpStatus.BAD_REQUEST);
            }
        }


        PartyRepresentative tempPartyRepresentative = new PartyRepresentative();
        tempPartyRepresentative.setFirstName(partyRepresentativeDto.getName());
        tempPartyRepresentative.setSurname(partyRepresentativeDto.getSurname());
        tempPartyRepresentative.setAddress(partyRepresentativeDto.getAddress());
        tempPartyRepresentative.setCity(partyRepresentativeDto.getCity());
        tempPartyRepresentative.setPostCode(partyRepresentativeDto.getPostCode());
        tempPartyRepresentative.setPhone(partyRepresentativeDto.getPhone());
        tempPartyRepresentative.setEmail(partyRepresentativeDto.getEmail());
        tempPartyRepresentative.setCpr(partyRepresentativeDto.getCpr());
        tempPartyRepresentative.setRelationship(partyRepresentativeDto.getRelationship());
        tempPartyRepresentative.setCreatedDate(LocalDateTime.now());
        tempPartyRepresentative.setCourse(course);
        tempPartyRepresentative.setActive(true);

        PartyRepresentative partyRepresentative = partyRepresentativeService.save(tempPartyRepresentative);
        
        //Creating a MeetingPartyRepresentative for the active meeting if the course is not a draft
        if (!course.isDraft()) {
        	meetingPartyRepresentativeService.createAndSave(partyRepresentative, course.getActiveMeeting());
        }

        return new ResponseEntity<>(course.getId(), HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Rediger part", args = {"courseId", "partyDto"})
    @ResponseBody
    @PostMapping("/rest/course/editparty")
    public ResponseEntity<?> editParty(Model model, @RequestParam Long courseId, @Valid @RequestBody PartyDto partyDto) {
        if (StringUtils.isEmpty(partyDto.getId()) || StringUtils.isEmpty(partyDto.getCpr()) || StringUtils.isEmpty(partyDto.getName()) || StringUtils.isEmpty(partyDto.getSurname()) || StringUtils.isEmpty(partyDto.getBirthday()) || StringUtils.isEmpty(partyDto.getAddress()) || StringUtils.isEmpty(partyDto.getCity()) || StringUtils.isEmpty(partyDto.getPostCode())) {
            return new ResponseEntity<>("Et eller flere krævede felter mangler en værdi", HttpStatus.BAD_REQUEST);
        }
        Course course = courseService.getById(courseId);
        Party party = partyService.getById(Long.parseLong(partyDto.getId()));

        if (course == null) {
            return new ResponseEntity<>("Der mangles et forløb", HttpStatus.BAD_REQUEST);
        }

        if (party == null) {
            return new ResponseEntity<>("Den part der er valgt, findes ikke i databasen", HttpStatus.BAD_REQUEST);
        }

        if (!party.isActive()) {
            return new ResponseEntity<>("Den part der er valgt, er inaktiv", HttpStatus.BAD_REQUEST);
        }
        party.setPhone(partyDto.getPhone());
        party.setEmail(partyDto.getEmail());

        party.setConsentType(partyDto.getConsentType());
        if (!StringUtils.isEmpty(partyDto.getConsentDate())) {
            party.setConsentDate(LocalDateTime.of(Integer.parseInt(partyDto.getConsentDate().split("/")[2]), Integer.parseInt(partyDto.getConsentDate().split("/")[1]), Integer.parseInt(partyDto.getConsentDate().split("/")[0]), 0, 0));
        } else {
            party.setConsentDate(null);
        }

        party.setSixteenYearMeetingHeld(partyDto.isMandatoryMeetingHeld16());
        party.setEighteenYearMeetingHeld(partyDto.isMandatoryMeetingHeld18());

        if (!StringUtils.isEmpty(partyDto.getMandatoryMeetingDate16())) {
            String[] split = partyDto.getMandatoryMeetingDate16().split("/");
            party.setSixteenYearMeetingDate(LocalDateTime.of(Integer.parseInt(split[2]), Integer.parseInt(split[1]), Integer.parseInt(split[0]), 0, 0));
        } else {
            party.setSixteenYearMeetingDate(null);
        }

        if (!StringUtils.isEmpty(partyDto.getMandatoryMeetingDate18())) {
            String[] split = partyDto.getMandatoryMeetingDate18().split("/");
            party.setEighteenYearMeetingDate(LocalDateTime.of(Integer.parseInt(split[2]), Integer.parseInt(split[1]), Integer.parseInt(split[0]), 0, 0));
        } else {
            party.setEighteenYearMeetingDate(null);
        }

        partyService.save(party);

        return new ResponseEntity<>(course.getId(), HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Rediger partsrepræsentant", args = {"courseId", "partyRepresentativeDto"})
    @ResponseBody
    @PostMapping("/rest/course/editpartyrepresentative")
    public ResponseEntity<?> editPartyRepresentative(Model model, @RequestParam Long courseId, @Valid @RequestBody PartyRepresentativeDto partyRepresentativeDto) {
        if (StringUtils.isEmpty(partyRepresentativeDto.getId()) || StringUtils.isEmpty(partyRepresentativeDto.getCpr()) || StringUtils.isEmpty(partyRepresentativeDto.getName()) || StringUtils.isEmpty(partyRepresentativeDto.getSurname()) || StringUtils.isEmpty(partyRepresentativeDto.getRelationship()) || StringUtils.isEmpty(partyRepresentativeDto.getAddress()) || StringUtils.isEmpty(partyRepresentativeDto.getCity()) || StringUtils.isEmpty(partyRepresentativeDto.getPostCode())) {
            return new ResponseEntity<>("Et eller flere krævede felter mangler en værdi", HttpStatus.BAD_REQUEST);
        }
        Course course = courseService.getById(courseId);
        PartyRepresentative partyRepresentative = partyRepresentativeService.getById(Long.parseLong(partyRepresentativeDto.getId()));

        if (course == null) {
            return new ResponseEntity<>("Der mangles et forløb", HttpStatus.BAD_REQUEST);
        }

        if (partyRepresentative == null) {
            return new ResponseEntity<>("Den partsrepræsentant der er valgt, findes ikke i databasen", HttpStatus.BAD_REQUEST);
        }

        if (!partyRepresentative.isActive()) {
            return new ResponseEntity<>("Den partsrepræsentant der er valgt, er inaktiv", HttpStatus.BAD_REQUEST);
        }

        partyRepresentative.setPhone(partyRepresentativeDto.getPhone());
        partyRepresentative.setEmail(partyRepresentativeDto.getEmail());
        partyRepresentative.setRelationship(partyRepresentativeDto.getRelationship());


        partyRepresentativeService.save(partyRepresentative);

        return new ResponseEntity<>(course.getId(), HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Slet part", args = {"id"})
    @PostMapping("/rest/course/deleteparty/{id}")
    public ResponseEntity<?> deleteParty(Model model, @PathVariable Long id) {
        Party party = partyService.getById(id);
        if (party == null) {
            return new ResponseEntity<>("Den part der er valgt, findes ikke i databasen", HttpStatus.BAD_REQUEST);
        }
        if (!party.isActive()) {
            return new ResponseEntity<>("Den part der er valgt, er allerede inaktiv", HttpStatus.BAD_REQUEST);
        }
        if( party.getCourse().isActive())
        {
            try {
                esdhService.closeCase(party.getEsdhCaseReference(), "Part fjernet fra forløb i SBF");
            } catch (Exception e) {
                log.error("Failed to close ESDHCase on party with ID: " + party.getId(),e);
            }
        }
        party.setActive(false);
        partyService.save(party);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Slet partsrepræsentant", args = {"id"})
    @PostMapping("/rest/course/deletepartyrepresentative/{id}")
    public ResponseEntity<?> deletePartyRepresentative(Model model, @PathVariable Long id) {
        PartyRepresentative partyRepresentative = partyRepresentativeService.getById(id);
        if (partyRepresentative == null) {
            return new ResponseEntity<>("Den partsrepræsentant der er valgt, findes ikke i databasen", HttpStatus.BAD_REQUEST);
        }
        if (!partyRepresentative.isActive()) {
            return new ResponseEntity<>("Den partsrepræsentant der er valgt, er allerede inaktiv", HttpStatus.BAD_REQUEST);
        }

        partyRepresentative.setActive(false);
        partyRepresentativeService.save(partyRepresentative);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Opret ekstern kontakt", args = {"courseId", "externalDto"})
    @ResponseBody
    @PostMapping("/rest/course/addexternal")
    public ResponseEntity<?> addExternal(Model model, @RequestParam(required = false) Long courseId, @Valid @RequestBody ExternalDto externalDto) {
        if (StringUtils.isEmpty(externalDto.getName()) || StringUtils.isEmpty(externalDto.getSurname()) || StringUtils.isEmpty(externalDto.getRelationship())) {
            return new ResponseEntity<>("Et eller flere krævede felter mangler en værdi", HttpStatus.BAD_REQUEST);
        }

        Course course = null;
        if (courseId == null) {
            Course tempCourse = new Course();
            tempCourse.setCreated(LocalDateTime.now());
            tempCourse.setDraft(true);
            tempCourse.setActive(true);
            course = courseService.save(tempCourse);
        } else {
            course = courseService.getById(courseId);
        }
        if (course == null) {
            return new ResponseEntity<>("Der mangles et forløb", HttpStatus.BAD_REQUEST);
        }

        ExternalContact tempExternal = new ExternalContact();
        tempExternal.setFirstName(externalDto.getName());
        tempExternal.setSurname(externalDto.getSurname());
        tempExternal.setPhone(externalDto.getPhone());
        tempExternal.setEmail(externalDto.getEmail());
        tempExternal.setRelationship(externalDto.getRelationship());

        tempExternal.setCourse(course);
        tempExternal.setActive(true);

        ExternalContact externalContact = externalContactService.save(tempExternal);
        
        //Creating a MeetingExternalContact for the active meeting if the course is not a draft
        if (!course.isDraft()) {
        	meetingExternalContactService.createAndSave(externalContact, course.getActiveMeeting());
        }

        return new ResponseEntity<>(course.getId(), HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Rediger ekstern kontakt", args = {"courseId", "externalDto"})
    @ResponseBody
    @PostMapping("/rest/course/editexternal")
    public ResponseEntity<?> editExternal(Model model, @RequestParam Long courseId, @Valid @RequestBody ExternalDto externalDto) {
        if (StringUtils.isEmpty(externalDto.getName()) || StringUtils.isEmpty(externalDto.getSurname()) || StringUtils.isEmpty(externalDto.getRelationship())) {
            return new ResponseEntity<>("Et eller flere krævede felter mangler en værdi", HttpStatus.BAD_REQUEST);
        }
        Course course = courseService.getById(courseId);
        if (course == null) {
            return new ResponseEntity<>("Der mangles et forløb", HttpStatus.BAD_REQUEST);
        }

        ExternalContact external = externalContactService.getById(externalDto.getId());
        if (external == null) {
            return new ResponseEntity<>("Den eksterne, der er valgt, findes ikke i databasen", HttpStatus.BAD_REQUEST);
        }

        external.setFirstName(externalDto.getName());
        external.setSurname(externalDto.getSurname());
        external.setPhone(externalDto.getPhone());
        external.setEmail(externalDto.getEmail());
        external.setRelationship(externalDto.getRelationship());

        externalContactService.save(external);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Slet ekstern kontakt", args = {"id"})
    @PostMapping("/rest/course/deleteexternal/{id}")
    public ResponseEntity<?> deleteExternal(Model model, @PathVariable Long id) {
        ExternalContact external = externalContactService.getById(id);
        if (external == null) {
            return new ResponseEntity<>("Den eksterne, der er valgt, findes ikke i databasen", HttpStatus.BAD_REQUEST);
        }
        if (!external.isActive()) {
            return new ResponseEntity<>("Den eksterne, der er valgt, er allerede inaktiv", HttpStatus.BAD_REQUEST);
        }

        external.setActive(false);
        externalContactService.save(external);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Opret intern kontakt", args = {"courseId", "internalDto"})
    @ResponseBody
    @PostMapping("/rest/course/addinternal")
    public ResponseEntity<?> addInternal(Model model, @RequestParam(required = false) Long courseId, @RequestBody InternalDto internalDto) {
        if (StringUtils.isEmpty(internalDto.getEmployeeUuid()) || StringUtils.isEmpty(internalDto.getRelationship())) {
            return new ResponseEntity<>("Et eller flere krævede felter mangler en værdi", HttpStatus.BAD_REQUEST);
        }

        Course course = null;
        if (courseId == null) {
            Course tempCourse = new Course();
            tempCourse.setCreated(LocalDateTime.now());
            tempCourse.setDraft(true);
            tempCourse.setActive(true);
            course = courseService.save(tempCourse);
        } else {
            course = courseService.getById(courseId);
        }
        if (course == null) {
            return new ResponseEntity<>("Der mangles et forløb", HttpStatus.BAD_REQUEST);
        }

        Employee employee = employeeService.getByUuid(internalDto.getEmployeeUuid());
        if (employee == null) {
            return new ResponseEntity<>("Der mangles en medarbejder", HttpStatus.BAD_REQUEST);
        }
        InternalContact internal = new InternalContact();
        internal.setActive(true);
        internal.setRelationship(internalDto.getRelationship());
        internal.setEmployee(employee);
        internal.setCourse(course);

        InternalContact internalContact = internalContactService.save(internal);
        if (course.getInternalContacts() == null || course.getInternalContacts().size() == 1) {
            course.setPrimaryInternalContact(internalContact);
            courseService.save(course);
        }
        
        //Creating a MeetingInternalContact for the active meeting if the course is not a draft
        if (!course.isDraft()) {
        	meetingInternalContactService.createAndSave(internalContact, course.getActiveMeeting());
        }

        return new ResponseEntity<>(course.getId(), HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Rediger intern kontakt", args = {"courseId", "internalDto"})
    @ResponseBody
    @PostMapping("/rest/course/editinternal")
    public ResponseEntity<?> editInternal(Model model, @RequestParam Long courseId, @RequestBody InternalDto internalDto) {
        if (StringUtils.isEmpty(internalDto.getRelationship())) {
            return new ResponseEntity<>("Forløbsrelation mangler en værdi", HttpStatus.BAD_REQUEST);
        }
        Course course = courseService.getById(courseId);
        if (course == null) {
            return new ResponseEntity<>("Der mangles et forløb", HttpStatus.BAD_REQUEST);
        }

        InternalContact internal = internalContactService.getById(internalDto.getId());
        if (internal == null) {
            return new ResponseEntity<>("Den interne, der er valgt, findes ikke i databasen", HttpStatus.BAD_REQUEST);
        }

        internal.setRelationship(internalDto.getRelationship());

        internalContactService.save(internal);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Slet intern kontakt", args = {"id"})
    @PostMapping("/rest/course/deleteinternal/{id}")
    public ResponseEntity<?> deleteInternal(Model model, @PathVariable Long id) {
        InternalContact internal = internalContactService.getById(id);
        if (internal == null) {
            return new ResponseEntity<>("Den eksterne, der er valgt, findes ikke i databasen", HttpStatus.BAD_REQUEST);
        }
        if (!internal.isActive()) {
            return new ResponseEntity<>("Den eksterne, der er valgt, er allerede inaktiv", HttpStatus.BAD_REQUEST);
        }

        internal.setActive(false);
        internalContactService.save(internal);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Aktiver forløb", args = {"id"})
    @PostMapping("/rest/course/activate/{id}")
    public ResponseEntity<?> activateCourse(Model model, @PathVariable Long id, HttpServletRequest request) {
        Course course = courseService.getById(id);
        if (course == null) {
            return new ResponseEntity<>("Forløbet findes ikke", HttpStatus.BAD_REQUEST);
        }

        course.setDraft(false);
        course.setStartDate(LocalDateTime.now());
        course = courseService.save(course);
        
        //Creating the first meeting
        Meeting tempMeeting = new Meeting();
        tempMeeting.setCourse(course);
        Meeting meeting = meetingService.save(tempMeeting);
        
        course.setActiveMeeting(meeting);
        
        meetingPartyService.createAndSaveAll(course.getParties(), meeting);
        meetingPartyRepresentativeService.createAndSaveAll(course.getPartyRepresentatives(), meeting);
        meetingInternalContactService.createAndSaveAll(course.getInternalContacts(), meeting);
        meetingExternalContactService.createAndSaveAll(course.getExternalContacts(), meeting);
        
        //creating a case for those who don't aleady have one
        var hasCaseErrors = false;
        for (Party party : course.getParties().stream().filter(p -> p.isActive()).collect(Collectors.toList())) {
        	if (StringUtils.isEmpty(party.getEsdhCaseReference())) {
                try {
                    ESDHCase esdhCase = esdhService.createCase(party);
                    party.setEsdhCaseReference(esdhCase.getCaseReference());
                    party.setEsdhCaseDisplayName(esdhCase.getDisplayName());
                    party.setEsdhLinkReference(esdhCase.getLinkReference());
                    partyService.save(party);
                } catch (Exception e) {
                    log.error("Failed to create case for party with id: " + party.getId());
                    hasCaseErrors = true;
                }
            }
        }
        
        courseService.save(course);
        
        String type = "";
        if (course.getParties().stream().filter(p -> p.isActive()).count() > 1) {
        	type = "Flerpartsforløb";
		} else {
			type = "Enkeltpartsforløb";
		}
        
        List<Party> activeParties = course.getParties().stream().filter(p -> p.isActive()).collect(Collectors.toList());
		String header = "<span style='padding-top: 3px;padding-bottom: 3px;'>" + activeParties.get(0).getFirstName() + " " + activeParties.get(0).getSurname() + "</span>";
		for (int i = 1; i < activeParties.size(); i++) {
			header += "<hr style='border-top: 0.01 em; margin: 2px;'/><span style='padding-top: 3px;padding-bottom: 3px;'>" + activeParties.get(i).getFirstName() + " " + activeParties.get(i).getSurname() + "</span>";
		}
		
		request.getSession().setAttribute("courseType", type);
        request.getSession().setAttribute("courseHeader", header);
        request.getSession().setAttribute("courseId", id);
        if( hasCaseErrors ) {
            return new ResponseEntity<>("Fejl under sagsoprettelse", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @SecurityLogIntercepted(operation = "Medarbejder-opslag", args = {"query"})
    @GetMapping("/rest/course/searchinternal")
    public ResponseEntity<?> searchInternal(Model model, @RequestParam String query) {
        return new ResponseEntity<>(employeeService.searchEmployees(query), HttpStatus.OK);
    }

    @PostMapping("/rest/course/locktonavbar")
    public ResponseEntity<?> locktonavbar(Model model, @RequestParam String type, @RequestParam String header, @RequestParam long id, HttpServletRequest request) {
        request.getSession().setAttribute("courseType", type);
        request.getSession().setAttribute("courseHeader", header);
        request.getSession().setAttribute("courseId", id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PostMapping("/rest/course/removelocked")
    public ResponseEntity<?> locktonavbar(HttpServletRequest request) {
        request.getSession().setAttribute("courseType", null);
        request.getSession().setAttribute("courseHeader", null);
        request.getSession().setAttribute("courseId", null);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Ændre et forløbs aktiv-status", args = {"id", "active"})
    @PostMapping("/rest/course/active/{id}")
    public ResponseEntity<?> setActiveCourse(Model model, @PathVariable Long id, @RequestParam boolean active, @RequestParam(required = false) String reason) {
        Course course = courseService.getById(id);
        if (course == null) {
            return new ResponseEntity<>("Forløbet findes ikke", HttpStatus.BAD_REQUEST);
        }
        
        if (!active) {
        	List<Goal> activeGoals = new ArrayList<>();
        	for(MeetingParty party : course.getActiveMeeting().getMeetingParties() ) {
        		activeGoals.addAll(party.getGoals().stream().filter(g -> g.isActive()).collect(Collectors.toList()));
        	}
    		if (!activeGoals.isEmpty()) {
    			return new ResponseEntity<>("Alle mål skal afsluttes før forløbet kan afsluttes.", HttpStatus.BAD_REQUEST);
    		}
        }

        for( var party : course.getParties() ) {
            if( party.isActive() ) {
                if( !active ) {
                    // close ESDH case on party
                    try {
                        esdhService.closeCase(party.getEsdhCaseReference(), reason);
                    } catch (Exception e) {
                        log.error("Failed to close ESDHCase on party with ID: " + party.getId(),e);
                    }
                } else {
                    // reopen ESDH case on party
                    try {
                        esdhService.openCase(party.getEsdhCaseReference());
                    } catch (Exception e) {
                        log.error("Failed to open ESDHCase on party with ID: " + party.getId(),e);
                    }
                }
            }
        }

        course.setActive(active);
        course.setInactiveReason(reason);
        courseService.save(course);
        
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SecurityLogIntercepted(operation = "Sæt tovholder", args = {"id", "courseId"})
    @PostMapping("/rest/course/primaryInternal/{id}")
    public ResponseEntity<?> setPrimary(Model model, @PathVariable Long id, @RequestParam(required = false) Long courseId) {
        InternalContact internalContact = internalContactService.getById(id);
        if (internalContact == null) {
            return new ResponseEntity<>("Den interne findes ikke", HttpStatus.BAD_REQUEST);
        }

        Course course = courseService.getById(courseId);
        if (course == null) {
            return new ResponseEntity<>("Forløbet findes ikke", HttpStatus.BAD_REQUEST);
        }


        course.setPrimaryInternalContact(internalContact);
        
        List<String> notUpdatedNames = new ArrayList<>();
        for (Party party : course.getParties().stream().filter(p -> p.isActive()).collect(Collectors.toList())) {
        		try {
        			esdhService.updateCaseWorker(party);
        		} catch (Exception e) {
					notUpdatedNames.add(party.getFirstName() + " " + party.getSurname());
				}
        }
        
        courseService.save(course);
        return new ResponseEntity<>(notUpdatedNames, HttpStatus.OK);
    }
    
    @PostMapping("/rest/course/recreatecase/{id}")
    public ResponseEntity<?> locktonavbar(Model model, @PathVariable Long id) {
        Party party = partyService.getById(id);
        if (party == null) {
        	return new ResponseEntity<>("Parten findes ikke", HttpStatus.BAD_REQUEST);
        }
        
        ESDHCase esdhCase = null;
        try {
            esdhCase = esdhService.createCase(party);
            party.setEsdhCaseReference(esdhCase.getCaseReference());
            party.setEsdhCaseDisplayName(esdhCase.getDisplayName());
            party.setEsdhLinkReference(esdhCase.getLinkReference());
        } catch (Exception e) {
            log.error("Failed to create case for party with id: " + party.getId(), e);
        }
        
        ESDHResponseDTO response = new ESDHResponseDTO();
        response.setCaseRef(party.getEsdhCaseReference());
        response.setLinkRef(party.getEsdhLinkReference());
        response.setEsdhCaseDisplayName(party.getEsdhCaseDisplayName());
        
        partyService.save(party);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}

