package dk.digitalidentity.sbf.controller.mvc;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dk.digitalidentity.sbf.dao.model.Course;
import dk.digitalidentity.sbf.dao.model.Party;
import dk.digitalidentity.sbf.dao.model.Progress;
import dk.digitalidentity.sbf.interceptors.SecurityLogIntercepted;
import dk.digitalidentity.sbf.service.CourseService;
import dk.digitalidentity.sbf.service.PartyService;
import dk.digitalidentity.sbf.service.ProgressService;

@Controller
public class ProgressController {
	
	@Autowired
	private CourseService courseService;
	
	@Autowired
	private PartyService partyService;
	
	@Autowired
	private ProgressService progressService;
	
	@SecurityLogIntercepted(operation = "Tilgår fremskridtssiden", args = {"courseId"})
	@GetMapping("/progress")
	public String coursePage(Model model, @RequestParam Long courseId) {
		Course course = courseService.getById(courseId);
		
		if (course == null) {
			return "redirect:/error";
		}
		
		LocalDateTime aYearAgo = LocalDateTime.now().minusYears(1);
		model.addAttribute("lessThanAYearAgo", course.getStartDate().isAfter(aYearAgo));
		model.addAttribute("startDate", course.getStartDate());
		model.addAttribute("courseId", course.getId());
		var parties = course.getParties().stream().filter(p -> p.isActive()).collect(Collectors.toList());
		model.addAttribute("parties", parties);
		model.addAttribute("firstPartyId", parties.get(0).getId());
		return "course/progress";
	}
	
	@GetMapping("/progress/fragments/progresstab")
	public String loadPartiesFragment(Model model, @RequestParam Long partyId) {
		Party party = partyService.getById(partyId);
		
		if (party == null) {
			return "redirect:/error";
		}
		
		//See if there is an other progress saved
		LocalDate now = LocalDate.now();
		Progress progress = progressService.getByPartyAndProgressDate(party, now);
		
		model.addAttribute("progress", progress);
		model.addAttribute("party", party);
        return "course/fragments/progress_tab_fragment :: progressTab";
	}
}
