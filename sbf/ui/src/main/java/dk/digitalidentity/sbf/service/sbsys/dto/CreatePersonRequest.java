package dk.digitalidentity.sbf.service.sbsys.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class CreatePersonRequest {
    private String cprNummer;
    private String navn;
    private CreatePersonRequestAddress Adresse = new CreatePersonRequestAddress();
}