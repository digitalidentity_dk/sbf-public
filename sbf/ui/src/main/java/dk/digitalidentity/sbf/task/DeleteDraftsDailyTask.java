package dk.digitalidentity.sbf.task;

import dk.digitalidentity.sbf.config.SBFConfiguration;
import dk.digitalidentity.sbf.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class DeleteDraftsDailyTask {
    @Autowired
    private CourseService courseService;

    @Autowired
    SBFConfiguration sbfConfiguration;

    //Run daily at 01:10
    @Scheduled(cron = "${cron.DeleteDraftsDailyTask:0 10 1 * * ?}")
    public void deleteDrafts() {
        if( sbfConfiguration.isScheduledJobsEnabled() )
        {
            courseService.deleteDrafts();
        }
    }
}
