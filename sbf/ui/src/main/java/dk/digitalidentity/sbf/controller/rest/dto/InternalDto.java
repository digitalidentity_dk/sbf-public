package dk.digitalidentity.sbf.controller.rest.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class InternalDto {
	@ToString.Include
	private Long id;
	private String employeeUuid;
	@ToString.Include
	private String relationship;
}
