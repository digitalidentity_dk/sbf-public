CREATE TABLE courses (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	start_date 				     DATETIME,
	active						 BOOLEAN NOT NULL DEFAULT false,
	inactive_reason				 TEXT,
	created                      DATETIME NOT NULL,
	draft						 BOOLEAN NOT NULL DEFAULT false,
	notes						 VARCHAR(255),
	primary_internal_contact_id  BIGINT,
	active_meeting_id 			 BIGINT
);

CREATE TABLE meetings (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	course_id                    BIGINT,
	meeting_time 				 DATETIME,
	agenda                       TEXT,
	invitation 					 TEXT,
	letter						 TEXT,
	address						 TEXT,
	invite_sent                  BOOLEAN NOT NULL DEFAULT false,
	invite_time					 DATETIME,
	record_sent					 BOOLEAN NOT NULL DEFAULT false,
	
	FOREIGN KEY (course_id) REFERENCES courses(id)
);

CREATE TABLE parties (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	course_id                    BIGINT NOT NULL,
	esdh_case_reference          VARCHAR(255),
	active						 BOOLEAN NOT NULL DEFAULT false,
	cpr                          VARCHAR(10) NOT NULL,
	first_name					 VARCHAR(255) NOT NULL,
	surname                      VARCHAR(255) NOT NULL,
	birth_date                   DATETIME NOT NULL,
	phone                        VARCHAR(255),
	email                        VARCHAR(255),
	address                      VARCHAR(255) NOT NULL,
	post_code                    VARCHAR(255) NOT NULL,
	city                         VARCHAR(255) NOT NULL,
	consent_type                 VARCHAR(255),
	consent_date                 DATETIME,
	sixteen_year_meeting_held    BOOLEAN NOT NULL DEFAULT false,
	sixteen_year_meeting_date    DATETIME,
	eighteen_year_meeting_held   BOOLEAN NOT NULL DEFAULT false,
	eighteen_year_meeting_date   DATETIME,
	created_date                 DATETIME,
	esdh_case_display_name       VARCHAR(255),
	   
	
	FOREIGN KEY (course_id) REFERENCES courses(id)
);

CREATE TABLE meeting_party (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	meeting_id 					 BIGINT NOT NULL,
	party_id                     BIGINT NOT NULL,
	dream                        VARCHAR(255),
	esdh_agenda_reference        VARCHAR(255),
	esdh_record_reference        VARCHAR(255),
	invited                      BOOLEAN NOT NULL DEFAULT false,
	attended                     BOOLEAN NOT NULL DEFAULT false,
	receive_meeting_record       BOOLEAN NOT NULL DEFAULT false,
	
	FOREIGN KEY (meeting_id) REFERENCES meetings(id),
	FOREIGN KEY (party_id) REFERENCES parties(id)
);

CREATE TABLE party_representatives (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	course_id                    BIGINT NOT NULL,
	active						 BOOLEAN NOT NULL DEFAULT false,
	cpr                          VARCHAR(10) NOT NULL,
	first_name					 VARCHAR(255) NOT NULL,
	surname                      VARCHAR(255) NOT NULL,
	phone                        VARCHAR(255),
	email                        VARCHAR(255),
	address                      VARCHAR(255) NOT NULL,
	post_code                    VARCHAR(255) NOT NULL,
	city                         VARCHAR(255) NOT NULL,
	created_date                 DATETIME,
	relationship                 VARCHAR(255) NOT NULL,
	   
	
	FOREIGN KEY (course_id) REFERENCES courses(id)
);

CREATE TABLE meeting_party_representatives (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	meeting_id 					 BIGINT NOT NULL,
	party_representative_id      BIGINT NOT NULL,
	invited                      BOOLEAN NOT NULL DEFAULT false,
	attended                     BOOLEAN NOT NULL DEFAULT false,
	receive_meeting_record       BOOLEAN NOT NULL DEFAULT false,
	
	FOREIGN KEY (meeting_id) REFERENCES meetings(id),
	FOREIGN KEY (party_representative_id) REFERENCES party_representatives(id)
);

CREATE TABLE goals (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	meeting_party_id 		     BIGINT NOT NULL,
	title                        VARCHAR(255) NOT NULL,
	objective                    VARCHAR(255) NOT NULL,
	active						 BOOLEAN NOT NULL DEFAULT false,
	closed_reason                VARCHAR(255),
	
	FOREIGN KEY (meeting_party_id) REFERENCES meeting_party(id)
);

CREATE TABLE sub_goals (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	goal_id 		             BIGINT NOT NULL,
	title                        VARCHAR(255),
	action                       VARCHAR(255),
	start_date 				     DATE,
	closed                       BOOLEAN NOT NULL DEFAULT false,
	closed_reason                VARCHAR(255),
	trial_action_initiated       BOOLEAN NOT NULL DEFAULT false,
	trial_action_approved        BOOLEAN NOT NULL DEFAULT false,
	
	FOREIGN KEY (goal_id) REFERENCES goals(id)
);


CREATE TABLE employees (
	employee_uuid                VARCHAR(255) NOT NULL PRIMARY KEY,
	active						 BOOLEAN NOT NULL DEFAULT false,
	username                     VARCHAR(255) NOT NULL,
	first_name					 VARCHAR(255) NOT NULL,
	surname                      VARCHAR(255) NOT NULL,
	email                        VARCHAR(255),
	phone                        VARCHAR(255),
	position 					 VARCHAR(255) NOT NULL,
	department                   VARCHAR(255) NOT NULL
);

CREATE TABLE internal_contacts (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	course_id                    BIGINT NOT NULL,
	employee_uuid                VARCHAR(255) NOT NULL,
	active						 BOOLEAN NOT NULL DEFAULT false,
	relationship                 VARCHAR(255) NOT NULL,
	
	FOREIGN KEY (course_id) REFERENCES courses(id),
	FOREIGN KEY (employee_uuid) REFERENCES employees(employee_uuid)
);

CREATE TABLE external_contacts (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	course_id                    BIGINT NOT NULL,
	active						 BOOLEAN NOT NULL DEFAULT false,
    first_name					 VARCHAR(255) NOT NULL,
	surname                      VARCHAR(255) NOT NULL,
	email                        VARCHAR(255),
	phone                        VARCHAR(255),
	relationship                 VARCHAR(255) NOT NULL,
		
	FOREIGN KEY (course_id) REFERENCES courses(id)
);

CREATE TABLE progresses (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	course_id                    BIGINT NOT NULL,
	party_id                     BIGINT NOT NULL,
	score                        BIGINT NOT NULL,
	progress_comment             VARCHAR(255) NOT NULL,
	progress_date                DATE NOT NULL,
	scored_by_professional       BOOLEAN NOT NULL DEFAULT false,
		
	FOREIGN KEY (course_id) REFERENCES courses(id),
	FOREIGN KEY (party_id) REFERENCES parties(id)
);

CREATE TABLE meeting_internal_contact (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	meeting_id                   BIGINT NOT NULL,
	internal_contact_id          BIGINT NOT NULL,
	invited                      BOOLEAN NOT NULL DEFAULT false,
	attended                     BOOLEAN NOT NULL DEFAULT false,
		
	FOREIGN KEY (meeting_id) REFERENCES meetings(id),
	FOREIGN KEY (internal_contact_id) REFERENCES internal_contacts(id)
);

CREATE TABLE meeting_external_contact (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	meeting_id                   BIGINT NOT NULL,
	external_contact_id          BIGINT NOT NULL,
	invited                      BOOLEAN NOT NULL DEFAULT false,
	attended                     BOOLEAN NOT NULL DEFAULT false,
		
	FOREIGN KEY (meeting_id) REFERENCES meetings(id),
	FOREIGN KEY (external_contact_id) REFERENCES external_contacts(id)
);

CREATE TABLE sub_goal_responsible_party (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	sub_goal_id                  BIGINT NOT NULL,
	party_id                     BIGINT NOT NULL,
		
	FOREIGN KEY (sub_goal_id) REFERENCES sub_goals(id),
    FOREIGN KEY (party_id) REFERENCES parties(id)
);

CREATE TABLE sub_goal_responsible_internal_contact (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	sub_goal_id                  BIGINT NOT NULL,
	internal_contact_id          BIGINT NOT NULL,
		
	FOREIGN KEY (sub_goal_id) REFERENCES sub_goals(id),
    FOREIGN KEY (internal_contact_id) REFERENCES internal_contacts(id)
);

CREATE TABLE sub_goal_responsible_external_contact (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	sub_goal_id                  BIGINT NOT NULL,
	external_contact_id          BIGINT NOT NULL,
		
	FOREIGN KEY (sub_goal_id) REFERENCES sub_goals(id),
    FOREIGN KEY (external_contact_id) REFERENCES external_contacts(id)
);


ALTER TABLE courses ADD FOREIGN KEY (primary_internal_contact_id) REFERENCES internal_contacts (id);
ALTER TABLE courses ADD FOREIGN KEY (active_meeting_id) REFERENCES meetings(id);