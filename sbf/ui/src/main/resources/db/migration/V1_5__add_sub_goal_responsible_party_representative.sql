CREATE TABLE sub_goal_responsible_party_representative (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	sub_goal_id                  BIGINT NOT NULL,
	party_representative_id      BIGINT NOT NULL,
		
	FOREIGN KEY (sub_goal_id) REFERENCES sub_goals(id),
    FOREIGN KEY (party_representative_id) REFERENCES party_representatives(id)
);