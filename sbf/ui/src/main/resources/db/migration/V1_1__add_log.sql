CREATE TABLE security_logs (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	user_uuid 					 VARCHAR(36) NOT NULL,
	tts                          DATETIME NOT NULL,
	ip							 VARCHAR(255) NOT NULL,
	operation					 VARCHAR(255) NOT NULL,
	details 				     TEXT
	
);