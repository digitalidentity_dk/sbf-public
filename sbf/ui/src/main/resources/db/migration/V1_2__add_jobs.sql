CREATE TABLE journalize_job (
	id                           BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	created 				     DATETIME NOT NULL,
	changed 				     DATETIME,
	created_by  				 VARCHAR(255) NOT NULL,
    agenda_type                  VARCHAR(255) NOT NULL,
    meeting_id                   BIGINT NOT NULL,
    meeting_party_id             BIGINT NOT NULL,
	status						 VARCHAR(255) NOT NULL,
	esdh_reference				 VARCHAR(255),
	error				         text,

	FOREIGN KEY (meeting_id) REFERENCES meetings(id),
	FOREIGN KEY (meeting_party_id) REFERENCES meeting_party(id)
);

CREATE TABLE post_job (
	id                                  BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	created 				            DATETIME NOT NULL,
	changed 				            DATETIME,
	created_by  				        VARCHAR(255) NOT NULL,
    agenda_type                         VARCHAR(255) NOT NULL,
    meeting_id                          BIGINT NOT NULL,
    meeting_party_id                    BIGINT,
    meeting_party_representative_id     BIGINT,
	status                              VARCHAR(255) NOT NULL,
	post_reference                      VARCHAR(255),
	error                               text,

	FOREIGN KEY (meeting_id) REFERENCES meetings(id),
	FOREIGN KEY (meeting_party_id) REFERENCES meeting_party(id),
	FOREIGN KEY (meeting_party_representative_id) REFERENCES meeting_party_representatives(id)
);

ALTER TABLE meeting_party DROP esdh_agenda_reference;
ALTER TABLE meeting_party DROP esdh_record_reference;
