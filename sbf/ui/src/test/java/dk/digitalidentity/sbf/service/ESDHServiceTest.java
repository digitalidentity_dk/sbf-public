package dk.digitalidentity.sbf.service;
/*
THESE ARE NOT ACTUAL TESTS.
Just some debug entrypoints to invoke/test code while developing
 */

import dk.digitalidentity.sbf.dao.model.Course;
import dk.digitalidentity.sbf.dao.model.Employee;
import dk.digitalidentity.sbf.dao.model.InternalContact;
import dk.digitalidentity.sbf.dao.model.Party;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@SpringBootTest
// switch to dev profile to test mock implementations
@ActiveProfiles("test")
class ESDHServiceTest {

//    @Autowired
//    ESDHService esdhService;
//
//    @Test
//    void createDocument() throws IOException {
//        var party = new Party();
//        party.setEsdhCaseReference("436086");
//        party.setCpr("111111-2122");
//        var bytes = Files.readAllBytes(Path.of("/home/pso/sbsystest.pdf"));
//        esdhService.createDocument("Test from code", party, bytes);
//    }
//
//    @Test
//    void createCase()
//    {
//        var party = new Party();
//        party.setFirstName("Test");
//        party.setSurname("Testsen");
//        party.setCpr("111111-2122");
//        var course = new Course();
//        var contact = new InternalContact();
//        var employee = new Employee();
//        employee.setUsername("kij4");
//        contact.setEmployee(employee);
//        course.setPrimaryInternalContact(contact);
//        party.setCourse(course);
//        esdhService.createCase(party);
//    }

}