#!/bin/bash

# copy source into local folder
rm -Rf build/
mkdir build
cp -R ../../remoteprint-integration/* build

# login to dockerhub
docker login

# build
docker build -t remoteprint-integration:latest .

# push to dockerhub (rename repo from "digid" to real repo)
docker tag remoteprint-integration:latest digid/remoteprint-integration:$(date +'%Y-%m-%d')
docker push digid/remoteprint-integration:$(date +'%Y-%m-%d')

rm -Rf build/
