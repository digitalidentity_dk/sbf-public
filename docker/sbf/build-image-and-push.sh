#!/bin/bash

# copy the codebase into a build folder so docker can see it (it cannot look outside this folder for security reasons)
rm -Rf build/
mkdir build

cp -R ../../sbf/* build

# login to docker hub
docker login

# build image
docker build -t sbf:latest .

# tag and push to dockerhub (rename digid to actual repository)
docker tag sbf:latest digid/sbf:$(date +'%Y-%m-%d')
docker push digid/sbf:$(date +'%Y-%m-%d')

# cleanup
rm -Rf build/
