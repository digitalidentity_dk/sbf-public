#!/bin/bash

if [ -z "$MAX_HEAP" ]
then
	MAX_HEAP=-Xmx256m
fi

java $MAX_HEAP -jar sbf-ui-1.0.0.jar
