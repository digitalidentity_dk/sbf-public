#!/bin/bash

# copy sourcecode to local folder for building
rm -Rf build/
mkdir build
cp -R ../../cpr-integration/* build

# login to dockerhub
docker login

# build
docker build -t cpr-integration:latest .

# push to dockerhub - rename "digid" with actual repository
docker tag cpr-integration:latest digid/cpr-integration:$(date +'%Y-%m-%d')
docker push digid/cpr-integration:$(date +'%Y-%m-%d')

# cleanup
rm -Rf build/
